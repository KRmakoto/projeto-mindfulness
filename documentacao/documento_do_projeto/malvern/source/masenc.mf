% masenc.mf 1.2.0 1994/10/11 -- standard TeX encoding
% Copyright 1994 P. Damian Cugley

%%% @METAFONT-file {
%%%   filename       = "masenc.mf",
%%%   version        = "1.2.0",
%%%   date           = "1994/10/11",
%%%   package        = "Malvern 1.2",
%%%   author         = "P. Damian Cugley",
%%%   email          = "damian.cugley@comlab.ox.ac.uk",
%%%   address        = "Oxford University Computing Laboratory,
%%%                     Parks Road, Oxford  OX1 3QD, UK",
%%%   codetable      = "USASCII",
%%%   keywords       = "Malvern, METAFONT, font, typefont, TeX",
%%%   supported      = "Maybe",
%%%   abstract       = "Encodign definition for the Malvern
%%%                     font family.",
%%%   dependencies   = "other program files",
%%% }

%  See the Malvern Handbook (maman.tex) for more info about Malvern.
%  This software is available freely but without warranty.
%  See the file COPYING for details.

%{{{ masenc.mf 1.2.0 1994/10/11
%{{{   cap Greek letters

code.gr.cap.gamma  	= 0;
code.gr.cap.delta  	= 1;
code.gr.cap.theta  	= 2;
code.gr.cap.lambda 	= 3;
code.gr.cap.xi	    	= 4;
code.gr.cap.pi	    	= 5;
code.gr.cap.sigma  	= 6;
code.gr.cap.upsilon	= 7;
code.gr.cap.phi    	= 8;
code.gr.cap.psi    	= 9;
code.gr.cap.omega  	= 10;

%}}}
%{{{   ligatures & specials

code.lc.f.f		= 11;
code.lc.f.i		= 12;
code.lc.f.l		= 13;
code.lc.f.f.i		= 14;
code.lc.f.f.l		= 15;
code.lc.dotless_i	= 16;
code.lc.dotless_j	= 17;
code.lc.eszet		= 25;
code.lc.ae		= 26;
code.lc.oe		= 27;
code.lc.o.slash		= 28;
code.cap.ae		= 29;
code.cap.oe		= 30;
code.cap.o.slash	= 31;		

%}}}
%{{{   marks for composite letters

code.mk.grave	= 18;
code.mk.acute	= 19;
code.mk.hook   	= 20;
code.mk.breve	= 21;
code.mk.macron	= 22;
code.mk.ring	= 23;
code.mk.circumflex= 94;
code.mk.dot 	= 95;
code.mk.hungarian= 125;
code.mk.tilde	= 126;
code.mk.twodots	= 127;

code.mk.cidella	= 24;
code.mk.L_bar	= 32;

%}}}
%{{{   punctuation

input ascii
code.dbl.apostrophe	= 34;
code.hyphen 		= ASCII"-";
code.inv.exclam		= 60;
code.inv.question	= 62;
code.dbl.inv.comma 	= 92;
code.en_dash		= 123;
code.em_dash		= 124;

%}}}

code.offset.LS = 0;

for i = ASCII"a" step 1 until ASCII"z":
    scantokens ("code.lc." & char i) = i;
    scantokens ("code.cap." & char i) = i - 32;
endfor

if encoding = 26:
    code.zero.old_style = ASCII "0";
    font_coding_scheme "TeX text with old-style numerals";
else:
    code.zero = ASCII"0";
    font_coding_scheme "TeX text";
fi

%}}}  standard TeX encoding

% Local variables:
% fold-folded-p: t
% End:
