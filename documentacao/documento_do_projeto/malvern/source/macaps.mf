% macaps.mf 1.2.0 94/10/11 -- capital letters program file
% Copyright  1991-4 P. Damian Cugley

%%% @METAFONT-file {
%%%   filename                   = "macaps.mf",
%%%   version                    = "1.2.0",
%%%   package                    = "Malvern 1.2",
%%%   author                     = "P. Damian Cugley",
%%%   email                      = "damian.cugley@comlab.ox.ac.uk",
%%%   address                    = "Oxford University Computing Laboratory,
%%%                                 Parks Road, Oxford  OX1 3QD, UK",
%%%   codetable                  = "USASCII",
%%%   keywords                   = "Malvern, METAFONT, font, typefont, TeX",
%%%   supported                  = "Maybe",
%%%   abstract                   = "Character programs for the Malvern
%%%                                 font family, to be compiled using
%%%                                 METAFONT into TFM files (for TeX) and
%%%                                 GF files (for your printer driver).",
%%%   dependencies               = "other program files",
%%% }

%  See the Malvern Handbook (maman.tex) for more info about Malvern.
%  This software is available freely but without warranty.
%  See the file COPYING for details.

%  This file produces capital letters of the Latin alphabet, including
%  a couple of national letters like thorn and AE ligature.

%{{{ macaps.mf

set_cap_widths;

if testing: endinput fi

input maglcaps

%{{{   L

ma_cap(capcode.l, narrow_wd# + pn_adj#)(1,1/2);
    box_points;
    draw (x.l, y.t) -- (x.l, y.b) -- (x.r, y.b);
remember;
    some_cap_marks(0.45[l,r]) l;
endchar;

%}}}   L
%{{{   D

D_wd# :=  wide_wd# + pn_adj#;

ma_cap(capcode.d, D_wd#)(1, O_sp); 
    D_stem; D_bowl.bowl(h, -d, r); set_ic_O;
if not no_co:
remember;
    some_cap_marks(0.45[l,r]) D;
fi
endchar;

%}}}   D
%{{{   R

ma_cap(capcode.r, max(2pn_wd# + 3u#, narrow_wd# + pn_adj#))(1,1/2);
    P_stem; D_bowl.bowl(h, hround bot 0.5[-d, h], r);
    z1tail = z4bowl;
    z2tail = (lft r + ho, top -d - o); % bot right
    draw z1tail -- z2tail;
    labels(1tail, 2tail);
    set_ic 0.9h#; charic := max(0, charic - 0.1(r# - l#));
if not no_co:
remember;
    some_cap_marks(0.5[l,r]) R;
fi
endchar;

%}}}
%{{{   G

ma_cap(capcode.g,  wide_wd# + 2pn_adj#)(O_sp, 3/4); 
    draw_C(l, h, r, -d) 1/20; 
    x6 = x5; x7 = good.x 0.6[l,r];
    y6 = y7 = good.y (0.45[-d, h]);
    draw z5 -- z6 -- z7;
    labels(6,7); set_ic 0.95h#;
if not no_co:
remember;
    some_cap_marks(0.6[l,r]) G;
fi
endchar;

%}}}
%{{{   Y

ma_cap(capcode.y, medium_wd# + 2pn_adj#)(1/2,1/2); 
    draw_Y(0.475[-d, h]); set_ic_tr; 
remember;
if not no_co:
    some_cap_marks(0.5w) Y;
fi
endchar;

%}}}
%{{{   U

ma_cap(capcode.u,medium_wd# + 2pn_adj#)(1,1);
    lft x1 = lft x0 = l; rt x3 = rt x4 = r;
    top y0 = top y4 = h + o;
    bot z2 = (1/2[l,r], -d -o);	
    z1 = z2 + whatever*(-hratio,1); z3 = z2 + whatever*(hratio,1);
    draw z0 --- z1 ... z2 ... z3 --- z4;
    labels(0,1,2,3,4); set_ic_tr;
remember;
if not no_co:
some_cap_marks(1/2[l,r]) U;
similarly(code.co.U.ring); "Cap. U with ring";
    if cap_mark_t - h >= 4v:
    	%  there is room to float the ring:
    	draw_clear_ring.ring(1/2[l,r], 1/2[h, cap_mark_t] - 1.5v - eps, 3u, 3v);
    elseif cap_mark_t - h >= 3v:
    	draw_clear_ring.ring(1/2[l,r], h, 3u, 3v);
    else:
    	%  Not enough room -- squash the ring:
	draw_joined_ring.ring(1/2[l,r], h, 3u, min(cap_mark_t - h + pn.ht - o, 3v));
    fi
    charht := cap_mark_t#;
wug;
fi
endchar;
%}}}  U
%{{{   C

% Compressible pdc Mon.  6 May 1991

ma_cap(capcode.c, wide_wd# + 2pn_adj#)(O_sp, 1/3); 
    draw_C(l, h, r, -d) 1/16; set_ic 0.95h#; 
remember;
if not no_co:
    some_cap_marks(0.6[l,r]) C;
fi
similarly(code.C_cedilla); "cap C with cedilla";
    chardp := Cedilla_ht#;  charht := cap_ht#;
    draw_cedilla.cedilla(x2, 0, Cedilla_wd, Cedilla_ht);
wug;
endchar;

%}}}
%{{{   S

ma_cap(capcode.s, 0.9narrow_wd# + pn_adj#)(0.8,0.8); 
    draw_S(h, -d)(0.53, 0.075, 0.1); 
    set_ic_tr; charic := max(0, charic - 1/2u#);
remember;
if not no_co:
    some_cap_marks(0.5[l,r]) S;
fi
similarly(code.S_cedilla); "cap S with cedilla";
    chardp := Cedilla_ht#;  charht := cap_ht#;
    draw_cedilla.cedilla(1/2[l,r], 0, Cedilla_wd, Cedilla_ht);
wug;
endchar;

%}}}
if co_only: endinput; fi
%{{{   A-ogonek

iff known capcode.a.ogonek: "Capital A with ogonek";
ma_char(capcode.a.ogonek, wide_wd# + 3pn_adj#, height#, ogonek_dp#)(1/3,1/3);
    A_body(true, false);
    z1ogonek = z3; y2ogonek = 2/3[y1ogonek, y3ogonek]; 
    rt x2ogonek = lft x1ogonek - min(u, pn.wd);
    if lft x2ogonek < 1/2[l,r]: x2ogonek := rt 1/2[l,r]; fi
    x3ogonek = good.x (lft x2ogonek + 2.5u);
    bot y3ogonek = vround -ogonek_dp; 
    draw hvarc(1ogonek,2ogonek) ... z3ogonek{right};
    labels(1ogonek, 2ogonek, 3ogonek);
endchar;

%}}}   A-ogonek
%{{{   E-ogonek

iff known capcode.e.ogonek: "E with ogonek";

ma_cap(capcode.e.ogonek, narrow_wd# + pn_adj#)(1, 1/2);
    rt x1 = rt x4 = r; lft x2 = lft x3 = lft x1bar = l;
    top y1 = top y2 = h; bot y3 = bot y4 = 0;
    y1bar = y2bar = 0.52h; x2bar = 4/5[l,r];
    draw z1 -- z2 -- z3 -- z4; draw z1bar -- z2bar;
    set_ic_tr;
    labels(1,2,3,4, 1bar, 2bar);
    z1ogonek = z4; y2ogonek = 2/3[y1ogonek, y3ogonek]; 
    rt x2ogonek = lft x1ogonek - min(u, pn.wd);
    if lft x2ogonek < 1/2[l,r]: x2ogonek := rt 1/2[l,r]; fi
    x3ogonek = good.x (lft x2ogonek + 2.5u);
    bot y3ogonek = vround -ogonek_dp; 
    draw hvarc(1ogonek,2ogonek) ... z3ogonek{right};
    labels(1ogonek, 2ogonek, 3ogonek);
endchar;

%}}}   E-ogonek
%{{{   AE and OE

% - pdc Tue. 26 Feb. 1991

def draw_lig_E =
    top y1mid = top y1bar = h;
    y2mid = y2bar = good.y 0.55[-d, h];
    bot y3mid = bot y3bar = -d;
    lft x1mid = lft x2mid = lft x3mid = hround (r - narrow_wd);
    rt x1bar = rt x3bar = r; x2bar = good.x 4/5[x1mid, x1bar];  
    draw z1bar -- z1mid -- z3mid -- z3bar;
    draw z2mid -- z2bar;
    labels(1mid, 2mid, 3mid, 1bar, 2bar, 3bar); set_ic_tr
enddef;    

if known capcode.ae:
    "Cap. lig. AE";
    ma_char(capcode.ae, 3/4medium_wd# + narrow_wd#, height#, 0v#)(1/2,1/2);
	draw_lig_E;
	y1 = y1mid; y2 = y2mid; y3=y3mid;
	x1 = x1mid - 2apex_adjust; lft x3 = l; z2 = whatever[z1, z3];   
	draw z3 -- z1 -- z1mid; draw z2 -- z2mid;
	labels(1, 2, 3);
    endchar;
fi

if known capcode.oe:
    "Cap. lig. OE";
    ma_char(capcode.oe, 1/2width# + narrow_wd# + 1/2pn.wd#,
    	    height#, 0v#)(1/2,1/2);
	draw_lig_E;
	x1 = x3 = lft x1mid ; y1 = y1mid; y3 = y3mid;
	lft x2 = l; y2 = 1/2[-d, h];
	draw z1mid --- z1 .. z2 .. z3 --- z3mid;
	labels(1,2,3);
    endchar;
fi
%}}}
%{{{   ETH

iff known capcode.eth: "Cap. Eth";
ma_cap(capcode.eth, D_wd#)(1, O_sp); 
    D_stem; D_bowl.bowl(h, -d, r); 
    lft x3 = hround (x1 - 1/10(r - l) - 1/2pn.wd);
    rt x4 = hround (x3 + 1/2(r - l));
    y3 = y4 = good.y (1/2[-d, h]);
    draw z3 -- z4;
    labels(3,4);  set_ic_O;
endchar;

%}}}
%{{{   F, L-bar

if known capcode.f:
    do_EFL(capcode.f, true, 0, 3/4, false);
fi

if known capcode.l.slash: 
    "Cap. L with bar"; 
    do_EFL(capcode.l.slash, false, -1/4, 1/2, true);
fi

%}}}
%{{{   Eng

iff known capcode.eng:  "Lappish cap. Eng";
ma_char(capcode.eng, medium_wd# + 2pn_adj#, height#, 2/3desc_dp#)(1,1);
    lft x1l = lft x2l = lft x3l = l; rt x1r = rt x2r = r;
    top y1l = top y1r = h + o; bot y3l = - o;
    bot y3r = bot y4r - 1/2v = -d - o; 
    x3r = 0.55[x2r, x4r]; x4r = 0.45[r,l];
    z2r = z3r + whatever*ne; z2l = z1r + whatever*ne;
    draw z1l -- z3l; draw z2l -- z1r --- z2r ... z3r{left} ... z4r;
    labels(1l, 2l, 3l, 1r, 2r, 3r, 4r, 5r); set_ic_tr;
endchar;

%} }}
%{{{   Thorn

iff known capcode.thorn: "Cap. Thorn";
ma_cap(capcode.thorn, max(2pn_wd# + 3u#, narrow_wd# + pn_adj#))(1,1/2); 
    H_stem; D_bowl.bowl(0.85[-d,h], 0.35[-d, h], r); set_ic 0.75h#;
endchar;

%}}}
%{{{   Q O/slash

"Cap. Q"; 
ma_char(capcode.q, O_wd#, height#, 1/6height#)(O_sp, O_sp);
    draw_O; set_ic_O;
    x1tail = x3 + 3u; y1tail = -d; draw z3 -- z1tail;
endchar;

iff known capcode.o.slash: "Cap. O with slash";
ma_char(capcode.o.slash, O_wd#, height#, 0v#)(O_sp, O_sp);
    draw_O; draw_O_slash; set_ic_O;
endchar;

%}}}
%{{{   V

ma_cap(capcode.v, medium_wd# + 3pn_adj#)(1/3,1/3); 
	draw_V;  set_ic_tr; 
endchar;

%}}}
%{{{   W

"Cap. W";
ma_char(capcode.w, 1.2width# + 2pn_adj#, height#, 0pt#)(1/3,1/3);
    if 1/2w <> apex.x 1/2w: change_width; fi
    lft x1 = w - rt x5 = l; top y1 = top y5 = h + o;
    bot y2 = bot y4 = -d;
    x3 = 1/2[x1,x5]; y3 = good.y 4/5[-d,h];
    x2 = w - x4 = apex.x (y3 / (y3 + y1))[l,1/2[l,r]]; 
    draw z1 -- vpex_path2 -- apex_path3 -- vpex_path4 -- z5;
    labels(1, 2, 3, 4, 5); set_ic_tr;
endchar;

%}}}  W
%{{{   J

"Cap. J";
ma_char(capcode.j, 0.5width# + pn_adj#, height#, 0pt#)(1/3,1);
    rt x1 = rt x2 = r; top y1 = h + o;
    x3 = 1/2[r, l]; bot y3 = -d - o;
    z2 = z3 + whatever*(hratio,1);
    lft x4 = l; y4 = 1/8[-d, h];
    draw z1 --- z2 .. z3 .. z4;
    labels(1,2,3,4); set_ic_tr;
remember;
if not no_co:
    some_cap_marks(lft r) J;
fi
if not co_only:
similarly(capcode.ij); "IJ ligature"; 
    charwd := charwd + pn.wd# + 4/3sp#;
    interim xoffset := pn.wd + hround(7/3sp#*hppp);
    lft  x1I = lft x0I = hround(-xoffset + sp);
    top y0I = h + o; bot y1I = -d - o; 
    draw z0I -- z1I; 
fi
endchar;

%}}}
%}}} macaps

%Local variables:
%fold-folded-p: t
%End:
