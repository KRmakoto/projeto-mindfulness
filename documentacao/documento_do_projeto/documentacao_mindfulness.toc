\select@language {brazil}
\contentsline {chapter}{\numberline {1}Controle de Vers\IeC {\~a}o do Projeto}{7}{chapter.1}
\contentsline {chapter}{\numberline {2}Termo de Abertura do Projeto}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}T\IeC {\'\i }tulo do Projeto}{9}{section.2.1}
\contentsline {section}{\numberline {2.2}Cliente}{9}{section.2.2}
\contentsline {section}{\numberline {2.3}Objetivo do Projeto}{10}{section.2.3}
\contentsline {section}{\numberline {2.4}Demanda do Neg\IeC {\'o}cio}{10}{section.2.4}
\contentsline {section}{\numberline {2.5}O que \IeC {\'e}/n\IeC {\~a}o \IeC {\'e} Escopo do Projeto}{10}{section.2.5}
\contentsline {section}{\numberline {2.6}Prazo Estimado para Conclus\IeC {\~a}o do Projeto}{11}{section.2.6}
\contentsline {section}{\numberline {2.7}Equipe B\IeC {\'a}sica}{11}{section.2.7}
\contentsline {section}{\numberline {2.8}Restri\IeC {\c c}\IeC {\~o}es}{11}{section.2.8}
\contentsline {chapter}{\numberline {3}Declara\IeC {\c c}\IeC {\~a}o do Escopo do Projeto}{13}{chapter.3}
\contentsline {section}{\numberline {3.1}Objetivo do Documento}{13}{section.3.1}
\contentsline {section}{\numberline {3.2}Justificativa do Projeto}{13}{section.3.2}
\contentsline {section}{\numberline {3.3}Produto deste Projeto}{14}{section.3.3}
\contentsline {section}{\numberline {3.4}Produtos a Serem Entregues}{14}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Projeto de Gerenciamento de Tempo de Projeto}{14}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Projeto de Gerenciamento de Qualidade do Projetos}{15}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Projeto de Gerenciamento de Comunica\IeC {\c c}\IeC {\~a}o de Projeto}{15}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Projeto de Gerenciamento de Risco}{15}{subsection.3.4.4}
\contentsline {subsection}{\numberline {3.4.5}Login de Cadastro de Usu\IeC {\'a}rio}{15}{subsection.3.4.5}
\contentsline {subsection}{\numberline {3.4.6}Projeto de Software}{16}{subsection.3.4.6}
\contentsline {subsection}{\numberline {3.4.7}Sistema para os Administradores do Sistema}{16}{subsection.3.4.7}
\contentsline {subsection}{\numberline {3.4.8}Sistema para os Professores Administrarem os Laborat\IeC {\'o}rios}{16}{subsection.3.4.8}
\contentsline {section}{\numberline {3.5}EAP}{16}{section.3.5}
\contentsline {section}{\numberline {3.6}Exclus\IeC {\~o}es do Projeto}{17}{section.3.6}
\contentsline {section}{\numberline {3.7}Restri\IeC {\c c}\IeC {\~o}es do Projeto}{17}{section.3.7}
\contentsline {section}{\numberline {3.8}Riscos do Projeto}{18}{section.3.8}
\contentsline {section}{\numberline {3.9}Crit\IeC {\'e}rios de Aceita\IeC {\c c}\IeC {\~a}o do Produto}{18}{section.3.9}
\contentsline {chapter}{\numberline {4}Plano de Gerenciamento de Tempo do Projeto}{19}{chapter.4}
\contentsline {section}{\numberline {4.1}Plano de Gerenciamento de Tempo}{19}{section.4.1}
\contentsline {section}{\numberline {4.2}Sequenciamento de Atividades}{19}{section.4.2}
\contentsline {chapter}{\numberline {5}Plano de Gerenciamento de Qualidade}{21}{chapter.5}
\contentsline {section}{\numberline {5.1}Padr\IeC {\~o}es de Qualidade}{21}{section.5.1}
\contentsline {section}{\numberline {5.2}Crit\IeC {\'e}rios de Avalia\IeC {\c c}\IeC {\~a}o}{21}{section.5.2}
\contentsline {section}{\numberline {5.3}Responsabilidade na Garantia da Qualidade}{21}{section.5.3}
\contentsline {chapter}{\numberline {6}Plano de Gerenciamento de Comunica\IeC {\c c}\IeC {\~a}o}{23}{chapter.6}
\contentsline {section}{\numberline {6.1}Aspectos Gerais}{23}{section.6.1}
\contentsline {section}{\numberline {6.2}Controle de Reuni\IeC {\~o}es}{23}{section.6.2}
\contentsline {chapter}{\numberline {7}Plano de Gerenciamento de Riscos do Projeto}{25}{chapter.7}
