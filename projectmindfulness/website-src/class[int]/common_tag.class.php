<?php
/* Esta Classe proporciona gerar um vormulario com elementos de textfield, text-
  area, radio e select do HTML.

  extrura basica e [create<Nomedatag>]

*/
class HTML
{
   //Variaveis
   private
      $init_content,     //Elementos ate o body
      $head_elem_number, //Numero de elementos
      $flag;
   
   private 
      $DOCTYPE,
      $HEAD,
      $BODY;

  
 
   /*Constroí a classe do formuário-------------------------------------------*/
   function __construct( $DOCTYPE, $flag = 0 )
   {
      if(!(empty($DOCTYPE)))
      {
         if( $flag == 0 )
         {
            $this->DOCTYPE = "<$DOCTYPE>";
            $this->HEAD    = [];
            $this->BODY    = [];
         }
         else if( $flag == 1 )
         {
            $this->DOCTYPE = "<$DOCTYPE>\n";
            $this->HEAD    = "<head>\n".
                             "\t<meta charset='utf-8'>\n".
                             "\t<title>NULL_NAME</title>\n".
                             "</head>\n";
            $this->BODY    = "<body>\n";
         }
         else
         {
            echo "<br>Flag invalida<br>";
         }
      }
   }

   /*Adicionar novo elemento no formulario*/
   private function setElement($str)
   {
      $this->elem_number++;
      $this->form_content[] =  $str ;
   }
  
   private function initElement()
   {
      $this->HEAD[]   = [0, "<head "];
      $this->BODY[]   = [0, "<body "];
      $this->FOOTER[] = [0, "<footer "];
   }



   /*Imprimir os elementos do formulario*/
   public function printInit()
   {
      if($this->flag == 0)
      {
      }
      
      echo $this->DOCTYPE;
      echo "<html>\n";
      echo $this->HEAD;
      echo $this->BODY;
   }

   public function printTerm()
   {
      if($this->flag == 0)
      {
      }

      echo "\n</body>\n";
      echo "</html>";
   }
}
?>
