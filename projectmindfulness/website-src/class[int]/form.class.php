<?php
/* Esta Classe proporciona gerar um vormulario com elementos de textfield, text-
  area, radio e select do HTML.

  extrura basica e [create<Nomedatag>]

*/
class Forms
{
   //Variaveis
   private $form_content,  //Elementos do formulario
           $btn_content;

   private $elem_number,
           $btn_number;    //Numero de elementos
 
   /*Constroí a classe do formuário-------------------------------------------*/
   function __construct( $name, $action, $method, $enctype)
   {
      $ini_form = "<form ";
      $this->elem_number = $this->btn_number = 0;        

      if( !empty($name) )
      {
         $ini_form .= "name='$name' ";
      }

      if( !empty($action) )
      {
         $ini_form .= "action='$action' ";
      }

      if( !empty($method) )
      {
         $ini_form .= "method='$method' ";
      }

      if( !empty($enctype) )
      {
         $ini_form .= "enctype='$enctype'";
      }
     
      $this->form_content = [0 => "$ini_form>"];
   }

   /*Adicionar novo elemento no formulario*/
   private function setElement($str)
   {
      $this->elem_number++;
      $this->form_content[] =  $str ;
   }

   private function setButton()
   {

   }

   /*Cria um novo textarea HTML*/
   public function createTextarea( $rotulo )
   {
      $str = "";
      $name = str_replace(" ", "_", strtolower($rotulo));

      $str .= "<label> $rotulo :</label><br>".
               "<textarea name='$name'></textarea>";

      $this->setElement($str);	
   }
	
   /*Cria um novo textfield*/
   public function createTextfield( $rotulo, $type ) 
   {
      $str = "";
      $name = str_replace(" ", "_", strtolower($rotulo));
      $type = strtolower($type);

      $str .= "<label> $rotulo :</label><br>".
               "<input name='$name' type='$type' value=><br>";

      $this->setElement($str);
   }

   /*Cria um novo radio HTML*/	
   public function createRadio( $array, $rotulo )
   {
      $str = "";
      $name = str_replace(" ", "_", strtolower($rotulo));

      $str .= "<label>$rotulo:</label><br>";

      foreach( $array as $value)
      {
         $strlow = strtolower($value);
         $str .= "<input type='radio' name='$name' value='$strlow'> $value<br>";
      }
      $this->setElement($str);
   }
   
   /*Cria um novo select HTML*/
   public function createSelect( $array, $rotulo )
   {
      $str = "";	
      $name = str_replace(" ", "_", strtolower($rotulo));
 
      $str .= "<label for='$name' >$rotulo:</label><br><select name='$name'>";
      foreach($array as $key => $value)
      {
         $str .= "<option value='$key'>$value</option>";
      }
	  
      $str .= "</select><br>";
      $this->setElement($str);
   }

   public function createButton($value, $script)
   {
      $str = "";

      if(empty($script))
      {
         $script = "";
      }	

      $str .= "<input type='submit' $script value='$value'><br>";
      $this->setElement($str);
   }

   /*Imprimir os elementos do formulario*/
   public function printForm()
   {
      foreach( $this->form_content as $key => $element)
          echo $element;
   
      echo "</form>";
   }
}
?>
