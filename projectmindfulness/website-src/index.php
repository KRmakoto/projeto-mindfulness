<?php
/******************************************************************************/
/*                            Documento Index                                 */
/******************************************************************************/
require "core/auth.php";

$type = checkAuth(1);
userType($type);
?>

<!DOCTYPE html>
<html>

<head>
   <title> MINDFULNESS </title>

   <meta charset="utf-8">

   <meta name="description"
         content="Site de Login para acesso aos Professores e Administradores">

   <meta name="author"
         content="Kourei Makoto">
   <!--CSS SEGMENTADO PARA DESENVOLVIMENTO - Em producao utilizar apenas um-->
   <!--Isto reduz o http request -->
   <link rel="stylesheet" type="text/css" href="style/global.css">
   <link rel="stylesheet" type="text/css" href="style/header.css">
   <link rel="stylesheet" type="text/css" href="style/form.css">
   <link rel="stylesheet" type="text/css" href="style/footer.css">
</head>

<body>
   <section class="box_section center relative">
      <!--CABECALHO COM AVISO-------------------------------------------------->
      <header>
        <div class="center alert font">
           <p>AVISO: Professor não cadastrado no sistema deve contactar com ???</p>
        </div> 
      </header>
     
      <!--AREA DE LOGIN-------------------------------------------------------->
      <section class='half_w'>
         <div id='login' class='half_h font'>
            <form method='POST' action='core/login.php'>

               <label>LOGIN:</label><br>
                  <input class='form_field' type='text'     name='login'  placeholder='login'><br>
               <label>SENHA:</label><br>
                  <input class='form_field' type='password' name='passwd' placeholder='password'><br>
               <input type='submit'   value='ACESSAR'>

            </form>
         </div>
      </section>

      <!--CABECALHO COM AVISO-------------------------------------------------->
      <footer class='fixed'>
         <div id='ifms' class='font'>
            <a  href='http://ifms.edu.br'>IFMS - CAMPUS TRÊS LAGOAS</a>			
         </div>
      </footer>
   </section>
</body>
</html>
