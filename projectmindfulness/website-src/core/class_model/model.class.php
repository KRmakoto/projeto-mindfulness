<?php
  /*
    Esta classe possui a função de servir como o model no modelo mvc, ou uma 
    abstração de uma manipulação de tabela no banco de uma forma genérica, como
    um framework.

    Atributos privados:
      object $db    = object $DB::__contruct();
      string $table = 'table_name';

    Metodos publicos:
      array Model::select( array $column=null, array $opt[array types = array()] = null )
      bool  Model::insert()
      bool  Model::update()
      boll  Model::delete()

   */
class Model
{
   //ATTRIBUTE
   private $db;
   private $table;


   // CONSTRUCTOR METHOD
   public function __construct( $db, $table )
   {
      $this->db    = $db;
      $this->table = $table;

      //verify if this table $table exist in the database
      if( !$this->db->existTable( $table ) )
         throw new Exception("WARNING. Table $table not exist.");
       
   }

   /***************************************************************************/
   /*   Metodo select tem como função passar o nome de um atributo da tabela  */
   /*  registrada no banco através de um array[], se passar atributos como    */
   /*  vazio, entra na operação padrão e retorna '*', todos seus elementos    */
   /*                                                                         */
   /* Atributos do metodo:                                                    */
   /*   array $col = ( 'column1', 'column2', ... , 'columnN )                 */
   /*   array $opt = ( 'option1', '( AND, OR, LIKE, = )' , 'value1', ...      */
   /*   array $add_table = ( 'table2', 'table3', ... , 'tableN')              */ 
   /**************************************************************************s*/
   public function select( $col = null , $opt = null, $add_table = null, $conditions = null , $AS = null)
   { 
      //Prepara a $col retirando formato de vetor e acrescentando virgula
      if( !empty( $col ) )
      {
           //NAO IMPLEMENTADO
      }
      else //Se não, prepara para receber todos os elementos
      {
         $col = '*';
      }

      //Perepara a $opt e verifica se nao há algumas irregularidades, 
      //como adições de condicionais logicos em posições inapropriadas
      if( !empty( $opt ))
      {
         $conditions = "WHERE ";
         $opt_size = sizeof($opt);
         for( $i = $j = 0 ; $i < $opt_size ; $i++, $j++ )
         {
            if( $i % 2 == 0 )
            {
               if( !in_array( $opt[$i], ['and','or','like', '=']) )
               {
                  $conditions .= " {$opt[$i]}";
               }
            }
            else
            {
               if(  in_array( $opt[$i], ['and','or','like', '=']) )
               {
                  $conditions .= " {$opt[$i]}";
               } 
               else
               {
                   echo 'nao vai dar certo';
               }
            }
         }   
      }
   
 
      //Prepara novas tabelas, e se for mais de uma retira do vetor
      if( !empty($add_table) )
      {
         if( is_array($add_table) )
         {
            $add_table = "," . implode( ",", $add_table );
         }
         else
         {
            $add_table = "," .$add_table;
         }
      }

      $sql = "SELECT $col FROM {$this->table} {$AS} $add_table {$conditions}";

      return( $this->db->query( $sql ));
   }



   /***************************************************************************/
   /*  Metodo insert tem como função receber uma opção de inserção por $opt   */
   /*  juntamente com os atributos apontados para seus valores que serão in-  */
   /*  seridos no banco como como INSERT do SQL                               */
   /*                                                                         */
   /* Atributos do metodo:                                                    */
   /*   array $attr= ( "column1"=>"value1", ... , "columnN"=>"valueN" )       */
   /*   array $opt = ( "option", "")                                          */
   /***************************************************************************/
   public function insert( $attr, $opt = null )
   {
      $keys  = implode( ","  , array_keys($attr) );
      $value = implode( ",", array_values($attr) );
	
      $sql   = "INSERT INTO {$this->table} ($keys) VALUES ($value)";
      return( $this->db->query($sql) );
   }



   /***************************************************************************/
   /*  Metodo update tem como função receber uma opção em $attr do que deve   */
   /*  ser atualizado no banco, para isso deve levar uma associacoa entre a   */
   /*  a chave => valor. Utiliza-se o UPDATE do postgresql.                   */
   /*                                                                         */
   /* Atributos do metodo:                                                    */
   /*   array $id  = ( "id_key"=>"id_value")                                  */
   /*   array $attr= ( "column1"=>"value1", ... , "columnN"=>"valueN" )       */
   /***************************************************************************/
   public function update( $id, $attr )
   {
      //Qualquer valor vazio ja encerra funcao
      if( empty( $id ) || empty($attr) ) 
      {
         return(false);
      }

      //Variaveis
      $size_t     = sizeof($attr);
      $conditions = null;    
      $id_key     = implode( "", array_keys($id)   );
      $id_value   = implode( "", array_values($id) ); 

      if(count($id_key) != count($id_value))
      {
         return(false);
      }

      //Pepara as condicoes que serao modificadas
      for( $i = 0 ; $i < $size_t ; $i++ )
      {   
         //Retira todos as chaves do array e retorna para um tmp
         $tmp_key   = array_keys( $attr ); 
         //Retira todos os elementos que possuem chaves e retorna para um tmp
         $tmp_value = array_values( $attr );
 
         //http://php.net/manual/pt_BR/function.end.php
         //Faz o ponteiro interno de um array apontar para o seu ultimo elemento
         end($tmp_key);
         end($tmp_value);   
   
         //Retira o primeiro elemento do array / 
         $tmp_key     = (string) array_shift($tmp_key);
         $tmp_value   = (string) array_shift($tmp_value );

         $conditions .= (string) " $tmp_key='$tmp_value'";

         //Retira o elemento do array naquela posicao
         //Nao tirar a chave do array, do contrario deletara o array inteiro
         unset($attr[$tmp_key]); 

         if( $i < $size_t - 1)
         {
            $conditions .= ",";
         }
      }

      $sql = "UPDATE {$this->table} SET $conditions  WHERE $id_key = $id_value";
      return( $this->db->query($sql));
   }
 
   public function delete( $id )
   {
      if( $id['id'] <= 0 )
      {
         return(false);
      }

      $key   = implode( "", array_keys($id));
      $value = implode( "", array_values($id));

      $sql = "DELETE FROM {$this->table} WHERE $key = '$value'";
      
      return( $this->db->query($sql) );
   }
}

?>
