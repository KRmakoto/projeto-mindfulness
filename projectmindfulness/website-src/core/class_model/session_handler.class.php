<?php
/*
  This class include in database session mode for connection 

  Attribute:
  -$pg
*/

class UserSessionHandler extends SessionHandler
{
   //ATTRIBUTE------------------------------------------------------------------
   private $pg;
   private $log;
   private $session_data;
   private $conn_query;
   private $siape;
   private $alert;

   //CONSTRUTURES  -  DESTRUTORES  -  DEBUGINFO---------------------------------
   public function __construct($siape = NULL)
   {
      $this->siape = $siape;
      $this->openFile('w');
      fwrite($this->log, "INI::LOG\n");

      //String de conexao
      $this->conn_query = "host=localhost port=5432 dbname=postgres user=root password=root";
   }

   public function __debugInfo()
   {
      return 0;
   }



   //METODOS PRIVADOS-----------------------------------------------------------
   //Abre o arquivo de Log
   private function openFile($type)
   {
      $this->log = fopen("/tmp/handler_log{$this->siape}.txt", $type);
   }

   //Fecha o arquivo de log
   private function closeFile()
   {
      fclose($this->log);
   }

   //Abre uma conexao para trabalhar com as sessoes
   private function openConn()
   {
     //Tenta realizar conexao 
      $this->pg = pg_connect($this->conn_query);
      if( $this->pg === false )
      {
         fwrite($this->log, "\t\tPGSQL::connect: false\n\n");
         $this->dbstatus = "FAILURE"; 
         return false;
      }//Verifica o estado da conexao
      else if(pg_connection_status($this->pg) == PGSQL_CONNECTION_BAD)
      { 
         fwrite($this->log, "\t\tPGSQL::connect_status: PGSQL_CONNECTION_BAD\n\n");
         $this->dbstatus = "FAILURE";
         return false;
      }

      fwrite($this->log, "\t\tPGSQL::connect: true\n\n");
   }

   //Fecha a conexao aberta
   private function closeConn()
   {
      return pg_close($this->pg);
   }

   //SOBRECARGAS----------------------------------------------------------------
   //SessionHandler::open()
   public function open($save_path ,$session_name)
   {
      //Realiza operacao padrao de abertura
      $rslt = parent::open($save_path, $session_name);
      
      $this->openConn();
  
      //Escreve no arquivo a abertura do open
      fwrite($this->log, "\tHANDLER::OPEN >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n".
                         "\t\t\$return       => $rslt\n\n".
                         "\t\t\$save_path    => $save_path\n".
                         "\t\t\$session_name => $session_name\n\n");        

      if($rslt === true)
      {
         fwrite($this->log, "SESSAO NAO EXISTE");
         return $this->alert = true;
      }
      else
      {
         return $this->alert = false;
      }
   }

   //SessionHandler::close()
   public function close()
   {
      //Realiza operacao padrao de fechamento
      $rslt = parent::close();

      //Escreve no arquivo a abertura do close
      fwrite($this->log, "\tHANDLER::CLOSE\n".
                         "\t\t\$return       => $rslt\n\n");

      $rslt = $this->closeConn();
      //Se acontecer erro ao fechar conexao
      if (!$rslt)
      {
         fwrite($this->log,"\t\tPGSQL::close: false\n\n\n");
         
         $this->closeFile();
         
         return $rslt;
      }

      fwrite($this->log,"\t\tPGSQL::close: true\n\n\n");
      $this->closeFile();

      return $rslt;

   } 
   
   //SessionHandler::read()
   public function read($id)
   {
      //Realiza opercao padrao de leitura
      $rslt = parent::read($id);

      //Escreve no arquivo a abertura do read
      fwrite($this->log, "\tHANDLER::READ >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n".
                         "\t\t\$return       => $rslt\n\n".
                         "\t\t\$id           => $id\n\n"); 

      $rslt = pg_query
         (
         $this->pg,
         "SELECT session_data FROM sys_user_session ".
         "WHERE session_id = '$id' AND session_off > CURRENT_TIMESTAMP"
         );

      if($this->session_data = pg_fetch_assoc($rslt))
      {
         fwrite($this->log,"\t\tPGSQL::pg_fetch_assoc:\$return {$this->session_data}\n\n\n");
         return $this->session_data['session_data'];
      }
      else
      {
         fwrite($this->log,"\t\tPGSQL::pg_fetch_assoc:\$return NENHUM DADO\n\n\n");
         return "";
      }
   }

   //SessionHandler::write()
   public function write($id, $data)
   {
      $rows;       //Resultado do numero de linhas
      $rslt;       //Resultado da query
      $rslt_write; //Resulado da operacao write;


      //Realiza opercao padrao de escrita
      $rslt_write = parent::write($id, $data);

      /*REGISTRO*/
      fwrite($this->log, "\tHANDLER::WRITE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n".
                            "\t\t\$return       => $rslt_write\n\n".
                            "\t\t\$id           => $id\n\n"); 

      $rslt = pg_query
         (
         $this->pg,
         "SELECT * FROM sys_user_session".
         " WHERE (session_id LIKE '$id');"
         );
 
      $rows = pg_num_rows($rslt);


      /*REGISTRO*/
      fwrite($this->log, "\t\tPGSQL::pg_query::select:\$return => $rslt\n\n"); 
      

      if($rows == -1)
      {//Caso algum erro ocoreu nao salvara no banco
         /*Gerar no futuro um codigo de mensagem com log no banco*/
         fwrite($this->log,"\t\tPGSQL::pg_num_rows:\$return $rows\n\n\n");
         return $rslt_write;
      }
      else if($rows == 1)
      {//Deverá retornar obrigatoriamente 1. Nao pode haver duas sessoes para um
       //mesmo usuario.
         $rslt = pg_query
           (
           $this->pg,
           "UPDATE sys_user_session ".
           " SET session_off = (CURRENT_TIMESTAMP + (24 ||' minutes')::interval),".
           " session_data = '$data'".
           " WHERE session_id LIKE '$id'"
         );

         fwrite($this->log,"\t\tPGSQL::pg_query::update:\$return $rslt\n\n\n");
      }
      else
      {//Se nao houver erro nem tupla para esta sessao, cria-se uma
         $rslt = pg_query
            (
            $this->pg,
            "INSERT INTO sys_user_session(session_id, session_on, session_off, session_data)".
            "VALUES('$id',  CURRENT_TIMESTAMP, CURRENT_TIMESTAMP + (24 ||' minutes')::interval, '$data');"
            );

         fwrite($this->log,"\t\tPGSQL::pg_query::insert:\$return $rslt\n\n\n");
      }

      return $rslt_write;
   }

   //SessionHandler::destroy()
   public function destroy($id)
   {
      //Realiza operacao padrao de destruicao da sessao
      $rslt = parent::destroy($id);

      //Escreve no arquivo a abertura do destroy
      fwrite($this->log,"\tHANDER::DESTROY>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n".
                        "\t\t\$id     => $id\n".
                        "\t\t\$return => $rslt\n\n");

      $db_rslt = pg_query($this->pg, "DELETE FROM sys_user_session WHERE session_id LIKE '$id';");

      if($db_rslt === true)
      {
         fwrite($this->log, "\t\tPGSQL::pg_query: EXCLUIDO\n\n\n");
         return true;
      }    

      fwrite($this->log, "\t\tPGSQL::pg_query: NENHUM EXCLUSAO REALIZADA\n\n\n" );
      return false;
   }


   //SessionHandler::gc() 
   public function gc($maxlifetime)
   {  
      //Realiza operacao padrao do proprio coletor
      $rslt = parent::gc($maxlifetime);

      //Escreve no arquivo a abertura do coletor
      fwrite($this->log,"\tHANDER::GC >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n".
                        "\t\t\$maxlifetime => false\n".
                        "\t\t\$return      => $rslt\n\n");    

      $db_rslt = pg_query("DELETE FROM sys_user_session WHERE session_off <= CURRENT_TIMESTAMP;");

      //Executa comando na shell para deletar arquivos de sessoes antigas
      $exect = "find /tmp/gc -cmin +".ini_get( session.gc_maxlifetime )."-type f | xargs rm";
      $shell_rslt = shell_exec($exect);


      if(is_null($shell_rslt))
      {
         //Escreve no banco que nao ha sessoes para remover
         fwrite($this->log, "\t\tCMD::shell_exec: NENHUM EXCLUSAO REALIZADA\n" );
      }
      if(is_null($db_rslt))
      {
         fwrite($this->log, "\t\tPGSQL::pg_query: NENHUM EXCLUSAO REALIZADA\n\n\n" );
         return false;
      }

      //Escreve o resultado da shell
      fwrite($this->log, "\t\tCMD::shell_exec: $shell_rslt\n" );
      fwrite($this->log, "\t\tPGSQL::pg_query: $db_rslt\n\n\n" );
      //Limpa o buffer da saída da shell
      ob_end_clean();

      return true;
   }
}
?>
