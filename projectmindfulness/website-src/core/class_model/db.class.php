<?php
/*
  This class include in database abstract mode for connection 

  Attribute:
  -$pg
*/

class PGSQL
{
   //ATTRIBUTE
   private $pg;
   private $conn_query;
   private $dbstatus;
   public  $num_rows;

   // CONSTRUCTOR METHOD
   public function __construct( $options )
   {
      /**
       *  Todos os valores tem um resultado padrao para teste que sao:
       *
       *  host: localhost
       *  user: root
       *  pass: root
       *  
       *   O unico valor que nao tem um pre-definido e o nome do banco, que deve
       *  obrigatoria ser passado para construir a classe
       */
       //Verifica se o ip/nome do host existem
      if( !empty( $options['ip_host'] ) )
         $ip_host = $options['ip_host'];
      else
         $ip_host = '127.0.0.1';
       //Verifica usuario do banco
      if( !empty( $options['db_user'] ) )
         $db_user = $options['db_user'];
      else
         $db_user = 'root';
       //Verifica a senha do banco
      if( !empty( $options['db_pass'] ) )
         $db_pass = $options['db_pass'];
      else
         $db_pass = 'root';

       //Recebe o nome do banco
      if( empty($db_name = $options['db_name']) )
      {
         $this->dbstatus = "FAILURE";
      }

       //String de conexao
      $this->conn_query = "host=$ip_host port=5432 dbname=$db_name".
                          " user=$db_user password=$db_pass";

      //Tenta realizar conexao 
      $this->pg = pg_connect($this->conn_query);
      if( $this->pg === false )
      {
         $this->dbstatus = "FAILURE"; 
      }//Verifica o estado da conexao
      else if(pg_connection_status($this->pg) == PGSQL_CONNECTION_BAD)
      {
         $this->dbstatus = "FAILURE";
      }
      
      $this->dbstatus = "SUCCESS";
   }

   //Tenta reconexao
   public function tryConnAgain()
   {
      //Tenta realizar conexao 
      $this->pg = pg_connect($this->conn_query);
      if( $this->pg === false )
      {
         return(-1); 
      }//Verifica o estado da conexao
      else if(pg_connection_status($this->pg) == PGSQL_CONNECTION_BAD)
      {
         return(-2);
      }
  
      return 0;
   }

   //Reseta a conexao
   public function resetConn()
   {
      pg_connection_reset($this->pg); 
   }

    // PUBLIC METHOD
   public function existTable( $table )
   {
      $result = pg_query
         (
         $this->pg,
         "SELECT * FROM pg_catalog.pg_tables WHERE".
         "(schemaname LIKE 'public' AND schemaname != 'information_schema') AND".
         "(tablename LIKE '$table');"
         );

      if( pg_num_rows($result) == 1 )
         return(true);
      else
         return(false);
   }

   public function query( $sql )
   {
      $final = [];

      if( !($result = pg_query( $this->pg, $sql)) )
      {
         return NULL;
      }
      //Compara
      if( $result === true )
        return true;
      else
        while( $row = pg_fetch_assoc($result) )
          $final[] = $row;     

      $this->num_rows = pg_num_rows($result);

      return $final;
    }

   public function close()
   {
      pg_close($this->pg);    
   }

   //GETTERS E SETTERS----------------------------------------------------------
   public function getConnInfo()
   {
      return $this->dbstatus;
   }
   
   public function getConn()
   {
      return $this->pg;
   }
     
}



?>
