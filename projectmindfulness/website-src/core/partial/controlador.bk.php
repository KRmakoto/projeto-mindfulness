<?php
/******************************************************************************/
/*                                  nav-bar                                   */
/******************************************************************************/
?>

<section >
   <section class="container relative">
      <div class="lab_area font">
         <div>

            <form action="user.php?url=CONTROLADOR">
              <label>Laboratório:</label><br>
               <select class="selectbox font" onclick="myFunction()">
                  <option value="none">Escolha a sala que deseja trabalhar</option>
               <?php for($x = 0 ; $x < 5; $x++){ 
                  $user_name = "Prof: Carlos"
               ?>
                  <option value="lab<?=($x+1)?>"><?="Laboratório ".($x + 1)." | $user_name" ?></option>
               <?php } ?>
               </select> 
               <input id="class_request" type="button" value="Selecionar">
            </form>

         </div>  
      </div>      
   </section>

   <hr>
   <div id="txt">
    oiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
   </div>   

   <section id="online_computer"class="container relative">
      <?php
      for( $i = 0 ; $i < 10 ; $i++ )
      {
         $user_ip = "127.0.0.1";
     
      ?>
      <div class="computer_area">
         <div class="computer_item">
            <div class="computer_svg">
            <svg  width="150" height="120">
               <?php/*Bordas do monitor*/?>
               <rect class="screen"    x="9"  y="20" rx="2" ry="2" width="130" height="65" fill-opacity= "0.0";/>
               <?php/*Parte interna da tela do monitor*/?> 
               <rect class="screen_on"x="19" y="30" rx="2" ry="2" width="110" height="45" />
               <?php/*Pe suporte do monitor*/?>
               <rect class="pad"       x="49" y="95" rx="2" ry="2" width="50"  height="20" />
               Este navegador nao possuí suporte para SVG
            </svg>
 
            <svg class="screen_linux absolute" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
            xmlns="http://www.w3.org/2000/svg" height="48" width="60" version="1.1" 
            xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/">

               <metadata class="metadata7">
                  <rdf:RDF>
                     <cc:Work rdf:about="">
                        <dc:format>image/svg+xml</dc:format>
                        <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage"/>
                        <dc:title/>
                     </cc:Work>
                  </rdf:RDF>
               </metadata>
               <g class="layer1" transform="translate(0,-1004.3622)">

               <path 
                  class="path3898" 
                  style="enable-background:accumulate;fill-rule:nonzero;color:#000000;fill:#000000;" 
                  d="m30.53,1006.5c-18.978,0-27.005,17.835-27.09,39.869h54.181c-0.08574-22.034-8.1126-39.869-27.09-39.869z"/>

               <path
                  class="path3898-3"
                  style="enable-background:accumulate;fill-rule:nonzero;color:#000000;fill:#e6e6e6;" 
                  d="m30.53,1024.5c-13.013-13.013-27.005-0.241-27.09,21.793h54.181c-0.08574-22.034-13.994-34.543-27.09-21.793z"/>

               <path
                  class="path3924"
                  style="enable-background:accumulate;fill-rule:nonzero;color:#000000;fill:#000000;" 
                  d="m-161.77,0.41545a12.635,12.635,0,1,1,-25.27,0,12.635,12.635,0,1,1,25.27,0z"
                  transform="matrix(0.59890372,0,0,0.59890372,127.41737,1033.7248)"/>

               <path
                  class="path3924-5"
                  style="enable-background:accumulate;fill-rule:nonzero;color:#000000;fill:#000000;" 
                  d="m-161.77,0.41545a12.635,12.635,0,1,1,-25.27,0,12.635,12.635,0,1,1,25.27,0z"
                  transform="matrix(0.59890372,0,0,0.59890372,142.5517,1033.7248)"/>

               <path
                  class="path3924-6"
                  style="enable-background:accumulate;fill-rule:nonzero;color:#000000;fill:#ffffff;" 
                  d="m-161.77,0.41545a12.635,12.635,0,1,1,-25.27,0,12.635,12.635,0,1,1,25.27,0z"
                  transform="matrix(0.21373503,0,0,0.21373503,57.539597,1031.1843)"/>

               <path
                  class="path3924-6-2"
                  style="enable-background:accumulate;fill-rule:nonzero;color:#000000;fill:#ffffff;" 
                  d="m-161.77,0.41545a12.635,12.635,0,1,1,-25.27,0,12.635,12.635,0,1,1,25.27,0z"
                  transform="matrix(0.21373503,0,0,0.21373503,72.673919,1031.1843)"/>

               <path
                  class="path3980"
                  style="enable-background:accumulate;fill-rule:nonzero;color:#000000;fill:#ff6600;"
                  transform="matrix(0.73279441,0,0,0.73279441,149.2984,1032.8649)" 
                  d="m-152.64,8.4098-4.7164,8.1691-4.7164,8.1691-4.7164-8.1691-4.7164-8.1691,9.4329-1E-7z"/>
               </g>
            </svg>
            </div>
            <hr>

            <h5>
               <?php/*O nome do computador sera o proprio ip*/?>
               <span class="computer_title">IPv4:<?=$user_ip?></span>
            </h5>

            <hr class="back_line">

            <section class="computer_status_table">
               <table class="font">
                  <tr>
                    <td>Portas</td>
                    <td>
                       <div class="icon_status_area">
                           <label class="switch">
                           <input type="checkbox" checked>
                           <span class="slider round"></span>
                           </label>
                        </div>
                    </td>
                  </tr>
               </table>
            </section>
         </div>
      </div>
      <?php }?>
   </section>
</section> 
