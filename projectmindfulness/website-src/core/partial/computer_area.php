<?php
/******************************************************************************/
/*                                  nav-bar                                   */
/******************************************************************************/
require "../auth.php";
require "../class_model/db.class.php";
require "../class_model/model.class.php";

  
      print_r($_GET);
 
      //Abre o banco de dados
      $dbinfo = new PGSQL(['db_name'=>'postgres']);
      if($dbinfo->getConnInfo() === "SUCCESS")
      {
         $computers   = new Model($dbinfo, "computer_status");

         define( 'CONST_RESULT', $computers->select( NULL, NULL, "computer AS c", "WHERE id_computer = c.id AND id_lab = ".
                                                   "(SELECT id FROM laboratory WHERE lab_name LIKE '{$_GET['nav']}')") );
  
         for( $i = 0 ; $i < $dbinfo->num_rows ; $i++  )
         {
            $user_ip = CONST_RESULT[$i]['ip'];

            if( CONST_RESULT[$i]['id_os'] == 1 )
               $OS = 'screen_linux';
            else
               $OS = 'screen_win';	        
      
         ?>
      <div class="computer_area">
         <div class="computer_item">
            <div class="computer_svg">
               <svg  width="150" height="120">
                  <?php/*Bordas do monitor*/?>
                  <rect class="screen"    x="9"  y="20" rx="2" ry="2" width="130" height="65" fill-opacity= "0.0";/>
                  <?php/*Parte interna da tela do monitor*/?> 
                  <rect class="screen_on"x="19" y="30" rx="2" ry="2" width="110" height="45" />
                  <?php/*Pe suporte do monitor*/?>
                  <rect class="pad"       x="49" y="95" rx="2" ry="2" width="50"  height="20" />
                  Este navegador nao possuí suporte para SVG
               </svg>
 
               <svg class="<?=$OS?> absolute " xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
               xmlns="http://www.w3.org/2000/svg" height="48" width="60" version="1.1" 
               xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/"></svg>
            </div>

            <hr>

            <h5>
               <?php/*O nome do computador sera o proprio ip*/?>
               <span class="computer_title">IPv4:<?=$user_ip?></span>
            </h5>

            <hr class="back_line">

            <section class="computer_status_table">
               <table class="font">
                  <tr>
                    <td>Portas</td>
                    <td>
                       <div class="icon_status_area">
                           <label class="switch">
                           <input type="checkbox" checked>
                           <span class="slider round"></span>
                           </label>
                        </div>
                    </td>
                  </tr>
               </table>
            </section>
         </div>
      </div>
      <?php }}?>
