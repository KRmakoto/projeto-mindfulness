<?php
require "auth.php";
require "class_model/db.class.php";
require "class_model/model.class.php";

/*Para atenticar o usuário*/
$auth = checkAuth();
userAdmin($auth);


/*Retirar esta variavel afetas possiveis dependencia dela em outros arquivos*/
$PATHNAME = "user.php";

?>

<!DOCTYPE html>
<html>
<head>
   <title> AREA DE USUARIO </title>

   <meta charset="utf-8">

   <meta name="description"
         content="Area de acesso do usuario">

   <meta name="author"
         content="Kourei Makoto">

   <link rel="stylesheet" type="text/css" href="../style/global.css">
   <link rel="stylesheet" type="text/css" href="../style/header.css">	
   <link rel="stylesheet" type="text/css" href="../style/footer.css">

</head>
<body>
   <section class="box_section center relative">
      <!--CABECALHO COM AVISO-------------------------------------------------->
      <header>
        <div class="center nav font">
           <?php require "partial/nav.php"?>
        </div> 
      </header>
     
      <!--MAIN-------------------------------------------------------->
      <section id="middle_section" class="bar_top_dist">
      <?php
         switch($_GET['url'])
         {
            case "HOME"       :
            {//Chama o codigo referente ao home - com avisos e noticias
               require "partial/home.php";
               break;
            }
            case "REGISTROS"  :
            {//Chama o codigo referente aos registros de atividades realizadas
               require "partial/registros.php";
               break;
            }
            case "CONTROLADOR":
            {//Chama o codigo referente ao controlador de laboratorios
               require "partial/controlador.php";
               break;
            }
            case "USUARIOS"   :
            {//Chama o codigo referente aos usuarios do sistema
               require "partial/usuario.php";
               break;
            }
            default:
            {
               header("Location: user.php?url=HOME"); 
            }
         }
      ?>


      </section>

      <!--CABECALHO COM AVISO-------------------------------------------------->
      <footer>
         <div id='ifms' class='font'>
            <a  href='http://ifms.edu.br'>IFMS - CAMPUS TRÊS LAGOAS</a>			
         </div>
      </footer>
   </section>
<script type="text/javascript" src="../js/test.js"></script>
</body>
</html>

