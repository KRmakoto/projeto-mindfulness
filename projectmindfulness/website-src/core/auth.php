<?php
/******************************************************************************/
/*                        FUNCOES DE AUTENTICACAO                             */
/******************************************************************************/
require "class_model/session_handler.class.php";


function
checkAuth($flag = 0)
{
   $session_handler = new UserSessionHandler();
   session_set_save_handler($session_handler, true);
   
   if(!session_start())
   {
      header("Location: acess_denied.php?flag=alert_start_session");
   }
   else
   {
      if(empty($_SESSION['logon']))
      {
         if($flag == 0 )
             header("Location: ../index.php?flag=retry");

         return "login";
      }
      else if($_SESSION['logon'][0]['user_level'] == 'f')
      {
         return "user";
      }
      else
      {  
         return "admin";
      }
   }
}

function
userAdmin($flag)
{
   if($flag == "user" )
   {
      header("Location: http://www.localhost/website-src/back-src/user.php?flag=acess_denied");
   }
}  

function
userType($type)
{
   if($type == "admin" )
   {
      header("Location: back-src/admin.php?flag=do_not_pass");
   }
   if($type == "user" )
   {
      header("Location: back-src/user.php?flag=do_not_pass");
   }
}  


?>
