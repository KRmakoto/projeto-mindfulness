<?php
/******************************************************************************/
/*                           Nucleo do Webapp                                 */
/******************************************************************************/
require "class_model/db.class.php";
require "class_model/model.class.php";
require "auth.php";

   /**
    * AREA DE LOGIN
    */
   if( (!(empty($_POST['login'])) || !(empty($_POST['passwd'])) ) AND
         (isset($_POST['login'])  ||   isset($_POST['passwd'])) )
   {
      //VARIAVEIS
      $siape;   //Código do Siap
      $passwd;  //Senha digitada
      $dbinfo;  //Conectando com o banco
      $user;    //Informacoes do usuario
      $login;
      $session_handler;

      $siape  = $_POST['login'];          
      $passwd = sha1($_POST['passwd']);             

      $dbinfo = new PGSQL(['db_name'=>'postgres']);
      if($dbinfo->getConnInfo() === "SUCCESS")
      {
         $user   = new Model($dbinfo, "sys_user");
     
         define( 'CONST_RESULT', $user->select( NULL, NULL, NULL, "WHERE (siape LIKE '$siape') AND (passwd LIKE '$passwd')") );
 
         if(empty(CONST_RESULT))
         {
            header("Location: index.php?flag=error_uorp");
         }
         /*else if(CONST_RESULT[0]['status'] === 't')
         {  
         //Gerar uma sessao especial, pois este usuario ja esta lgado, para que ele decida se quer deslogar em suas outras contas
         //header("back-src/user/conflict.php?<<Codigo_do_conflito>>")
         //Depois que resolvido o conflito, fechear sessao do conflito e registrar log no banco

            //Abertura de uma nova sessão
            session_start();

            print_r($user->update(['id' => CONST_RESULT[0]['id']], //WHERE
                                  ['status' => 'f']));             //SET

            unset($_SESSION['logon']);
            session_destroy();

            header("Location: index.php?flag=autooff");
         }*/
         else
         {
            $session_handler = new UserSessionHandler();
            session_set_save_handler($session_handler, true);

            //Abertura de uma nova sessão
            session_start();

            //$_SESSION['logon'] = CONST_RESULT;
            //$_SESSION['logon'][0]['status'] = 't';

            $_SESSION['logon']['id']   = CONST_RESULT[0]['id'];
            $_SESSION['logon']['user'] = CONST_RESULT[0]['user_name'];
            $_SESSION['logon']['msg']  = "CHECKOUT_USER_OK";


            $user->update(['id' => CONST_RESULT[0]['id']], //WHERE
                          ['status' => 't']);              //SET                

            $login = new Model($dbinfo, "sys_user_login");
            $login->insert(['sys_user_id' => CONST_RESULT[0]['id'],
                            'session_on'  =>'CURRENT_TIMESTAMP']);

            $level = CONST_RESULT[0]['user_level'];

            
            if($level === 't')
            {
               header("Location: admin.php");            
            }
            if($level === 'f')
            {
               header("Location: user.php?url=HOME");            
            }           
         }
      }
   }
   else
   {   
      header("Location: index.php?flag=warning");
   } 

?>
