#include "dwit.h"
void dwit_1(char *host, char *comando)
{
	CLIENT *clnt;
	char * *result_1;
#ifndef	DEBUG
	clnt = clnt_create (host, DWIT, DWIT_VERSAO, "udp");
	if (clnt == NULL) {
		clnt_pcreateerror (host);
		exit (1);
	}
#endif	/* DEBUG */

	result_1 = dowhatitell_1(&comando, clnt);
	if (result_1 == (char **) NULL) {
		clnt_perror (clnt, "Problemas ao chamar procedimento remoto.\n");
	}
	else
	{
		printf("Host: %s   Comando: %s\n",host,comando);
    	printf("%s\n",*result_1);
	}
#ifndef	DEBUG
	clnt_destroy (clnt);
#endif	 /* DEBUG */
}
int main (int argc, char *argv[])
{
	char *host;
	char *comando;
	if(argc != 3)
	{
		fprintf(stderr,"Uso: %s host comando\n",argv[0]);
		fprintf(stderr,"Uso: %s host \"comando argumentos\"\n",argv[0]);
		fprintf(stderr,"Uso: %s host 'comando argumentos'\n",argv[0]);
		fprintf(stderr,"Uso: %s host 'comando | comando'\n",argv[0]);
		fprintf(stderr,"Uso: %s host 'comando argumentos | comando argumentos'\n",argv[0]);
		fprintf(stderr,"Uso: %s host 'comando && comando && comando\n",argv[0]);
		exit(1);
	}
	host = argv[1];
	comando = argv[2];
	dwit_1 (host,comando);
	exit (0);
}
