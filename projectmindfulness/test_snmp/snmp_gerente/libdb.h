#ifndef _H_LIBDB
#define _H_LIBDB

#define ERROR(msg) fprintf(stderr, "ERROR : %s | LINE: %d\n\n", (msg), __LINE__)


/*******************************************************************************
 *
 *  @AUTHOR: Talles Henrique brolezzi Fagundes
 *
 *
 *  @INCLUDES: <libpq-fe.h>
 *
 *  @COMPILE : $CC (...) -I/usr/include/postgresql -lpq -std=c99
 *
 *  @COMMENT : Esta biblioteca e uma extencao da libpq-fe ou API do PostgreSQL
 *            cliente em C. O que difere exclusivamente da biblioteca original  
 *            sao apenas nomenclatura das funcoes e validacoes internas ao es-
 *            copo de cada  funcao  e novas  implementacoes  de  tratamento de 
 *            query string e outros e filtragem por simbolos.
 *
 *             Pode  parecer  superficial, mas foi  inspirado  na nomenclatura
 *            utiliza para as funcoes do PostgreSQL no PHP.
 *
 ******************************************************************************/
 

 /*OVERLOAD WITH DEFINE*/

#define pgClose(conn)       PQfinish(conn) /* Recebe um PGconn   (*) */
#define pgClearResult(rslt) PQclear(rslt)  /* Recebe um PGresult (*) */
#define pgNumTuples(rslt)   PQntuples(rslt)/* -----------=---------- */
#define pgNumFields(rslt)   PQnfields(rslt)/* -----------=---------- */



/*CABECALHO DAS FUNCOES*/

void pgForceClose(PGconn * conn);
void pgResultForceClose(PGresult * rslt, const char *query);

char *prepareFields( char *fields, char *values, char simbol );

PGconn   *pgConn(const char *query);
PGresult *pgQuery(PGconn * conn, const char *query);

unsigned int countWord( char *text, char simbol );
unsigned int firstWordInText( char *text, char simbol );


#endif

