#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <stdlib.h>
#include <stdbool.h>
#include <libpq-fe.h>

#ifdef HAVE_WINSOCK_H
#include <winsock.h>
#endif

#define ERROR(msg) fprintf(stderr, "ERROR : %s | LINE: %d\n\n", (msg), __LINE__)

//DEFINE DE CORES
#define RESET   "\033[0m"
#define BLACK   "\033[30m"      /* Black */
#define RED     "\033[31m"      /* Red */
#define GREEN   "\033[32m"      /* Green */
#define YELLOW  "\033[33m"      /* Yellow */

/*******************************************************************************
 *
 *  @AUTHOR: Talles Henrique brolezzi Fagundes
 *
 *
 *  @INCLUDES: <libpq-fe.h>
 *
 *  @COMPILE : $CC (...) -I/usr/include/postgresql -lpq -std=c99
 *
 *  @COMMENT : Esta biblioteca e uma extencao da libpq-fe ou API do PostgreSQL
 *            cliente em C. O que difere exclusivamente da biblioteca original  
 *            sao apenas nomenclatura das funcoes e validacoes internas ao es-
 *            copo de cada  funcao  e novas  implementacoes  de  tratamento de 
 *            query string e outros e filtragem por simbolos.
 *
 *             Pode  parecer  superficial, mas foi  inspirado  na nomenclatura
 *            utiliza para as funcoes do PostgreSQL no PHP.
 *
 ******************************************************************************/
 

 /*OVERLOAD WITH DEFINE*/

#define pgClose(conn)       PQfinish(conn) /* Recebe um PGconn   (*) */
#define pgClearResult(rslt) PQclear(rslt)  /* Recebe um PGresult (*) */
#define pgNumTuples(rslt)   PQntuples(rslt)/* -----------=---------- */
#define pgNumFields(rslt)   PQnfields(rslt)/* -----------=---------- */



/*CABECALHO DAS FUNCOES*/

void pgForceClose(PGconn * conn);
void pgResultForceClose(PGresult * rslt, const char *query);

char *prepareFields( char *fields, char *values, char simbol );

PGconn   *pgConn(const char *query);
PGresult *pgQuery(PGconn * conn, const char *query);

unsigned int countWord( char *text, char simbol );
unsigned int firstWordInText( char *text, char simbol );



/*ESCOPO DAS FUNCOES*/

/**
 *  @RETURN     : VOID
 *  @FUNCNAME   : pgForceClose
 *  @PARAMETERS : ( 1 )
 *  ............. @TYPE : PGconn (*)  #Caminho da conexao com o banco 
 *
 *  @COMMENT    : Funciona  semelhante a  funcao  PQfinish(),  no  entanto ela 
 *               forca o fechamento do programa, alem do fechamento da conexao.
 *   
 *                Esta funcao deve ser invocada quando o erro ocorrido for fa-
 *               tal para a ocorrencia do programa toda, entrando na categoria
 *               de erros fatais. 
 */

void
pgForceClose(PGconn * conn)
{   
  //Invoca a funcao de encerramento de conexao do libpq-fe
  PQfinish(conn);

  exit(EXIT_FAILURE);
}



/**
 *  @RETURN     : PG conn (*)
 *  @FUNCNAME   : pgConn
 *  @PARAMETERS : ( 1 )
 *  ............. @TYPE : const char (*) #Query string dos parametros do banco
 *
 *  @COMMENT    : Semelhante a  funcao &PQconnectdb(), sendo a funcao da libpq 
 *               que realiza uma conexao atraves de um query string contendo 
 *               as informacoes de conexao de do banco.
 *
 *                O compotamento e exatamento o mesmo da original, com uma ve-
 *               rificao  de erro em caso  de conexoes nao estabelicidades com 
 *               sucesso.
 *
 *                * CONNECTION_BAD : Caso entre neste  estado, algum parametro
 *                  de  conexao nao  foi escrito com  sucesso o que executa um 
 *                  erro fatal e invoca a &pgForceClose() e encerra o programa
 *                  imediamente.
 */

PGconn *
pgConn(const char *query)
{
  /*Variaveis*/
  PGconn 
   *conn;
 
  /*Operacoes*/
  //  Executa conexao via query string.  Tem outros metodos, precisa cuidar ao 
  //invocar esta funcao.
  conn = PQconnectdb(query);

  if(PQstatus(conn) == CONNECTION_BAD)
    {
    fprintf
      (
      stderr,
      "Erro fatal na tentativa de conexao com o banco : ERRO := %s\n",
      PQerrorMessage(conn)
      );
    pgForceClose(conn);
    }

  return conn;
}



/**
 *  @RETURN     : VOID
 *  @FUNCNAME   : pgResultForceClose
 *  @PARAMETERS : ( 2 )
 *  ............. @TYPE [1]: PGresult (*)   #Os resultados do &pgQuery
 *  ............. @TYPE [2]: const char (*) #Query usada na &pgQuery
 *
 *  @COMMENT    : Funcao  invocada apenas em  caso de erros fatais para o fun-
 *               cionamento  do codigo.  Estes erros exigem o encerramento to-
 *               tal da aplicacao.
 *
 *                * PGRES_EMPTY_QUERY : Caso  seja  passado  uma  query string 
 *                  vazia, pode ser recomendado executar esta funcao.
 *
 *                * PGRES_FATAL_ERROR : Este  e erro  na formatacao  da query,
 *                  pode ser aconselhavel encerrar ou tratar com cautela.
 */
 
void
pgResultForceClose(PGresult * rslt, const char *query)
{
   fprintf
      (
      stderr,
      "Falha no comando enviada ao banco:\n"
      "\tCMD  :=>> %s <<\n"
      "\tERROR:=>> %s <<\n",
      query, PQresultErrorMessage(rslt)
      );

  //Limpa qualquer registro de retorno que supostamente possa ter ocorrido
  PQclear(rslt);
  
  exit(EXIT_FAILURE);
}



/**
 *  @RETURN     : RGresult (*)
 *  @FUNCNAME   : pgQuery
 *  @PARAMETERS : ( 2 )
 *  ............. @TYPE [1]: PGconn (*)     #Caminho da conexao com o banco
 *  ............. @TYPE [2]: const char (*) #Query que deseja executar
 *
 *  @COMMENT    : Funcao que  invoca &PQexec da  libpq-fe e trata alguns erros 
                 mais graves.
 *
 *                * PGRES_EMPTY_QUERY : Caso  seja  passado  uma  query string 
 *                  vazia, pode ser recomendado executar esta funcao.
 *
 *                * PGRES_FATAL_ERROR : Este  e erro  na formatacao  da query,
 *                  pode ser aconselhavel encerrar ou tratar com cautela.
 */

PGresult *
pgQuery(PGconn * conn, const char *query)
{
  //Variaveis
  PGresult 
   *rslt;    //Ponteiro para o resultado o banco
 
  ExecStatusType
    status;  //Resultado da query do banco

  rslt   = PQexec(conn, query);
 
  //Recebe o estado da operacao com PQexec
  status = PQresultStatus(rslt);  

  switch(status)
    {
    case PGRES_EMPTY_QUERY :
      { 
      pgResultForceClose(rslt, query);
      pgForceClose(conn);
      }
    case PGRES_FATAL_ERROR :
      {
      pgResultForceClose(rslt, query);
      pgForceClose(conn);
      }
    default:
      return rslt; 
    }
}



/**
 *  @RETURN     : unsigned int
 *  @FUNCNAME   : countWord
 *  @PARAMETERS : ( 2 )
 *  ............. @TYPE [1]: char (*) #O vetor para contagem
 *  ............. @TYPE [2]: char     #Simbolo que nao deve ser contado
 *
 *  @COMMENT    : Percorre o parametro TYPE [1] contando todos os caracteres
 *               nele presente menos o simbolo[caractere] contido no para-
 *               metro TYPE [2].
 *                
 *                Esta funcao encerra em \n ou \0. Cuidado nesta parte que a 
 *               funcao ainda nao esta preparada para WIN, sendo possivel 
 *               se deparar com diferencas de no HEXADECIMA do \n.

 *                Para o WIN pode se utilizar nova linha ou avanco de linha
 *               que em ASCII (0A HEX).
 *             
 */

unsigned int
countWord( char *text, char simbol )
{
  /* Variaveis */
  char
   *buf = text;

  int
    i,
    value;

  /* Operacoes */
  for( value = i = 0 ; buf[i] != 0x00 ; i++ )
    if(( (buf[i+1] == simbol) || ( buf[i+1] == 0x20 ) || ( buf[i+1] == 0x00 ) ))
      value++;

   return value;
}



/**
 *  @RETURN     : unsigned int
 *  @FUNCNAME   : firstWordInText
 *  @PARAMETERS : ( 2 )
 *  ............. @TYPE [1]: char (*) #O vetor para contagem
 *  ............. @TYPE [2]: char     #Simbolo que nao deve ser contado
 *
 *  @COMMENT    : Percorre o parametro TYPE [1] contando todos os caracteres
 *               nele presente até o simbolo[caractere] contido no para-
 *               metro TYPE [2] ou \0[0x00].
 */

unsigned int
firstWordInText( char *text, char simbol )
{
  /* Variaveis */
  char
   *buf = text;

  int 
    i,
    value;

  for( value = i = 0 ; ((buf[i] != simbol) || (buf[i] == 0x00)) ; i++ )
    value++;

  return value;
}



/**
 *  @RETURN     : char (*)
 *  @FUNCNAME   : prepareFields
 *  @PARAMETERS : ( 3 )
 *  ............. @TYPE [1]: char (*) #String de campos
 *  ............. @TYPE [2]: char (*) #String dos valores dos campos
 *  ............. @TYPE [3]: char     #Simbolo que separa os campos
 *
 *  @COMMENT    : Percorre o parametro TYPE [1] contando todos os caracteres
 *               nele presente até o simbolo[caractere] contido no para-
 *               metro TYPE [2] ou \0[0x00].
 */

/**
 *   Esta e a ver.1.0 desta funcao, e apesar de estar estavel agora, pode ser 
 *  migrada para uma versa com struct encadeado.
 *
 *
 *  struct fields{
 *    uint8_t id;
 *    size_t  field_s;
 *    size_t  value_s;
 *    size_t  simbol_s;    
 *    char   *field;
 *    char   *value;
 *    char   *simbol;
 *    struct fields *next;
 *  }
 */

char *
prepareFields( char *fields, char *values, char simbol )
{
   /*Variaveis*/
  char 
   *buf_fields = fields,  //Buffer dos campos
   *buf_values = values,  //Buffer dos valores
   *query;

  size_t
    count = 0,
    query_size = 0,
    fields_size,
    word_size_field,
    word_size_value;            //Tamanho da palavra paara alocar


  //Alocacao da query
  fields_size = countWord(fields, simbol);
  
  query_size = sizeof(char) * ( (strlen(fields)+ strlen(values) + (8 * fields_size) ));

  query = (char *) malloc(query_size);

  if(!query)
    {
    ERROR("Erro de Alocacao de memoria");
    exit(EXIT_FAILURE);
    }

  //Limpando a memoria
  memset(query, 0, query_size );

  do{ 
    //Variaveis temporarias
    char
     *word_field,      //Apenas uma palavra dos campos
     *word_value;      //Apenas uma palavra dos valores

    //Verifica os campos e separa da seu simbolo
    word_size_field = (firstWordInText( buf_fields, simbol ) + 1 ) * sizeof(char) ;

    //Alocando memoria para a 'palavra do campo'
    word_field = (char *) malloc( word_size_field );
    if(!word_field)
      {
      ERROR("Erro de Alocacao de memoria");
      exit(EXIT_FAILURE);
      }

    //Limpando memoria 
    memset(word_field, 0,  word_size_field );

    //copia um trecho da memoria referente ao tamanho da palavra
    strncpy(word_field, buf_fields, word_size_field - 1 );

    //Adiciona \0 no final da string
    word_field[word_size_field - 1] = 0x00;

    //Desloca o ponteiro paara a proxima palavra
    buf_fields = (buf_fields + word_size_field  ); 

    //Verifica os valores e separa da seu simbolo -- igual esquema acima
    word_size_value = (firstWordInText( buf_values, simbol ) + 1 ) * sizeof(char) ;

    //Alocando memoria para a 'palavra do valor'
    word_value = (char *) malloc( word_size_value );
    if(!word_value)
      {
      ERROR("Erro de Alocacao de memoria");
      exit(EXIT_FAILURE);
      }
 
    strncpy(word_value, buf_values, word_size_value - 1);
    word_value[word_size_value - 1] = 0x00;
    buf_values = ( buf_values + word_size_value );  
 
    //Inicia a formacao da query
    strcat(query, word_field);

    // Adiciona o = entre o field e o value
    strncat(query, " = ", 3); 

    //Adiciona o value e verifica se chegou no ultimo campo, se nao adiciona ','
    strcat(query, word_value); 
     
    if(count + 1 < fields_size)
      strcat(query, ", ");

    /* Limpa os ponteiros internos */
    free(word_field);
    free(word_value);

    word_field = NULL;
    word_value = NULL;

    count++;
  }while(count < (fields_size));

  query[strlen(query)] = 0x00;

  /* Limpa os ponteiros internos */
  /* Obs.: Nao fazer &free() aqui, para nao desalocar ponteiro  recebido, ape-
     nas deslocar o ponteiro para um NULL */
  buf_fields = NULL;
  buf_values = NULL;
 
  return(query);
}





/*******************************************************************************
 *
 *  @AUTHOR: Talles Henrique brolezzi Fagundes
 *  @AUTHOR: Fabiano Mallmann
 *
 *
 *  @INCLUDES: "libdb.h"
 *             <net-snmp/net-snmp-config.h>
 *             <net-snmp/net-snmp-includes.h>
 *
 *  @COMPILE : $CC (...) -lnetsnmp (PQSQL_PAR)
 *
 *  @COMMENT : Esta biblioteca e uma extensao complementa da biblioteca desen-
 *            volvida por Fabiano Mallmann.
 *
 *             As alteracoes  incluem em  correcoes de memoria,  e migracao da 
 *            API cliente  do banco de dados que  antes era MySQL agora e toda
 *            em PostgreSQL.
 *           
 *            
 *
 ******************************************************************************/

/* DEFINE */

#define MAX_LAB 10  /* Valor maximo de lab - Risco de OVERFLOW : Cuidado  */


/* STRUCTS */

/**
 *  @COMMENT : Esta estrutura representa o modelo da tabela de hosts[computer]
 *            no banco de dados. Sendo esta opera atraves do formato de pilha.
 *
 *             Buscar utilizar  os metodos dela para  tratar da estrutura para
 *            evitar problemas  de memoria, pois seus  metodos ja passaram por 
 *            testes de memory leak.
 */
 
typedef
struct host{
   char *id_host;     /* Computer.id        : INTEGER   */
   char *id_lab;      /* Computer.id_lab    : INTEGER   */
   char *id_os;       /* Computer.id_os     : INTEGER   */
   char *patrimony;   /* Computer.patrimony : VARCHAR[] */
   char *ip_host;     /* Computer.ip        : INET      */
   char *community;   /* Computer.community : VARCHAR[] */
   char *status;      /* Computer.status    : VARCHAR[] */
   struct host *next; /* PTR para proximo host - FINAL = NULL */
} _HOST;


/**
 *  @COMMENT : Esta estrutura  foi retirada do exemplo  fornecido pelo proprio
 *            site e tutorial do SNMP. Nao sofreu nenhuma alteracao.
 */

struct oid{
   const char *Name;
   oid         Oid[MAX_OID_LEN];
   size_t        OidLen;
} oids[] = {
  { ".1.3.6.1.2.1.1.5.0" },   // Nome do host
  { ".1.3.6.1.2.1.1.3.0" },   // Uptime
  { ".1.3.6.1.2.1.1.1.0" },   // Versao do Kernel e Sistema Operaconal
  { NULL }                    // Indica final
};


/* VARIAVEIS GLOBAIS */

int
   number_of_rows;     //Numero de linhas 

/*FIM-VARIAVEIS GLOBAIS*/


/* ESCOPO DAS FUNCOES */
void startSnmp(void);
void synchronous( PGconn *conn, _HOST *hosts);
int saveResult(PGconn * conn, int status, struct snmp_session *sp, struct snmp_pdu *pdu, const char *auxoid, char  *auxid);
bool pgUpdateRegister(PGconn *conn, char *campo, char *valor, char *id_host);

_HOST *initHost(_HOST *hosts);
void   freeHost(_HOST *hosts);
char  *initHostAttribute(char * attr, size_t len );
short  insertHost(char *id, char *idlab, char *idos, char *patri, char *ip, char *comm, char *status, _HOST *hosts );
bool   removeHost(_HOST *hosts);
_HOST *detachHost(_HOST *hosts);
bool   checkLaboratory(PGconn *conn, unsigned int laboratory_number);

/*FIM-ESCOPO DAS FUNCOES*/



/**
 *  @RETURN     : VOID
 *  @FUNCNAME   : startSnmp
 *  @PARAMETERS : ( VOID )
 *
 *  @COMMENT    : Funcao de inicializacao das funcionalidades do SNMP e veri-
 *               fica os OIDs que deseja ser verificados. Caso algum OID nao
 *               exista e assionado um erro FATAL e encerra a aplicacao.
 *
 *                Nao sera permitido o acrescimo de nenhum OID inexistem nas 
 *               verificacoes do protocolo.

 *               * SOCK_STARTUP : utilizar esta inicia o socket para trabalhar
 *                 com o protocolo do SNMP, so deve ser usado no escopo do 
 *                 processo principal, do main, sem desviar o fluxo.
 *
 *               * Maiores detalhes em :
 *                 url:http://net-snmp.sourceforge.net/docs/README.thread.html   
 */

void
startSnmp(void)
{
  /* Variaveis */
  struct oid
   *oid_pointer = oids;   //Ponteiro para os OIDS GLOBAIS

  /* Operacoes */
    SOCK_STARTUP;          //Inicia o Socket para o SNMP
    init_snmp("synchapp");

  //Percorre todos OIDS registrados
  while(oid_pointer->Name) 
    {
    oid_pointer->OidLen = sizeof(oid_pointer->Oid)/sizeof(oid_pointer->Oid[0]);
    if(!read_objid(oid_pointer->Name, oid_pointer->Oid, &(oid_pointer->OidLen)))
      {
      snmp_perror("read_objid");
      exit(EXIT_FAILURE);
      }

      oid_pointer = (oid_pointer+1);
    }
}



/**
 *  @RETURN     : BOOLEAN
 *  @FUNCNAME   : checkLaboratory
 *  @PARAMETERS : ( 2 )
 *  ............. @TYPE [1]: PGconn (*) #Caminho da conexao com o banco
 *  ............. @TYPE [2]: int        #O numero do laboratorio
 *
 *  @COMMENT    : Verifica  no banco se o  laboratory existe antes de permitir
 *               que  as  informacoes  do banco  sejam selecionadas,  evitando 
 *               possiveis falhas.
 */

bool
checkLaboratory(PGconn *conn, unsigned int laboratory_number)
{
  /*Variaveis*/
  PGresult
   *rslt;  //Resultado do PGSQL

  char
   *query; //Query para o banco

  int
    query_size = sizeof(char) * 55;
  
  /* Operacoes */

  if(laboratory_number > MAX_LAB)
    {
    ERROR("Tentativa de acesso com numero excessivo de laboratorios");
    exit(EXIT_FAILURE);
    }

  //Alocacao da query - OBS: 'texto_completo_ate_ID='(49) + num_id(6)
  //Nao se tem expectativa de atingir este numero, podendo ser alterado, podendo
  //no maximo chegar a 50 se nao houver mais que 9 laboratorios 
  query = (char *) malloc( query_size ); 
  if(!query)
    {
    ERROR("Erro de Alocacao de memoria");
    exit(EXIT_FAILURE);
    }  

  //Requerindo se existe ou nao o laboratorio
  /*OBS.: SN evita overflow neste ponto*/
  snprintf(query, query_size, "SELECT EXISTS( SELECT 1 FROM Laboratory WHERE id=%d);", laboratory_number);

  rslt = pgQuery(conn, query);

  free(query);
  query = NULL;

  //Verifica se o resultado da existencia do lab e verdadeiro ou falso
  if( strcmp( PQgetvalue(rslt, 0, 0), "t" ) == 0 )
    {
    pgClearResult(rslt);
    return true;
    }

   printf("%s\n", PQgetvalue(rslt, 0, 0));
   pgClearResult(rslt);

   return false;
}



/**
 *  @RETURN     : _HOST (*)
 *  @FUNCNAME   : initHost
 *  @PARAMETERS : ( 1 )
 *  ............. @TYPE [1]: _HOST (*) #Ponteiro para Inicio de Lista
 *
 *  @COMMENT    : Inicia uma lista encadeada no formato de fila do tipo _HOST
 *               e já verifica erros e falhas de alocações iniciais. 
 *             
 *                Esta funcao tambem ja define NULL no final da fila e ja 
 *               limpa dados de lixo da memoria.
 */

_HOST *
initHost(_HOST *hosts)
{
  hosts = (_HOST *) malloc(sizeof(_HOST));
  if(!hosts)
    {
    ERROR("Erro de alocacao de memoria");
    exit(-1);
    }

  memset(hosts, 0, sizeof(_HOST));
  hosts->next = NULL;

  return hosts;
}



/**
 *  @RETURN     : char (*)
 *  @FUNCNAME   : initHostAttribute
 *  @PARAMETERS : ( 2 )
 *  ............. @TYPE [1]: char (*) #Dado generico do attr do host
 *  ............. @TYPE [2]: char (*) #Tamanho da alocacao
 *
 *  @COMMENT    : Esta funcao realiza uma alocacao generica do tipo char, de
 *               acordo com o tamanho da alocacao, ja faz as verificacoes de 
 *               erro e limpeza do lixo da memoria.
 *
 *                No final disso e adionado o dado passado como referencia o
 *               novo ponteiro alocado. 
 */

char *
initHostAttribute(char * attr, size_t len )
{
  /* Variaveis */
  char
   *new_attr = (char *) malloc(sizeof(char) * len);

  /* Operacoes */
  if(!new_attr)
    {
    ERROR("Erro de alocacao de memoria");
    exit(-1);
    }

   memset(new_attr, 0, len);
   //Manter strNcpy, provem mais segurancao contra overrun
   strncpy(new_attr, attr, len);
 
   return new_attr;
}



/**
 *  @RETURN     : short
 *  @FUNCNAME   : insertHost
 *  @PARAMETERS : ( 8 )
 *  ............. @TYPE [1]: char  (*) #ID do Host
 *  ............. @TYPE [2]: char  (*) #ID do Laboratorio
 *  ............. @TYPE [3]: char  (*) #ID do Sistema Operacional
 *  ............. @TYPE [4]: char  (*) #Numero do Patrimonio
 *  ............. @TYPE [5]: char  (*) #Numero do Endereco IPv4*!
 *  ............. @TYPE [6]: char  (*) #Nome da comunidade
 *  ............. @TYPE [7]: char  (*) #Status resultante da SNMP
 *  ............. @TYPE [8]: _HOST (*) #Ponteiro da cabeca da fila
 *
 *  @COMMENT    : Inserir  no final da  fila um novo  host. Cuidar com a ordem
 *               dos dados passados, todos sao ponteiros  genericos de char no
 *               codigo, mas existe distincao  no banco e pode dar conflito de
 *               insercao. 
 */

short
insertHost(char *id, char *idlab, char *idos, char *patri, char *ip, char *comm, char *status, _HOST *hosts )
{
  /* Variaveis */
  _HOST
   *new = NULL; //Ponteiro para novo host da fila

  /* Operacoes */
  //Novo alocacao de _HOST seguido de seus atributos.
  new = initHost(new);
  new->id_host   = initHostAttribute( id    , (strlen(id)     + 1));
  new->id_lab    = initHostAttribute( idlab , (strlen(idlab)  + 1));
  new->id_os     = initHostAttribute( idos  , (strlen(idos)   + 1));
  new->patrimony = initHostAttribute( patri , (strlen(patri)  + 1));
  new->ip_host   = initHostAttribute( ip    , (strlen(ip)     + 1));
  new->community = initHostAttribute( comm  , (strlen(comm)   + 1));
  new->status    = initHostAttribute( status, (strlen(status) + 1));

  //Se a cabeca estiver vazia, ja adiciona na primeira posicao
  if(hosts->next == NULL)
    {
    hosts->next = new;
    return 0;
    }
  else //Se nao percorre ate achar o primeiro NULL
    {
    _HOST *tmp = hosts;

    while(tmp->next != NULL)
      tmp = tmp->next;

    tmp->next = new;

    return 0;
    }
  return -1;
}



/**
 *  @RETURN     : BOOLEAN
 *  @FUNCNAME   : removeHost
 *  @PARAMETERS : ( 1 )
 *  ............. @TYPE [1]: _HOST (*) #Ponteiro da fila
 *
 *  @COMMENT    : Remove definitivamente da fila e limpa os dados. Retorna
 *               apenas se a operacao fui bem sucedida ou nao, se houver a 
 *               necessidade do dado retirado utilizar &detachHost().
 */

bool
removeHost(_HOST *hosts)
{
  /* Variaveis */
  _HOST
   *gb;

  /* Operacoes */
  if(hosts->next == NULL)
    return false;

  gb          = hosts->next;
  hosts->next = gb->next;

  /* Limpa os ponteiros internos */
  free(gb->status);
  free(gb->community);
  free(gb->ip_host);
  free(gb->patrimony);
  free(gb->id_os);
  free(gb->id_lab);
  free(gb->id_host);
  free(gb);
   
  gb = NULL;

  return true;
}



/**
 *  @RETURN     : _HOST (*)
 *  @FUNCNAME   : removeHost
 *  @PARAMETERS : ( 1 )
 *  ............. @TYPE [1]: _HOST (*) #Ponteiro da fila
 *
 *  @COMMENT    : Esta  retira o dado do  comeco da fila e o retorna como pon-
 *               teiro.
 *
 *                Obs.: Usar &removeHost no dado retornado por esta funcao, do
 *                     contrario este dado nao sera desalocado.
 */

_HOST *
detachHost(_HOST *hosts)
{
  /* Variaveis */
  _HOST
   *detached;

  /* Operacoes */
  if(hosts->next == NULL)
    return NULL;

  detached    = hosts->next;
  hosts->next = detached->next;

  return detached;
}


/**
 *  @RETURN     : VOID
 *  @FUNCNAME   : freeHosts
 *  @PARAMETERS : ( 1 )
 *  ............. @TYPE [1]: _HOST (*) #Ponteiro da CABECA da fila
 *
 *  @COMMENT    : Muito cuidado com esta funcao, ela desaloca por completo a
 *               fila de hosts e limpa seus dados. Nao deve ser usada como a
 *               funcao &free() da <stdlib.h>.
 */

void
freeHosts(_HOST *hosts)
{
  while(removeHost(hosts) == true );
}



/**
 *  @RETURN     : _HOST (*)
 *  @FUNCNAME   : getHost
 *  @PARAMETERS : ( 3 )
 *  ............. @TYPE [1]: PGconn (*) #Caminho da conexao com o banco
 *  ............. @TYPE [2]: _HOST  (*) #Ponteiro da fila (vazia)
 *  ............. @TYPE [3]: int    (*) #Ponteiro da fila
 *
 *  @COMMENT    : Remove definitivamente da fila e limpa os dados. Retorna
 *               apenas se a operacao fui bem sucedida ou nao, se houver a 
 *               necessidade do dado retirado utilizar &detachHost().
 */

_HOST *
getHost(PGconn *conn, _HOST * hosts, int class_number)
{
  /* Variaveis */
  PGresult
   *rslt;           //Resultado dos computadores da sala;

  int
    count,          //Contador para numero de linhas
    query_size,     //Tamanho da query string
    tuples_number;  //Numero de Tuplas

  char
   *query;


  /* Operacoes */

  query_size = sizeof(char) * 80;
  query = (char *) malloc( query_size ); 
  if(!query)
    {
    ERROR("Alocacao de memoria");
    exit(-1);
    }

  /*Requerindo do banco um SELEC dos computadores cadastrados*/
  snprintf(query, query_size, "SELECT * FROM Computer WHERE id_lab=%d ORDER BY id;", class_number);
  rslt = pgQuery(conn, query);
  
  /*Numero de rows = tuples e column = fields*/
  tuples_number = number_of_rows = pgNumTuples(rslt);

  hosts = initHost(hosts);

  /*Percorre cada tupla de acordo com o numero de usuarios*/
  for(count = 0; count < tuples_number; count ++ )
    {
    short
      check; //Verifica o retorno

    check = insertHost
      (
      PQgetvalue(rslt, count, 0), 
      PQgetvalue(rslt, count, 1), 
      PQgetvalue(rslt, count, 2), 
      PQgetvalue(rslt, count, 3), 
      PQgetvalue(rslt, count, 4),
      PQgetvalue(rslt, count, 5),
      PQgetvalue(rslt, count, 6),
      hosts
      );

      if(check == -1)
        {
        ERROR("Comportamento inesperado o ocorreu");
        exit(-1);
        }     
    }

  pgClearResult(rslt);

  /* Limpa os ponteiros internos */
  free(query);
  query = NULL;

  return hosts;
}



/**
 *  @RETURN     : int
 *  @FUNCNAME   : saveResult
 *  @PARAMETERS : ( 6 )
 *  ............. @TYPE [1]: PGconn              (*) #Caminho da conexao com o banco
 *  ............. @TYPE [2]: int                 (*) #Resultado do SNMP
 *  ............. @TYPE [3]: struct snmp_session (*) #Ponteiro da SESSAO SNMP
 *  ............. @TYPE [4]: struct snmp_pdu     (*) #Ponteiro do PDU SNMP
 *  ............. @TYPE [5]: const char          (*) #Ponteiro do OID
 *  ............. @TYPE [6]: char                (*) #Ponteiro do ID do HOST
 *
 *  @COMMENT    : Ao executar o SNMP &syncronous() gerara um resultado que 
 *               chamara esta funcao para executar o UPDATE! no banco sobre
 *               a situacao ATUAL que se encontra o HOST pesquisado.
 *
 *                Funcao delicada que pode se envolver com diversos erros 
 *               tanto de ponteiro do SNMP quanto os internos. Possui um 
 *               empilhamento de funcoes mais complexo de debbugar.
 *
 *               * STAT_SUCCESS : Caso o SNMP tenha exito na busca com 'status'
 *
 *               * STAT_TIMEOUT : Caso o agente SNMP nao resposta no tempo
 *
 *               * STAT_ERROR : Caso ocorra erro na comunicacao com o agente  
 *
 *               * SNMP_ERR_NOERROR : Caso o SNMP  nao tenha ocorrido  erro no
 *                 seus cabecalho.                  
 */

int 
saveResult(PGconn * conn, int status, struct snmp_session *sp, struct snmp_pdu *pdu, const char *auxoid, char  *auxid)
{
  /* Variaveis */
  char
    buf[1024];

  struct variable_list
   *variable_p;  //Ponteiro para 'struct variables' dentro da 'struct pdu'


  /* Operacoes */

  switch (status)
    {
    case STAT_SUCCESS://CASO OCORREU TUDO CORRETAMENTE -------------------------
      { 
      variable_p = pdu->variables;

      if(pdu->errstat == SNMP_ERR_NOERROR)
        {
        while(variable_p != NULL)
          {
          snprint_variable(buf, 1024, variable_p->name, variable_p->name_length, variable_p);
          printf("\nBUFFER: %s\n\tOID: %s\n\n", buf, auxoid);

          if(strcmp(auxoid, ".1.3.6.1.2.1.1.5.0")==0)
            {
            pgUpdateRegister(conn, "status","'Disponivel'",auxid);
            }
          if(strcmp(auxoid, ".1.3.6.1.2.1.1.3.0")==0)
            {
            pgUpdateRegister(conn, "status","'Disponivel'",auxid);
            }
          if(strcmp(auxoid, ".1.3.6.1.2.1.1.1.0")==0)
            {
            pgUpdateRegister(conn, "status","'Disponivel'",auxid);
            }
          variable_p = variable_p->next_variable;
          }
        }

      return 0;
      }
    case STAT_TIMEOUT:
      {
      printf(YELLOW"\tSTATUS : TIMEOUT"RESET"\n");
      pgUpdateRegister(conn, "status","'Timeout'",auxid);
      return -1;
      }
    case STAT_ERROR:
      {      
      printf(RED"\tSTATUS : ERROR"RESET"\n");
      pgUpdateRegister(conn, "status","'Desconhecido'",auxid);
      snmp_perror(sp->peername);
      return -1;
      }
  }

  return 0;
}



/**
 *  @RETURN     : BOOLEAN
 *  @FUNCNAME   : saveResult
 *  @PARAMETERS : ( 4 )
 *  ............. @TYPE [1]: PGconn (*) #Caminho da conexao com o banco
 *  ............. @TYPE [2]: char   (*) #String dos campos
 *  ............. @TYPE [3]: char   (*) #String dos valores dos campos
 *  ............. @TYPE [4]: char   (*) #ID do HOST
 *
 *  @COMMENT    : Executa um UPDATE no banco para um determinado HOST.                
 *               
 *              Obs.: Esta funcao possui muitas funcoes de concanexao e medi-
 *                   de string, necessario estar sempre atento aos tamanhos.
 */

bool
pgUpdateRegister(PGconn * conn, char * fields, char * values, char * id_host)
{  
  /* Variaveis */
  char 
    init_query[] = "UPDATE Computer SET ",  //Se alterar aqui, altere as strnlen
    last_query[] = " WHERE id = ",
   *into_query,
   *query;

  int
    query_size,
    init_size,
    into_size,
    last_size;


  /* Operacoes */
  //Prepara os campos para UPDATE

  into_query = prepareFields(fields, values, ',');

  query_size = 0;
  init_size = strnlen(init_query, 21); //Se alterar init_query[], alterar aqui
  last_size = strnlen(last_query, 13); //Se alterar last_query[], alterar aqui
  into_size = strnlen(into_query, 1024); 

  query_size = sizeof(char) * (init_size + last_size + into_size + 10);

  query = (char *) malloc(query_size);
  if(!query)
    {
    ERROR("Alocacao de memoria");
    exit(EXIT_FAILURE);
    }

  memset(query, 0, query_size);

  //Monta a query
  strncat(query, init_query, init_size + 1);
  strncat(query, into_query, into_size + 1);
  strncat(query, last_query, last_size + 1);
  strncat(query, id_host, query_size + 1);

  //Requista a query para o banco
  pgQuery(conn, query);

  /* Limpa os ponteiros internos */
  free(into_query);
  free(query);

  into_query = NULL;
  query = NULL;

  return true;
}



/**
 *  @RETURN     : VOID
 *  @FUNCNAME   : synchronous
 *  @PARAMETERS : ( 4 )
 *  ............. @TYPE [1]: PGconn (*) #Caminho da conexao com o banco
 *  ............. @TYPE [2]: _HOST  (*) #Ponteiro da cabeca da fila
 *
 *  @COMMENT    : Funcao  chave do programa,  esta varre toda  a rede em busca
 *               dos  Agentes SNMP e  verifica como eles estao e o que retorna
 *               seus respectivos OIDs.
 */

void
synchronous( PGconn *conn, _HOST *hosts)
{
  //Variaveis 
  _HOST
   *hp;  //Ponteiro para os hosts

  int
    i;

  //Percorre cada host para saber se os determinados OID estao operando
  for( hp = hosts->next, i = 0 ; (hp != NULL) && ( i < number_of_rows ); i++, hp = hp->next ) 
    {
    //Estruturas e variaveis internas
    struct snmp_session
       ss,              //Sessao
      *sp;              //Ponteiro da sessao

    struct oid
       *op;

    printf("Lab: %s  - Host:" GREEN " %s " RESET "\n", hp->id_lab, hp->ip_host);

    /* initialize session */
    snmp_sess_init(&ss);  

    ss.version       = SNMP_VERSION_2c;
    ss.peername      = strdup(hp->ip_host);
    ss.community     = (u_char *) strdup(hp->community);
    ss.community_len = strlen((const char *)ss.community);

    if(!(sp = snmp_open(&ss))) 
      {
      snmp_perror("snmp_open");
      continue;
      }	

    for( op = oids; op->Name != NULL ; op = (op+1))
      {
      //Variaveis
      int
         status;  //Se ocorreram ocorretamente as operecoes do SNMP

      struct snmp_pdu
        *req,     //Requisicao enviada
        *resp;    //Resposta do host

      //Prepara o PDU da requisicao
      req = snmp_pdu_create(SNMP_MSG_GET);
      //Prepara a requisicao para um determinado OID
      snmp_add_null_var(req, op->Oid, op->OidLen);
      //Recebe a resposta do host
      status = snmp_synch_response(sp, req, &resp);

      if( saveResult(conn, status, sp, resp, op->Name, hp->id_host) == -1 )
        break;

      //Finaliza a resposta
      snmp_free_pdu(resp);
      }
    //Encerra sessao  
    snmp_close(sp);
    }
}



//MAIN--------------------------------------------------------------------------
int main (int argc, char **argv)
{
  /* Variaveis */
  PGconn 
   *conn;

  int
    laboratory_number = 1;  

  _HOST 
   *hosts;              //Host do laboratorio

  /*Parametros de inicializacao do snmp*/
  netsnmp_ds_toggle_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_QUICK_PRINT);
  netsnmp_ds_toggle_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_PRINT_BARE_VALUE);


  printf(GREEN"\n\n\n"
         "--------------------------------------------------------------------------------\n"
         "|____________________________________________________________________________  |\n"
         "|  ________________________________________________________________________  | |\n"
         "| |  ____________________________________________________________________  | | |\n"
         "| | |  ________Sistema Sincrono SNMP-MINDFULNESS - v.1.0.0 stable______  | | | |\n"
         "| | | |  INSTITUTO FEDERAL DE CIENCIA E TECNOLOGIA - Campus Três Lagoas| | | | |\n"
         "| | | |__________________________________________________________________| | | |\n"
         "| | |______________________________________________________________________| | |\n"
         "| |__________________________________________________________________________| |\n"
         "--------------------------------------------------------------------------------\n"RESET
         "\n\n@Sobre: \n"
         "  Software de monitoramento dos computadores dos laboratorios do campus, atraves\n"
         "do uso do protocolo SNMP. Este sistema dectecta os computadores \'Agentes\', arma-\n"
         "zenando no banco de dados \'PostgreSQL\' a situacao que estes se encontram.\n\n"
             "@Colaboradores:\n" RED 
             " Prof.: Eduardo Hiroshi Nakamura\n"
             " Prof.: Vladimir Piccolo Barcelos\n"
             " Prof.: Edson Castro\n"
             " Aluno: Fabiano Mallmann\n"
             " Aluno: Talles Henrique Brolezzi Fagundes\n"RESET "\n\n"
        "--------------------------------------------------------------------------------\n");



  while("happy")
    {
    /*Inicia o SNMP*/
    printf("\n"RED"INICIANDO SNMP"RESET"\n");
    startSnmp();
    printf(GREEN"INICIANDO COM SUCESSO"RESET"\n\n");

    /*Abertura de conexao com PostgreSQL*/     
    printf(RED"CONECTANDO-SE AO BANCO DE DADOS"RESET"\n");
    if(laboratory_number == 1)
      {
      conn = pgConn
             (
             "host=localhost "
             "port=5432 "
             "dbname=postgres "
             "user=root "
             "password=root"
             );
      }
    printf(GREEN"CONECTADO COM SUCESSO"RESET"\n\n");

    /*Verifica se o labatorio existe*/
    if(checkLaboratory(conn, laboratory_number) == false )
      {
      printf(RED"Reiniciando busca de Agentes SNMP"RESET"\n");
      pgClose(conn);
      laboratory_number = 1;
      continue;
      }

    initHost(hosts); 

    /*pega os computadores cadastrados*/
    hosts = getHost(conn, hosts, laboratory_number );

    printf(YELLOW"------------------------------- Buscando Agentes -------------------------------\n");
    printf("Laboratorio %d"RESET"\n", laboratory_number);
    synchronous(conn, hosts);

    freeHosts(hosts);

    laboratory_number++;
    printf(YELLOW"Proximo Laboratorio"RESET"\n");
    }

   return 0;
}
