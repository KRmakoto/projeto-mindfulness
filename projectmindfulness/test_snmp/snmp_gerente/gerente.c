#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <stdlib.h>
#include <stdbool.h>
#include "libdb.h"

#ifdef HAVE_WINSOCK_H
#include <winsock.h>
#endif



/*******************************************************************************
 *
 *  @AUTHOR: Talles Henrique brolezzi Fagundes
 *  @AUTHOR: Fabiano Mallmann
 *
 *
 *  @INCLUDES: "libdb.h"
 *             <net-snmp/net-snmp-config.h>
 *             <net-snmp/net-snmp-includes.h>
 *
 *  @COMPILE : $CC (...) -lnetsnmp (PQSQL_PAR)
 *
 *  @COMMENT : Esta biblioteca e uma extensao complementa da biblioteca desen-
 *            volvida por Fabiano Mallmann.
 *
 *             As alteracoes  incluem em  correcoes de memoria,  e migracao da 
 *            API cliente  do banco de dados que  antes era MySQL agora e toda
 *            em PostgreSQL.
 *           
 *            
 *
 ******************************************************************************/

/* DEFINE */

#define MAX_LAB 10  /* Valor maximo de lab - Risco de OVERFLOW : Cuidado  */


/* STRUCTS */

/**
 *  @COMMENT : Esta estrutura representa o modelo da tabela de hosts[computer]
 *            no banco de dados. Sendo esta opera atraves do formato de pilha.
 *
 *             Buscar utilizar  os metodos dela para  tratar da estrutura para
 *            evitar problemas  de memoria, pois seus  metodos ja passaram por 
 *            testes de memory leak.
 */
 
typedef
struct host{
   char *id_host;     /* Computer.id        : INTEGER   */
   char *id_lab;      /* Computer.id_lab    : INTEGER   */
   char *id_os;       /* Computer.id_os     : INTEGER   */
   char *patrimony;   /* Computer.patrimony : VARCHAR[] */
   char *ip_host;     /* Computer.ip        : INET      */
   char *community;   /* Computer.community : VARCHAR[] */
   char *status;      /* Computer.status    : VARCHAR[] */
   struct host *next; /* PTR para proximo host - FINAL = NULL */
} _HOST;


/**
 *  @COMMENT : Esta estrutura  foi retirada do exemplo  fornecido pelo proprio
 *            site e tutorial do SNMP. Nao sofreu nenhuma alteracao.
 */

struct oid{
   const char *Name;
   oid         Oid[MAX_OID_LEN];
   int         OidLen;
} oids[] = {
  { ".1.3.6.1.6.3.1.1.5.1" }, //coldStart
  { ".1.3.6.1.2.1.1.5.0" },   // Nome do host
  { ".1.3.6.1.2.1.1.3.0" },   // Uptime
  { ".1.3.6.1.2.1.1.1.0" },   // Versao do Kernel e Sistema Operaconal
  { NULL }                    // Indica final
};


/* VARIAVEIS GLOBAIS */

int
   number_of_rows;     //Numero de linhas 

/*FIM-VARIAVEIS GLOBAIS*/


/* ESCOPO DAS FUNCOES */
void startSnmp(void);
void synchronous( PGconn *conn, _HOST *hosts);
int saveResult(PGconn * conn, int status, struct snmp_session *sp, struct snmp_pdu *pdu, const char *auxoid, char  *auxid);
bool pgUpdateRegister(PGconn *conn, char *campo, char *valor, char *id_host);

_HOST *initHost(_HOST *hosts);
void   freeHost(_HOST *hosts);
char  *initHostAttribute(char * attr, size_t len );
short  insertHost(char *id, char *idlab, char *idos, char *patri, char *ip, char *comm, char *status, _HOST *hosts );
bool   removeHost(_HOST *hosts);
_HOST *detachHost(_HOST *hosts);
bool   checkLaboratory(PGconn *conn, unsigned int laboratory_number);

/*FIM-ESCOPO DAS FUNCOES*/



/**
 *  @RETURN     : VOID
 *  @FUNCNAME   : startSnmp
 *  @PARAMETERS : ( VOID )
 *
 *  @COMMENT    : Funcao de inicializacao das funcionalidades do SNMP e veri-
 *               fica os OIDs que deseja ser verificados. Caso algum OID nao
 *               exista e assionado um erro FATAL e encerra a aplicacao.
 *
 *                Nao sera permitido o acrescimo de nenhum OID inexistem nas 
 *               verificacoes do protocolo.

 *               * SOCK_STARTUP : utilizar esta inicia o socket para trabalhar
 *                 com o protocolo do SNMP, so deve ser usado no escopo do 
 *                 processo principal, do main, sem desviar o fluxo.
 *
 *               * Maiores detalhes em :
 *                 url:http://net-snmp.sourceforge.net/docs/README.thread.html   
 */

void
startSnmp(void)
{
  /* Variaveis */
  struct oid
   *oid_pointer = oids;   //Ponteiro para os OIDS GLOBAIS

  /* Operacoes */
    SOCK_STARTUP;          //Inicia o Socket para o SNMP
    init_snmp("synchapp");

  //Percorre todos OIDS registrados
  while(oid_pointer->Name) 
    {
    oid_pointer->OidLen = sizeof(oid_pointer->Oid)/sizeof(oid_pointer->Oid[0]);
    if(!read_objid(oid_pointer->Name, oid_pointer->Oid, &(oid_pointer->OidLen)))
      {
      snmp_perror("read_objid");
      exit(EXIT_FAILURE);
      }

      oid_pointer = (oid_pointer+1);
    }
}



/**
 *  @RETURN     : BOOLEAN
 *  @FUNCNAME   : checkLaboratory
 *  @PARAMETERS : ( 2 )
 *  ............. @TYPE [1]: PGconn (*) #Caminho da conexao com o banco
 *  ............. @TYPE [2]: int        #O numero do laboratorio
 *
 *  @COMMENT    : Verifica  no banco se o  laboratory existe antes de permitir
 *               que  as  informacoes  do banco  sejam selecionadas,  evitando 
 *               possiveis falhas.
 */

bool
checkLaboratory(PGconn *conn, unsigned int laboratory_number)
{
  /*Variaveis*/
  PGresult
   *rslt;  //Resultado do PGSQL

  char
   *query; //Query para o banco

  int
    query_size = sizeof(char) * 55;
  
  /* Operacoes */

  if(laboratory_number > MAX_LAB)
    {
    ERROR("Tentativa de acesso com numero excessivo de laboratorios");
    exit(EXIT_FAILURE);
    }

  //Alocacao da query - OBS: 'texto_completo_ate_ID='(49) + num_id(6)
  //Nao se tem expectativa de atingir este numero, podendo ser alterado, podendo
  //no maximo chegar a 50 se nao houver mais que 9 laboratorios 
  query = (char *) malloc( query_size ); 
  if(!query)
    {
    ERROR("Erro de Alocacao de memoria");
    exit(EXIT_FAILURE);
    }  

  //Requerindo se existe ou nao o laboratorio
  /*OBS.: SN evita overflow neste ponto*/
  snprintf(query, query_size, "SELECT EXISTS( SELECT 1 FROM Laboratory WHERE id=%d);", laboratory_number);

  rslt = pgQuery(conn, query);

  free(query);
  query = NULL;

  //Verifica se o resultado da existencia do lab e verdadeiro ou falso
  if( strcmp( PQgetvalue(rslt, 0, 0), "t" ) == 0 )
    {
    pgClearResult(rslt);
    return true;
    }

   printf("%s\n", PQgetvalue(rslt, 0, 0));
   pgClearResult(rslt);

   return false;
}



/**
 *  @RETURN     : _HOST (*)
 *  @FUNCNAME   : initHost
 *  @PARAMETERS : ( 1 )
 *  ............. @TYPE [1]: _HOST (*) #Ponteiro para Inicio de Lista
 *
 *  @COMMENT    : Inicia uma lista encadeada no formato de fila do tipo _HOST
 *               e já verifica erros e falhas de alocações iniciais. 
 *             
 *                Esta funcao tambem ja define NULL no final da fila e ja 
 *               limpa dados de lixo da memoria.
 */

_HOST *
initHost(_HOST *hosts)
{
  hosts = (_HOST *) malloc(sizeof(_HOST));
  if(!hosts)
    {
    ERROR("Erro de alocacao de memoria");
    exit(-1);
    }

  memset(hosts, 0, sizeof(_HOST));
  hosts->next = NULL;
}



/**
 *  @RETURN     : char (*)
 *  @FUNCNAME   : initHostAttribute
 *  @PARAMETERS : ( 2 )
 *  ............. @TYPE [1]: char (*) #Dado generico do attr do host
 *  ............. @TYPE [2]: char (*) #Tamanho da alocacao
 *
 *  @COMMENT    : Esta funcao realiza uma alocacao generica do tipo char, de
 *               acordo com o tamanho da alocacao, ja faz as verificacoes de 
 *               erro e limpeza do lixo da memoria.
 *
 *                No final disso e adionado o dado passado como referencia o
 *               novo ponteiro alocado. 
 */

char *
initHostAttribute(char * attr, size_t len )
{
  /* Variaveis */
  char
   *new_attr = (char *) malloc(sizeof(char) * len);

  /* Operacoes */
  if(!new_attr)
    {
    ERROR("Erro de alocacao de memoria");
    exit(-1);
    }

   memset(new_attr, 0, len);
   //Manter strNcpy, provem mais segurancao contra overrun
   strncpy(new_attr, attr, len);
 
   return new_attr;
}



/**
 *  @RETURN     : short
 *  @FUNCNAME   : insertHost
 *  @PARAMETERS : ( 8 )
 *  ............. @TYPE [1]: char  (*) #ID do Host
 *  ............. @TYPE [2]: char  (*) #ID do Laboratorio
 *  ............. @TYPE [3]: char  (*) #ID do Sistema Operacional
 *  ............. @TYPE [4]: char  (*) #Numero do Patrimonio
 *  ............. @TYPE [5]: char  (*) #Numero do Endereco IPv4*!
 *  ............. @TYPE [6]: char  (*) #Nome da comunidade
 *  ............. @TYPE [7]: char  (*) #Status resultante da SNMP
 *  ............. @TYPE [8]: _HOST (*) #Ponteiro da cabeca da fila
 *
 *  @COMMENT    : Inserir  no final da  fila um novo  host. Cuidar com a ordem
 *               dos dados passados, todos sao ponteiros  genericos de char no
 *               codigo, mas existe distincao  no banco e pode dar conflito de
 *               insercao. 
 */

short
insertHost(char *id, char *idlab, char *idos, char *patri, char *ip, char *comm, char *status, _HOST *hosts )
{
  /* Variaveis */
  _HOST
   *new; //Ponteiro para novo host da fila

  /* Operacoes */
  //Novo alocacao de _HOST seguido de seus atributos.
  new = initHost(new);
  new->id_host   = initHostAttribute( id    , (strlen(id)     + 1));
  new->id_lab    = initHostAttribute( idlab , (strlen(idlab)  + 1));
  new->id_os     = initHostAttribute( idos  , (strlen(idos)   + 1));
  new->patrimony = initHostAttribute( patri , (strlen(patri)  + 1));
  new->ip_host   = initHostAttribute( ip    , (strlen(ip)     + 1));
  new->community = initHostAttribute( comm  , (strlen(comm)   + 1));
  new->status    = initHostAttribute( status, (strlen(status) + 1));

  //Se a cabeca estiver vazia, ja adiciona na primeira posicao
  if(hosts->next == NULL)
    {
    hosts->next = new;
    return 0;
    }
  else //Se nao percorre ate achar o primeiro NULL
    {
    _HOST *tmp = hosts;

    while(tmp->next != NULL)
      tmp = tmp->next;

    tmp->next = new;

    return 0;
    }
  return -1;
}



/**
 *  @RETURN     : BOOLEAN
 *  @FUNCNAME   : removeHost
 *  @PARAMETERS : ( 1 )
 *  ............. @TYPE [1]: _HOST (*) #Ponteiro da fila
 *
 *  @COMMENT    : Remove definitivamente da fila e limpa os dados. Retorna
 *               apenas se a operacao fui bem sucedida ou nao, se houver a 
 *               necessidade do dado retirado utilizar &detachHost().
 */

bool
removeHost(_HOST *hosts)
{
  /* Variaveis */
  _HOST
   *gb;

  /* Operacoes */
  if(hosts->next == NULL)
    return false;

  gb          = hosts->next;
  hosts->next = gb->next;

  /* Limpa os ponteiros internos */
  free(gb->status);
  free(gb->community);
  free(gb->ip_host);
  free(gb->patrimony);
  free(gb->id_os);
  free(gb->id_lab);
  free(gb->id_host);
  free(gb);
   
  gb = NULL;

  return true;
}



/**
 *  @RETURN     : _HOST (*)
 *  @FUNCNAME   : removeHost
 *  @PARAMETERS : ( 1 )
 *  ............. @TYPE [1]: _HOST (*) #Ponteiro da fila
 *
 *  @COMMENT    : Esta  retira o dado do  comeco da fila e o retorna como pon-
 *               teiro.
 *
 *                Obs.: Usar &removeHost no dado retornado por esta funcao, do
 *                     contrario este dado nao sera desalocado.
 */

_HOST *
detachHost(_HOST *hosts)
{
  /* Variaveis */
  _HOST
   *detached;

  /* Operacoes */
  if(hosts->next == NULL)
    return NULL;

  detached    = hosts->next;
  hosts->next = detached->next;

  return detached;
}


/**
 *  @RETURN     : VOID
 *  @FUNCNAME   : freeHosts
 *  @PARAMETERS : ( 1 )
 *  ............. @TYPE [1]: _HOST (*) #Ponteiro da CABECA da fila
 *
 *  @COMMENT    : Muito cuidado com esta funcao, ela desaloca por completo a
 *               fila de hosts e limpa seus dados. Nao deve ser usada como a
 *               funcao &free() da <stdlib.h>.
 */

void
freeHosts(_HOST *hosts)
{
  while(removeHost(hosts) == true );
}



/**
 *  @RETURN     : _HOST (*)
 *  @FUNCNAME   : getHost
 *  @PARAMETERS : ( 3 )
 *  ............. @TYPE [1]: PGconn (*) #Caminho da conexao com o banco
 *  ............. @TYPE [2]: _HOST  (*) #Ponteiro da fila (vazia)
 *  ............. @TYPE [3]: int    (*) #Ponteiro da fila
 *
 *  @COMMENT    : Remove definitivamente da fila e limpa os dados. Retorna
 *               apenas se a operacao fui bem sucedida ou nao, se houver a 
 *               necessidade do dado retirado utilizar &detachHost().
 */

_HOST *
getHost(PGconn *conn, _HOST * hosts, int class_number)
{
  /* Variaveis */
  PGresult
   *rslt;           //Resultado dos computadores da sala;

  int
    count,          //Contador para numero de linhas
    query_size,     //Tamanho da query string
    tuples_number,  //Numero de Tuplas
    fields_number;  //Numero de Campos

  char
   *query;


  /* Operacoes */

  query_size = sizeof(char) * 80;
  query = (char *) malloc( query_size ); 
  if(!query)
    {
    ERROR("Alocacao de memoria");
    exit(-1);
    }

  /*Requerindo do banco um SELEC dos computadores cadastrados*/
  snprintf(query, query_size, "SELECT * FROM Computer WHERE id_lab=%d ORDER BY id;", class_number);
  rslt = pgQuery(conn, query);
  
  /*Numero de rows = tuples e column = fields*/
  tuples_number = number_of_rows = pgNumTuples(rslt);
  fields_number = pgNumFields(rslt);

  hosts = initHost(hosts);

  /*Percorre cada tupla de acordo com o numero de usuarios*/
  for(count = 0; count < tuples_number; count ++ )
    {
    short
      check; //Verifica o retorno

    insertHost
      (
      PQgetvalue(rslt, count, 0), 
      PQgetvalue(rslt, count, 1), 
      PQgetvalue(rslt, count, 2), 
      PQgetvalue(rslt, count, 3), 
      PQgetvalue(rslt, count, 4),
      PQgetvalue(rslt, count, 5),
      PQgetvalue(rslt, count, 6),
      hosts
      );

      if(check == -1)
        {
        ERROR("Comportamento inesperado o ocorreu");
        exit(-1);
        }     
    }

  pgClearResult(rslt);

  /* Limpa os ponteiros internos */
  free(query);
  query = NULL;

  return hosts;
}



/**
 *  @RETURN     : int
 *  @FUNCNAME   : saveResult
 *  @PARAMETERS : ( 6 )
 *  ............. @TYPE [1]: PGconn              (*) #Caminho da conexao com o banco
 *  ............. @TYPE [2]: int                 (*) #Resultado do SNMP
 *  ............. @TYPE [3]: struct snmp_session (*) #Ponteiro da SESSAO SNMP
 *  ............. @TYPE [4]: struct snmp_pdu     (*) #Ponteiro do PDU SNMP
 *  ............. @TYPE [5]: const char          (*) #Ponteiro do OID
 *  ............. @TYPE [6]: char                (*) #Ponteiro do ID do HOST
 *
 *  @COMMENT    : Ao executar o SNMP &syncronous() gerara um resultado que 
 *               chamara esta funcao para executar o UPDATE! no banco sobre
 *               a situacao ATUAL que se encontra o HOST pesquisado.
 *
 *                Funcao delicada que pode se envolver com diversos erros 
 *               tanto de ponteiro do SNMP quanto os internos. Possui um 
 *               empilhamento de funcoes mais complexo de debbugar.
 *
 *               * STAT_SUCCESS : Caso o SNMP tenha exito na busca com 'status'
 *
 *               * STAT_TIMEOUT : Caso o agente SNMP nao resposta no tempo
 *
 *               * STAT_ERROR : Caso ocorra erro na comunicacao com o agente  
 *
 *               * SNMP_ERR_NOERROR : Caso o SNMP  nao tenha ocorrido  erro no
 *                 seus cabecalho.                  
 */

int 
saveResult(PGconn * conn, int status, struct snmp_session *sp, struct snmp_pdu *pdu, const char *auxoid, char  *auxid)
{
  /* Variaveis */
  char
    buf[1024];

  struct variable_list
   *variable_p;  //Ponteiro para 'struct variables' dentro da 'struct pdu'


  /* Operacoes */

  switch (status)
    {
    case STAT_SUCCESS://CASO OCORREU TUDO CORRETAMENTE -------------------------
      { 
      variable_p = pdu->variables;

      if(pdu->errstat == SNMP_ERR_NOERROR)
        {
        while(variable_p != NULL)
          {
          snprint_variable(buf, 1024, variable_p->name, variable_p->name_length, variable_p);
          printf("\nBUFFER: %s\n\tOID: %s\n\n", buf, auxoid);

          if(strcmp(auxoid, ".1.3.6.1.2.1.1.5.0")==0)
            {
            pgUpdateRegister(conn, "status","'Disponivel'",auxid);
            }
          if(strcmp(auxoid, ".1.3.6.1.2.1.1.3.0")==0)
            {
            pgUpdateRegister(conn, "status","'Disponivel'",auxid);
            }
          if(strcmp(auxoid, ".1.3.6.1.2.1.1.1.0")==0)
            {
            pgUpdateRegister(conn, "status","'Disponivel'",auxid);
            }
          variable_p = variable_p->next_variable;
          }
        }
      else
        {
        for( ;variable_p != NULL ; variable_p = variable_p->next_variable)
          {
          if(variable_p != NULL)
            {
            snprint_objid(buf, 1024, variable_p->name, variable_p->name_length);
            }
          else
            {
            strcpy(buf, "(none)");
            //pgUpdateRegister(conn, "status", snmp_errstring(pdu->errstat),auxid);
            fprintf(stdout, "%s: %s: %s\n", sp->peername, buf, snmp_errstring(pdu->errstat));
            } 
          } 
        }

      return 0;
      }
    case STAT_TIMEOUT:
      {
      pgUpdateRegister(conn, "status","'Timeout'",auxid);
      return -1;
      }
    case STAT_ERROR:
      {      printf("Deu erro\n");
      pgUpdateRegister(conn, "status","'Desconhecido'",auxid);
      snmp_perror(sp->peername);
      return -1;
      }
  }
      printf("Caiu fora\n");
  return 0;
}



/**
 *  @RETURN     : BOOLEAN
 *  @FUNCNAME   : saveResult
 *  @PARAMETERS : ( 4 )
 *  ............. @TYPE [1]: PGconn (*) #Caminho da conexao com o banco
 *  ............. @TYPE [2]: char   (*) #String dos campos
 *  ............. @TYPE [3]: char   (*) #String dos valores dos campos
 *  ............. @TYPE [4]: char   (*) #ID do HOST
 *
 *  @COMMENT    : Executa um UPDATE no banco para um determinado HOST.                
 *               
 *              Obs.: Esta funcao possui muitas funcoes de concanexao e medi-
 *                   de string, necessario estar sempre atento aos tamanhos.
 */

bool
pgUpdateRegister(PGconn * conn, char * fields, char * values, char * id_host)
{  
  /* Variaveis */
  char 
    init_query[] = "UPDATE Computer SET ",  //Se alterar aqui, altere as strnlen
    last_query[] = " WHERE id = ",
   *into_query,
   *query;

  unsigned
    count;

  int
    query_size,
    init_size,
    into_size,
    last_size;


  /* Operacoes */
  //Prepara os campos para UPDATE

  into_query = prepareFields(fields, values, ',');

  query_size = 0;
  init_size = strnlen(init_query, 21); //Se alterar init_query[], alterar aqui
  last_size = strnlen(last_query, 13); //Se alterar last_query[], alterar aqui
  into_size = strnlen(into_query, 1024); 

  query_size = sizeof(char) * (init_size + last_size + into_size + 10);

  query = (char *) malloc(query_size);
  if(!query)
    {
    ERROR("Alocacao de memoria");
    exit(EXIT_FAILURE);
    }

  memset(query, 0, query_size);

  //Monta a query
  strncat(query, init_query, init_size + 1);
  strncat(query, into_query, into_size + 1);
  strncat(query, last_query, last_size + 1);
  strncat(query, id_host, query_size + 1);

  //Requista a query para o banco
  pgQuery(conn, query);

  /* Limpa os ponteiros internos */
  free(into_query);
  free(query);

  into_query = NULL;
  query = NULL;

  return true;
}



/**
 *  @RETURN     : VOID
 *  @FUNCNAME   : synchronous
 *  @PARAMETERS : ( 4 )
 *  ............. @TYPE [1]: PGconn (*) #Caminho da conexao com o banco
 *  ............. @TYPE [2]: _HOST  (*) #Ponteiro da cabeca da fila
 *
 *  @COMMENT    : Funcao  chave do programa,  esta varre toda  a rede em busca
 *               dos  Agentes SNMP e  verifica como eles estao e o que retorna
 *               seus respectivos OIDs.
 */

void
synchronous( PGconn *conn, _HOST *hosts)
{
  //Variaveis 
  _HOST
   *hp;  //Ponteiro para os hosts

  int
    i;

  //Percorre cada host para saber se os determinados OID estao operando
  for( hp = hosts->next, i = 0 ; (hp != NULL) && ( i < number_of_rows ); i++, hp = hp->next ) 
    {
    //Estruturas e variaveis internas
    struct snmp_session
       ss,              //Sessao
      *sp;              //Ponteiro da sessao

    struct oid
       *op;
    
    char 
       *aux_id;

    printf("Host: %s\n", hp->ip_host);

    /* initialize session */
    snmp_sess_init(&ss);  

    ss.version       = SNMP_VERSION_2c;
    ss.peername      = strdup(hp->ip_host);
    ss.community     = strdup(hp->community);
    ss.community_len = strlen(ss.community);

    if(!(sp = snmp_open(&ss))) 
      {
      snmp_perror("snmp_open");
      continue;
      }	

    for( op = oids; op->Name != NULL ; op = (op+1))
      {
      //Variaveis
      int
         status;  //Se ocorreram ocorretamente as operecoes do SNMP

      struct snmp_pdu
        *req,     //Requisicao enviada
        *resp;    //Resposta do host

      //Prepara o PDU da requisicao
      req = snmp_pdu_create(SNMP_MSG_GET);
      //Prepara a requisicao para um determinado OID
      snmp_add_null_var(req, op->Oid, op->OidLen);
      //Recebe a resposta do host
      status = snmp_synch_response(sp, req, &resp);

      if( saveResult(conn, status, sp, resp, op->Name, hp->id_host) == -1 )
        break;

      //Finaliza a resposta
      snmp_free_pdu(resp);
      }
    //Encerra sessao  
    snmp_close(sp);
    }
}



//MAIN--------------------------------------------------------------------------
int main (void)
{
  /* Variaveis */
  PGconn 
   *conn;

  int
    laboratory_number = 1;  

  _HOST 
   *hosts;              //Host do laboratorio


  /*Parametros de inicializacao do snmp*/
  netsnmp_ds_toggle_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_QUICK_PRINT);
  netsnmp_ds_toggle_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_PRINT_BARE_VALUE);

  while("happy")
    {
    /*Inicia o SNMP*/
    startSnmp();

    /*Abertura de conexao com PostgreSQL*/     
    if(laboratory_number == 1)
      {
      conn = pgConn
             (
             "host=localhost "
             "port=5432 "
             "dbname=postgres "
             "user=root "
             "password=root"
             );
      }

    /*Verifica se o labatorio existe*/
    if(checkLaboratory(conn, laboratory_number) == false )
      {
      printf("RESET SNMP HOST SEARCH LIST\n");
      pgClose(conn);
      laboratory_number = 1;
      continue;
      }

    initHost(hosts); 

    /*pega os computadores cadastrados*/
    hosts = getHost(conn, hosts, laboratory_number );

    synchronous(conn, hosts);

    freeHosts(hosts);

    laboratory_number++;
    sleep(3);
    }

   return 0;
}
