#include <stdlib.h>
#include <stdbool.h>
#include <libpq-fe.h>
#include "libdb.h"

#ifdef HAVE_WINSOCK_H
#include <winsock.h>
#endif


#define ERROR(msg) fprintf(stderr, "ERROR : %s | LINE: %d\n\n", (msg), __LINE__)


/*******************************************************************************
 *
 *  @AUTHOR: Talles Henrique brolezzi Fagundes
 *
 *
 *  @INCLUDES: <libpq-fe.h>
 *
 *  @COMPILE : $CC (...) -I/usr/include/postgresql -lpq -std=c99
 *
 *  @COMMENT : Esta biblioteca e uma extencao da libpq-fe ou API do PostgreSQL
 *            cliente em C. O que difere exclusivamente da biblioteca original  
 *            sao apenas nomenclatura das funcoes e validacoes internas ao es-
 *            copo de cada  funcao  e novas  implementacoes  de  tratamento de 
 *            query string e outros e filtragem por simbolos.
 *
 *             Pode  parecer  superficial, mas foi  inspirado  na nomenclatura
 *            utiliza para as funcoes do PostgreSQL no PHP.
 *
 ******************************************************************************/
 

 /*OVERLOAD WITH DEFINE*/

#define pgClose(conn)       PQfinish(conn) /* Recebe um PGconn   (*) */
#define pgClearResult(rslt) PQclear(rslt)  /* Recebe um PGresult (*) */
#define pgNumTuples(rslt)   PQntuples(rslt)/* -----------=---------- */
#define pgNumFields(rslt)   PQnfields(rslt)/* -----------=---------- */



/*CABECALHO DAS FUNCOES*/

void pgForceClose(PGconn * conn);
void pgResultForceClose(PGresult * rslt, const char *query);

char *prepareFields( char *fields, char *values, char simbol );

PGconn   *pgConn(const char *query);
PGresult *pgQuery(PGconn * conn, const char *query);

unsigned int countWord( char *text, char simbol );
unsigned int firstWordInText( char *text, char simbol );



/*ESCOPO DAS FUNCOES*/

/**
 *  @RETURN     : VOID
 *  @FUNCNAME   : pgForceClose
 *  @PARAMETERS : ( 1 )
 *  ............. @TYPE : PGconn (*)  #Caminho da conexao com o banco 
 *
 *  @COMMENT    : Funciona  semelhante a  funcao  PQfinish(),  no  entanto ela 
 *               forca o fechamento do programa, alem do fechamento da conexao.
 *   
 *                Esta funcao deve ser invocada quando o erro ocorrido for fa-
 *               tal para a ocorrencia do programa toda, entrando na categoria
 *               de erros fatais. 
 */

void
pgForceClose(PGconn * conn)
{   
  //Invoca a funcao de encerramento de conexao do libpq-fe
  PQfinish(conn);

  exit(EXIT_FAILURE);
}



/**
 *  @RETURN     : PG conn (*)
 *  @FUNCNAME   : pgConn
 *  @PARAMETERS : ( 1 )
 *  ............. @TYPE : const char (*) #Query string dos parametros do banco
 *
 *  @COMMENT    : Semelhante a  funcao &PQconnectdb(), sendo a funcao da libpq 
 *               que realiza uma conexao atraves de um query string contendo 
 *               as informacoes de conexao de do banco.
 *
 *                O compotamento e exatamento o mesmo da original, com uma ve-
 *               rificao  de erro em caso  de conexoes nao estabelicidades com 
 *               sucesso.
 *
 *                * CONNECTION_BAD : Caso entre neste  estado, algum parametro
 *                  de  conexao nao  foi escrito com  sucesso o que executa um 
 *                  erro fatal e invoca a &pgForceClose() e encerra o programa
 *                  imediamente.
 */

PGconn *
pgConn(const char *query)
{
  /*Variaveis*/
  PGconn 
   *conn;
 
  /*Operacoes*/
  //  Executa conexao via query string.  Tem outros metodos, precisa cuidar ao 
  //invocar esta funcao.
  conn = PQconnectdb(query);

  if(PQstatus(conn) == CONNECTION_BAD)
    {
    fprintf
      (
      stderr,
      "Erro fatal na tentativa de conexao com o banco : ERRO := %s\n",
      PQerrorMessage(conn)
      );
    pgForceClose(conn);
    }

  return conn;
}



/**
 *  @RETURN     : VOID
 *  @FUNCNAME   : pgResultForceClose
 *  @PARAMETERS : ( 2 )
 *  ............. @TYPE [1]: PGresult (*)   #Os resultados do &pgQuery
 *  ............. @TYPE [2]: const char (*) #Query usada na &pgQuery
 *
 *  @COMMENT    : Funcao  invocada apenas em  caso de erros fatais para o fun-
 *               cionamento  do codigo.  Estes erros exigem o encerramento to-
 *               tal da aplicacao.
 *
 *                * PGRES_EMPTY_QUERY : Caso  seja  passado  uma  query string 
 *                  vazia, pode ser recomendado executar esta funcao.
 *
 *                * PGRES_FATAL_ERROR : Este  e erro  na formatacao  da query,
 *                  pode ser aconselhavel encerrar ou tratar com cautela.
 */
 
void
pgResultForceClose(PGresult * rslt, const char *query)
{
   fprintf
      (
      stderr,
      "Falha no comando enviada ao banco:\n"
      "\tCMD  :=>> %s <<\n"
      "\tERROR:=>> %s <<\n",
      query, PQresultErrorMessage(rslt)
      );

  //Limpa qualquer registro de retorno que supostamente possa ter ocorrido
  PQclear(rslt);
  
  exit(EXIT_FAILURE);
}



/**
 *  @RETURN     : RGresult (*)
 *  @FUNCNAME   : pgQuery
 *  @PARAMETERS : ( 2 )
 *  ............. @TYPE [1]: PGconn (*)     #Caminho da conexao com o banco
 *  ............. @TYPE [2]: const char (*) #Query que deseja executar
 *
 *  @COMMENT    : Funcao que  invoca &PQexec da  libpq-fe e trata alguns erros 
                 mais graves.
 *
 *                * PGRES_EMPTY_QUERY : Caso  seja  passado  uma  query string 
 *                  vazia, pode ser recomendado executar esta funcao.
 *
 *                * PGRES_FATAL_ERROR : Este  e erro  na formatacao  da query,
 *                  pode ser aconselhavel encerrar ou tratar com cautela.
 */

PGresult *
pgQuery(PGconn * conn, const char *query)
{
  //Variaveis
  PGresult 
   *rslt;    //Ponteiro para o resultado o banco
 
  ExecStatusType
    status;  //Resultado da query do banco

  rslt   = PQexec(conn, query);
 
  //Recebe o estado da operacao com PQexec
  status = PQresultStatus(rslt);  

  switch(status)
    {
    case PGRES_EMPTY_QUERY :
      { 
      pgResultForceClose(rslt, query);
      pgForceClose(conn);
      }
    case PGRES_FATAL_ERROR :
      {
      pgResultForceClose(rslt, query);
      pgForceClose(conn);
      }
    default:
      return rslt; 
    }
}



/**
 *  @RETURN     : unsigned int
 *  @FUNCNAME   : countWord
 *  @PARAMETERS : ( 2 )
 *  ............. @TYPE [1]: char (*) #O vetor para contagem
 *  ............. @TYPE [2]: char     #Simbolo que nao deve ser contado
 *
 *  @COMMENT    : Percorre o parametro TYPE [1] contando todos os caracteres
 *               nele presente menos o simbolo[caractere] contido no para-
 *               metro TYPE [2].
 *                
 *                Esta funcao encerra em \n ou \0. Cuidado nesta parte que a 
 *               funcao ainda nao esta preparada para WIN, sendo possivel 
 *               se deparar com diferencas de no HEXADECIMA do \n.

 *                Para o WIN pode se utilizar nova linha ou avanco de linha
 *               que em ASCII (0A HEX).
 *             
 */

unsigned int
countWord( char *text, char simbol )
{
  /* Variaveis */
  char
   *buf = text;

  int
    i,
    value;

  /* Operacoes */
  for( value = i = 0 ; buf[i] != 0x00 ; i++ )
    if(( (buf[i+1] == simbol) || ( buf[i+1] == 0x20 ) || ( buf[i+1] == 0x00 ) ))
      value++;

   return value;
}



/**
 *  @RETURN     : unsigned int
 *  @FUNCNAME   : firstWordInText
 *  @PARAMETERS : ( 2 )
 *  ............. @TYPE [1]: char (*) #O vetor para contagem
 *  ............. @TYPE [2]: char     #Simbolo que nao deve ser contado
 *
 *  @COMMENT    : Percorre o parametro TYPE [1] contando todos os caracteres
 *               nele presente até o simbolo[caractere] contido no para-
 *               metro TYPE [2] ou \0[0x00].
 */

unsigned int
firstWordInText( char *text, char simbol )
{
  /* Variaveis */
  char
   *buf = text;

  int 
    i,
    value;

  for( value = i = 0 ; ((buf[i] != simbol) || (buf[i] == 0x00)) ; i++ )
    value++;

  return value;
}



/**
 *  @RETURN     : char (*)
 *  @FUNCNAME   : prepareFields
 *  @PARAMETERS : ( 3 )
 *  ............. @TYPE [1]: char (*) #String de campos
 *  ............. @TYPE [2]: char (*) #String dos valores dos campos
 *  ............. @TYPE [3]: char     #Simbolo que separa os campos
 *
 *  @COMMENT    : Percorre o parametro TYPE [1] contando todos os caracteres
 *               nele presente até o simbolo[caractere] contido no para-
 *               metro TYPE [2] ou \0[0x00].
 */

/**
 *   Esta e a ver.1.0 desta funcao, e apesar de estar estavel agora, pode ser 
 *  migrada para uma versa com struct encadeado.
 *
 *
 *  struct fields{
 *    uint8_t id;
 *    size_t  field_s;
 *    size_t  value_s;
 *    size_t  simbol_s;    
 *    char   *field;
 *    char   *value;
 *    char   *simbol;
 *    struct fields *next;
 *  }
 */

char *
prepareFields( char *fields, char *values, char simbol )
{
   /*Variaveis*/
  char 
   *buf_fields = fields,  //Buffer dos campos
   *buf_values = values,  //Buffer dos valores
   *query;

  size_t
    count = 0,
    query_size = 0,
    fields_size,
    word_size_field,
    word_size_value;            //Tamanho da palavra paara alocar


  //Alocacao da query
  fields_size = countWord(fields, simbol);
  
  query_size = sizeof(char) * ( (strlen(fields)+ strlen(values) + (8 * fields_size) ));

  query = (char *) malloc(query_size);

  if(!query)
    {
    ERROR("Erro de Alocacao de memoria");
    exit(EXIT_FAILURE);
    }

  //Limpando a memoria
  memset(query, 0, query_size );

  do{ 
    //Variaveis temporarias
    char
     *word_field,      //Apenas uma palavra dos campos
     *word_value;      //Apenas uma palavra dos valores

    //Verifica os campos e separa da seu simbolo
    word_size_field = (firstWordInText( buf_fields, simbol ) + 1 ) * sizeof(char) ;

    //Alocando memoria para a 'palavra do campo'
    word_field = (char *) malloc( word_size_field );
    if(!word_field)
      {
      ERROR("Erro de Alocacao de memoria");
      exit(EXIT_FAILURE);
      }

    //Limpando memoria 
    memset(word_field, 0,  word_size_field );

    //copia um trecho da memoria referente ao tamanho da palavra
    strncpy(word_field, buf_fields, word_size_field - 1 );

    //Adiciona \0 no final da string
    word_field[word_size_field - 1] = 0x00;

    //Desloca o ponteiro paara a proxima palavra
    buf_fields = (buf_fields + word_size_field  ); 

    //Verifica os valores e separa da seu simbolo -- igual esquema acima
    word_size_value = (firstWordInText( buf_values, simbol ) + 1 ) * sizeof(char) ;

    //Alocando memoria para a 'palavra do valor'
    word_value = (char *) malloc( word_size_value );
    if(!word_value)
      {
      ERROR("Erro de Alocacao de memoria");
      exit(EXIT_FAILURE);
      }
 
    strncpy(word_value, buf_values, word_size_value - 1);
    word_value[word_size_value - 1] = 0x00;
    buf_values = ( buf_values + word_size_value );  
 
    //Inicia a formacao da query
    strcat(query, word_field);

    // Adiciona o = entre o field e o value
    strncat(query, " = ", 3); 

    //Adiciona o value e verifica se chegou no ultimo campo, se nao adiciona ','
    strcat(query, word_value); 
     
    if(count + 1 < fields_size)
      strcat(query, ", ");

    /* Limpa os ponteiros internos */
    free(word_field);
    free(word_value);

    word_field = NULL;
    word_value = NULL;

    count++;
  }while(count < (fields_size));

  query[strlen(query)] = 0x00;

  /* Limpa os ponteiros internos */
  /* Obs.: Nao fazer &free() aqui, para nao desalocar ponteiro  recebido, ape-
     nas deslocar o ponteiro para um NULL */
  buf_fields = NULL;
  buf_values = NULL;
 
  return(query);
}




