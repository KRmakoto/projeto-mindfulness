﻿
/*TABELA PARA REGISTRAR OS USUÁRIOS*/
CREATE TABLE SysUser(
   id         SERIAL       PRIMARY KEY NOT NULL,
   siape      VARCHAR(20)  UNIQUE      NOT NULL,
   passwd     VARCHAR(100)             NOT NULL,
   user_level BOOLEAN                  NOT NULL,
   user_name  VARCHAR(40)              NOT NULL
);

SELECT * FROM pg_catalog.pg_tables WHERE schemaname LIKE 'public';
drop table sys_user;

/*TABELA DAS SESSOES DO USUARIO*/
CREATE TABLE SysUserSession(
   id             SERIAL       PRIMARY KEY             NOT NULL,
   session_id     TEXT         UNIQUE                  NOT NULL,
   session_on     TIMESTAMP WITH TIME ZONE             NOT NULL, 
   session_off    TIMESTAMP WITH TIME ZONE             NOT NULL,
   session_data   TEXT         UNIQUE                  NOT NULL
);
   drop table sysusersession
/*TABELA DOS SISTEMAS OPERACIONAIS EM USO ATUALMENTE NOS LABORATORIOS*/
CREATE TABLE OpSystem(
   id SERIAL PRIMARY KEY NOT NULL,
   os_name VARCHAR(15) NOT NULL
);

/*TABELA DOS REGISTROS DE LOGIN DO USUARIO*/
CREATE TABLE SysUserLogin(
   id             SERIAL       PRIMARY KEY             NOT NULL,
   sys_user_id    INTEGER      REFERENCES SysUser(id) NOT NULL,
   session_on     TIMESTAMP WITH TIME ZONE             NOT NULL
);

/*TABELA DE SESSAO DAS AULAS DOS USUARIOS*/
CREATE TABLE Laboratory(
   id       SERIAL      PRIMARY KEY NOT NULL,
   lab_name VARCHAR(20) UNIQUE  NOT NULL,
   id_user  INTEGER REFERENCES SysUser(id),
   logout   TIMESTAMP   WITH TIME ZONE 
);

drop table computer, laboratory;
/*TABELA DOS COMPUTADORES DOS LABORATORIOS*/
CREATE TABLE Computer(
   id   SERIAL PRIMARY KEY NOT NULL,
   id_lab      INTEGER     REFERENCES Laboratory(id) NOT NULL,
   id_os       INTEGER     REFERENCES OpSystem(id),
   patrimony   VARCHAR(9)  NOT NULL,
   ip          INET        NOT NULL,
   community   VARCHAR(10) NOT NULL,
   status      VARCHAR(10) NOT NULL     
);

drop table Computer;
CREATE TABLE Command(
   id SERIAL PRIMARY KEY NOT NULL,
   id_os     INTEGER     REFERENCES OpSystem(id) NOT NULL,
   cmd_name  VARCHAR(11),
   cmd_text  VARCHAR(1000); 
);

CREATE TABLE SysUserCommand(
   id SERIAL PRIMARY KEY NOT NULL,
   id_user   INTEGER     NOT NULL,
   id_comp   INTEGER     NOT NULL,
   id_lab    INTEGER     NOT NULL,
   id_cmd    INTEGER     NOT NULL,
   cmd_start TIMESTAMP   WITH TIME ZONE 
);
/*VIEWS*/
CREATE VIEW selectview AS 
   SELECT 
      distinct on (lab) l.lab_name AS lab,
      CASE WHEN id_user IS NULL THEN 'LIVRE' 
      ELSE u.user_name 
      END as user
   FROM laboratory AS l , sysuser AS u;

CREATE VIEW cmduserview AS
   SELECT
      

/*TRIGGER*/
CREATE OR REPLACE FUNCTION session_disable()	
RETURNS TRIGGER AS $$
BEGIN
   IF
      OLD.session_off <= now()
      THEN
         OLD.session_status := false;
   END IF;
   return OLD;
END;
$$ language 'plpgsql';

CREATE TRIGGER session_disable


/*SELECTS*/  
SELECT * FROM SysUserSession;
SELECT * FROM SysUserLogin;
SELECT * FROM SysUser;
SELECT * FROM Computer ORDER BY id;
SELECT * FROM Laboratory;
SELECT * FROM OpSystem;

/*UPDATE*/
UPDATE laboratory SET id_user = 2 WHERE id = 3;

/*INSERTION*/
INSERT INTO SysUser(siape, passwd, user_level, user_name)
   VALUES ('123', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', true, 'Pedro Henrique'),
          ('111', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', false, 'Flavio Siquei');

INSERT INTO laboratory(lab_name)
   VALUES ('lab1'),
          ('lab2'),
          ('lab3'),
          ('lab4'),
          ('lab5');

INSERT INTO Computer(id_lab, patrimony, ip, community, status)
   VALUES (1, '00000000', '127.0.0.1', 'public', 'offline'),
          (1, '78568000', '10.8.33.1', 'public', 'offline'),
          (1, '80111000', '10.8.33.2', 'public', 'offline'),
          (1, '80112000', '10.8.33.3', 'public', 'offline'),
          (1, '78568000', '10.8.33.4', 'public', 'offline'),
          (1, '80111000', '10.8.33.5', 'public', 'offline'),
          (1, '80112000', '10.8.33.6', 'public', 'offline'),
          (1, '78568000', '10.8.33.7', 'public', 'offline'),
          (1, '80111000', '10.8.33.8', 'public', 'offline'),
          (1, '80112000', '10.8.33.9', 'public', 'offline'),
          (1, '78568000', '10.8.33.10', 'public', 'offline'),
          (1, '80111000', '10.8.33.11', 'public', 'offline'),
          (1, '80112000', '10.8.33.12', 'public', 'offline'),
          (1, '78568000', '10.8.33.13', 'public', 'offline'),
          (1, '80111000', '10.8.33.14', 'public', 'offline'),
          (1, '80112000', '10.8.33.15', 'public', 'offline'),
          (1, '78568000', '10.8.33.16', 'public', 'offline'),
          (1, '80111000', '10.8.33.17', 'public', 'offline'),
          (1, '80112000', '10.8.33.18', 'public', 'offline'),
          (1, '78568000', '10.8.33.19', 'public', 'offline'),
          (1, '80111000', '10.8.33.20', 'public', 'offline'),
          (1, '80112000', '10.8.33.21', 'public', 'offline'),
          (1, '80111000', '10.8.33.22', 'public', 'offline'),
          (1, '80112000', '10.8.33.23', 'public', 'offline'),
          (1, '80111000', '10.8.33.24', 'public', 'offline'),
          (1, '80112000', '10.8.33.25', 'public', 'offline'),
          (1, '80111000', '10.8.33.26', 'public', 'offline'),
          (1, '80112000', '10.8.33.27', 'public', 'offline'),
          (1, '80111000', '10.8.33.28', 'public', 'offline'),
          (1, '80112000', '10.8.33.29', 'public', 'offline'),
          (1, '80111000', '10.8.33.30', 'public', 'offline'),
          (1, '80112000', '10.8.33.31', 'public', 'offline'),
          (1, '80111000', '10.8.33.32', 'public', 'offline'),
          (1, '80112000', '10.8.33.33', 'public', 'offline'),
          (1, '80111000', '10.8.33.34', 'public', 'offline'),
          (1, '80112000', '10.8.33.35', 'public', 'offline'),
          (1, '80111000', '10.8.33.36', 'public', 'offline'),
(1, '80112000', '10.8.33.37', 'public', 'offline'),
(1, '80112000', '10.8.33.38', 'public', 'offline'),
(1, '80112000', '10.8.33.39', 'public', 'offline'),
(1, '80112000', '10.8.33.40', 'public', 'offline'),
(2, '80112000', '10.8.33.41', 'public', 'offline'),
(2, '80112000', '10.8.33.42', 'public', 'offline'),
(2, '80112000', '10.8.33.43', 'public', 'offline'),
(2, '80112000', '10.8.33.44', 'public', 'offline'),
(2, '80112000', '10.8.33.45', 'public', 'offline'),
(2, '80112000', '10.8.33.46', 'public', 'offline'),
(2, '80112000', '10.8.33.47', 'public', 'offline'),
(2, '80112000', '10.8.33.48', 'public', 'offline'),
(2, '80112000', '10.8.33.49', 'public', 'offline'),
(2, '80112000', '10.8.33.50', 'public', 'offline'),
(2, '80112000', '10.8.33.51', 'public', 'offline'),
(2, '80112000', '10.8.33.52', 'public', 'offline'),
(2, '80112000', '10.8.33.53', 'public', 'offline'),
(2, '80112000', '10.8.33.54', 'public', 'offline'),
(2, '80112000', '10.8.33.55', 'public', 'offline'),
(2, '80112000', '10.8.33.56', 'public', 'offline'),
(2, '80112000', '10.8.33.57', 'public', 'offline'),
(2, '80112000', '10.8.33.58', 'public', 'offline'),
(2, '80112000', '10.8.33.59', 'public', 'offline'),
(2, '80112000', '10.8.33.60', 'public', 'offline'),
(2, '80112000', '10.8.33.61', 'public', 'offline'),
(2, '80112000', '10.8.33.62', 'public', 'offline'),
(2, '80112000', '10.8.33.63', 'public', 'offline'),
(2, '80112000', '10.8.33.64', 'public', 'offline'),
(2, '80112000', '10.8.33.65', 'public', 'offline'),
(2, '80112000', '10.8.33.66', 'public', 'offline'),
(2, '80112000', '10.8.33.67', 'public', 'offline'),
(2, '80112000', '10.8.33.68', 'public', 'offline'),
(2, '80112000', '10.8.33.69', 'public', 'offline'),
(2, '80112000', '10.8.33.70', 'public', 'offline'),
(2, '80112000', '10.8.33.71', 'public', 'offline'),
(2, '80112000', '10.8.33.72', 'public', 'offline'),
(2, '80112000', '10.8.33.73', 'public', 'offline'),
(2, '80112000', '10.8.33.74', 'public', 'offline'),
(2, '80112000', '10.8.33.75', 'public', 'offline'),
(2, '80112000', '10.8.33.76', 'public', 'offline'),
(2, '80112000', '10.8.33.77', 'public', 'offline'),
(2, '80112000', '10.8.33.78', 'public', 'offline'),
(2, '80112000', '10.8.33.79', 'public', 'offline'),
(2, '80112000', '10.8.33.80', 'public', 'offline'),
(3, '80112000', '10.8.33.81', 'public', 'offline'),
(3, '80112000', '10.8.33.82', 'public', 'offline'),
(3, '80112000', '10.8.33.83', 'public', 'offline'),
(3, '80112000', '10.8.33.84', 'public', 'offline'),
(3, '80112000', '10.8.33.85', 'public', 'offline'),
(3, '80112000', '10.8.33.86', 'public', 'offline'),
(3, '80112000', '10.8.33.87', 'public', 'offline'),
(3, '80112000', '10.8.33.88', 'public', 'offline'),
(3, '80112000', '10.8.33.89', 'public', 'offline'),
(3, '80112000', '10.8.33.90', 'public', 'offline'),
(3, '80112000', '10.8.33.91', 'public', 'offline'),
(3, '80112000', '10.8.33.92', 'public', 'offline'),
(3, '80112000', '10.8.33.93', 'public', 'offline'),
(3, '80112000', '10.8.33.94', 'public', 'offline'),
(3, '80112000', '10.8.33.95', 'public', 'offline'),
(3, '80112000', '10.8.33.96', 'public', 'offline'),
(3, '80112000', '10.8.33.97', 'public', 'offline'),
(3, '80112000', '10.8.33.98', 'public', 'offline'),
(3, '80112000', '10.8.33.99', 'public', 'offline'),
(3, '80112000', '10.8.33.100', 'public', 'offline'),
(3, '80112000', '10.8.33.101', 'public', 'offline'),
(3, '80112000', '10.8.33.102', 'public', 'offline'),
(3, '80112000', '10.8.33.103', 'public', 'offline'),
(3, '80112000', '10.8.33.104', 'public', 'offline'),
(3, '80112000', '10.8.33.105', 'public', 'offline'),
(3, '80112000', '10.8.33.106', 'public', 'offline'),
(3, '80112000', '10.8.33.107', 'public', 'offline'),
(3, '80112000', '10.8.33.108', 'public', 'offline'),
(3, '80112000', '10.8.33.109', 'public', 'offline'),
(3, '80112000', '10.8.33.110', 'public', 'offline'),
(3, '80112000', '10.8.33.111', 'public', 'offline'),
(3, '80112000', '10.8.33.112', 'public', 'offline'),
(3, '80112000', '10.8.33.113', 'public', 'offline'),
(3, '80112000', '10.8.33.114', 'public', 'offline'),
(3, '80112000', '10.8.33.115', 'public', 'offline'),
(3, '80112000', '10.8.33.116', 'public', 'offline'),
(3, '80112000', '10.8.33.117', 'public', 'offline'),
(3, '80112000', '10.8.33.118', 'public', 'offline'),
(3, '80112000', '10.8.33.119', 'public', 'offline'),
(3, '80112000', '10.8.33.120', 'public', 'offline'),
(4, '80112000', '10.8.33.121', 'public', 'offline'),
(4, '80112000', '10.8.33.122', 'public', 'offline'),
(4, '80112000', '10.8.33.123', 'public', 'offline'),
(4, '80112000', '10.8.33.124', 'public', 'offline'),
(4, '80112000', '10.8.33.125', 'public', 'offline'),
(4, '80112000', '10.8.33.126', 'public', 'offline'),
(4, '80112000', '10.8.33.127', 'public', 'offline'),
(4, '80112000', '10.8.33.128', 'public', 'offline'),
(4, '80112000', '10.8.33.129', 'public', 'offline'),
(4, '80112000', '10.8.33.130', 'public', 'offline'),
(4, '80112000', '10.8.33.131', 'public', 'offline'),
(4, '80112000', '10.8.33.132', 'public', 'offline'),
(4, '80112000', '10.8.33.133', 'public', 'offline'),
(4, '80112000', '10.8.33.134', 'public', 'offline'),
(4, '80112000', '10.8.33.135', 'public', 'offline'),
(4, '80112000', '10.8.33.136', 'public', 'offline'),
(4, '80112000', '10.8.33.137', 'public', 'offline'),
(4, '80112000', '10.8.33.138', 'public', 'offline'),
(4, '80112000', '10.8.33.139', 'public', 'offline'),
(4, '80112000', '10.8.33.140', 'public', 'offline'),
(4, '80112000', '10.8.33.141', 'public', 'offline'),
(4, '80112000', '10.8.33.142', 'public', 'offline'),
(4, '80112000', '10.8.33.143', 'public', 'offline'),
(4, '80112000', '10.8.33.144', 'public', 'offline'),
(4, '80112000', '10.8.33.145', 'public', 'offline'),
(4, '80112000', '10.8.33.146', 'public', 'offline'),
(4, '80112000', '10.8.33.147', 'public', 'offline'),
(4, '80112000', '10.8.33.148', 'public', 'offline'),
(4, '80112000', '10.8.33.149', 'public', 'offline'),
(4, '80112000', '10.8.33.150', 'public', 'offline'),
(4, '80112000', '10.8.33.151', 'public', 'offline'),
(4, '80112000', '10.8.33.152', 'public', 'offline'),
(4, '80112000', '10.8.33.153', 'public', 'offline'),
(4, '80112000', '10.8.33.154', 'public', 'offline'),
(4, '80112000', '10.8.33.155', 'public', 'offline'),
(4, '80112000', '10.8.33.156', 'public', 'offline'),
(4, '80112000', '10.8.33.157', 'public', 'offline'),
(4, '80112000', '10.8.33.158', 'public', 'offline'),
(4, '80112000', '10.8.33.159', 'public', 'offline'),
(4, '80112000', '10.8.33.160', 'public', 'offline'),
(5, '80112000', '10.8.33.161', 'public', 'offline'),
(5, '80112000', '10.8.33.162', 'public', 'offline'),
(5, '80112000', '10.8.33.163', 'public', 'offline'),
(5, '80112000', '10.8.33.164', 'public', 'offline'),
(5, '80112000', '10.8.33.165', 'public', 'offline'),
(5, '80112000', '10.8.33.166', 'public', 'offline'),
(5, '80112000', '10.8.33.167', 'public', 'offline'),
(5, '80112000', '10.8.33.168', 'public', 'offline'),
(5, '80112000', '10.8.33.169', 'public', 'offline'),
(5, '80112000', '10.8.33.170', 'public', 'offline'),
(5, '80112000', '10.8.33.171', 'public', 'offline'),
(5, '80112000', '10.8.33.172', 'public', 'offline'),
(5, '80112000', '10.8.33.173', 'public', 'offline'),
(5, '80112000', '10.8.33.174', 'public', 'offline'),
(5, '80112000', '10.8.33.175', 'public', 'offline'),
(5, '80112000', '10.8.33.176', 'public', 'offline'),
(5, '80112000', '10.8.33.177', 'public', 'offline'),
(5, '80112000', '10.8.33.178', 'public', 'offline'),
(5, '80112000', '10.8.33.179', 'public', 'offline'),
(5, '80112000', '10.8.33.180', 'public', 'offline'),
(5, '80112000', '10.8.33.181', 'public', 'offline'),
(5, '80112000', '10.8.33.182', 'public', 'offline'),
(5, '80112000', '10.8.33.183', 'public', 'offline'),
(5, '80112000', '10.8.33.184', 'public', 'offline'),
(5, '80112000', '10.8.33.185', 'public', 'offline'),
(5, '80112000', '10.8.33.186', 'public', 'offline'),
(5, '80112000', '10.8.33.187', 'public', 'offline'),
(5, '80112000', '10.8.33.188', 'public', 'offline'),
(5, '80112000', '10.8.33.189', 'public', 'offline'),
(5, '80112000', '10.8.33.190', 'public', 'offline'),
(5, '80112000', '10.8.33.191', 'public', 'offline'),
(5, '80112000', '10.8.33.192', 'public', 'offline'),
(5, '80112000', '10.8.33.193', 'public', 'offline'),
(5, '80112000', '10.8.33.194', 'public', 'offline'),
(5, '80112000', '10.8.33.195', 'public', 'offline'),
(5, '80112000', '10.8.33.196', 'public', 'offline'),
(5, '80112000', '10.8.33.197', 'public', 'offline'),
(5, '80112000', '10.8.33.198', 'public', 'offline'),
(5, '80112000', '10.8.33.199', 'public', 'offline'),
(5, '80112000', '10.8.33.200', 'public', 'offline');

UPDATE Computer SET ip = '10.8.33.3' WHERE id = 8


 SELECT  distinct on (lab_name) l.lab_name AS lab, u.user_name AS user FROM Computer AS c, Laboratory AS l , SysUser AS u IF (id_user)

SELECT EXISTS( SELECT 1 FROM Laboratory WHERE id=5)

 

SELECT * FROM selectview