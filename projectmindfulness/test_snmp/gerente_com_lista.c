#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <stdlib.h>
#include <stdbool.h>
#include <libpq-fe.h>

//Teste para implementar com com a versão 3

#ifdef HAVE_WINSOCK_H
#include <winsock.h>
#endif

#define ERROR(msg) fprintf(stderr, "FAILURE := %s : \t\t\t\t\t\tLINE %d\n\n", (msg), __LINE__)


//Fechar conexao e o sistema
void pgForceClose(PGconn * conn)
{   
   PQfinish(conn);
   //Exit nao portavel para WIN - tratar com ifdef
   exit(1);
}

//Fechar a conexao
void pgClose(PGconn * conn)
{   
   PQfinish(conn);
}

//Limpar o result
void pgClearResult(PGresult * rslt)
{
   PQclear(rslt);
}

//Abrir conexao
PGconn * pgConn(const char *query)
{
   PGconn * conn = PQconnectdb(query);

   if(PQstatus(conn) == CONNECTION_BAD)
   {
      fprintf(stderr, "Falha na conexao com o banco de dados: ERRO := %s\n", PQerrorMessage(conn));
      pgForceClose(conn);
   }

   return conn;
}

//Forcar resultado com saida 
void
pgResultForceClose(PGresult * rslt, const char *query)
{
   fprintf
      (
      stderr,
      "Falha no comando enviada ao banco:\n"
      "\tCMD  :=>> %s <<\n"
      "\tERROR:=>> %s <<\n",
      query, PQresultErrorMessage(rslt)
      );
   PQclear(rslt);
}

//Executar comando no banco 
PGresult * pgQuery(PGconn * conn, const char *query)
{
   PGresult * rslt;
   ExecStatusType status;

   rslt = PQexec(conn, query);
   status = PQresultStatus(rslt);  

   switch(status)
   {//Caso retorne query vazia
      case PGRES_EMPTY_QUERY :
      { 
         pgResultForceClose(rslt, query);
         pgForceClose(conn);
      }
      case PGRES_COMMAND_OK  :
      {
         fprintf
            (
            stdout,
            "\tResultado do comando:\n"
            "\t\tCMD  :=>> %s <<\n"
            "\t\tRSTL:=>> PGRES_COMMAND_OK <<\n",
            query
            );
         return(rslt); 
      }//Caso retorne tuples > 1
      case PGRES_TUPLES_OK  :
      {
         fprintf
            (
            stdout,
            "\tResultado do comando:\n"
            "\t\tCMD  :=>> %s <<\n"
            "\t\tRSTL:=>> PGRES_TUPLES_OK <<\n",
            query
            );
         return(rslt); 
      }//Caso retorne apenas um tuple
      case PGRES_SINGLE_TUPLE:
      {
         fprintf
            (
            stdout,
            "\tResultado do comando:\n"
            "\t\tCMD  :=>> %s <<\n"
            "\t\tRSTL:=>> PGRES_SINGLE_TUPLE  <<\n",
            query
            );
         return(rslt); 
      }
      default:
         return(rslt);
   }
}

//Pega o numero de tuple  - ROWS
int pgNumTuples(const PGresult * rslt)
{
   if(rslt == NULL)
   {
      return 0;
   }

   return PQntuples(rslt);
}

//Pega o numero de Fields - COLUMN
int pgNumFields(const PGresult * rslt)
{
   if(rslt == NULL)
   {
      return 0;
   }

   return PQnfields(rslt);
}

/*GERENTE**********************************************************************/
typedef struct host{
   char *id_host;
   char *id_lab;
   char *id_os;
   char *patrimony;
   char *ip_host;
   char *community;
   char *status;
   struct host *next;
} _HOST;


struct oid{
   const char *Name;
   oid   Oid[MAX_OID_LEN];
   int   OidLen;
} oids[] = {
  { ".1.3.6.1.2.1.1.5.0" },   //
  { ".1.3.6.1.2.1.1.3.0" },   //
  { ".1.3.6.1.2.1.1.1.0" },   //
  { NULL }
};


/*VARIAVEIS GLOBAIS*/

int
   number_of_rows;     //Numero de linhas 
/*FIM-VARIAVEIS GLOBAIS*/


/*ESCOPO DAS FUNCOES*/
int pgUpdateRegister(PGconn *conn, char *campo, char *valor, char *id_host);
void startSnmp(void);
/*FIM-ESCOPO DAS FUNCOES*/


/*Funcao que inicia o socket SNMP*/
/*  Como  utiliza-se da  chamada SOCK_STARTUP, utilizar  esta funcao  apenas  no
  escopo da main(). 
  url: http://net-snmp.sourceforge.net/docs/README.thread.html*/
void
startSnmp(void)
{
   struct oid
     *oid_pointer = oids;   //Ponteiro para os OIDS GLOBAIS
     SOCK_STARTUP;          //Inicia o Socket para o SNMP
     init_snmp("synchapp");

   //Percorre todos OIDS registrados
   while (oid_pointer->Name) 
   {
      oid_pointer->OidLen = sizeof(oid_pointer->Oid)/sizeof(oid_pointer->Oid[0]);
      if(!read_objid(oid_pointer->Name, oid_pointer->Oid, &(oid_pointer->OidLen)))
      {
         snmp_perror("read_objid");
         exit(1);
      }
      oid_pointer++;
   }
}

/*Verifica se o laboratorio para este numero de laboratorio*/
bool
checkLaboratory(PGconn *conn, int laboratory_number)
{
  /*Variaveis*/
  PGresult
   *rslt;  //Resultado do PGSQL

  char
   *query; //Query para o banco

  //Alocacao da query - OBS: 55 = 'texto_completo_ate_ID='(49) + num_id(6)
  //Nao se tem expectativa de atingir este numero, podendo ser alterado, podendo
  //no maximo chegar a 50 se nao houver mais que 9 laboratorios 
  query = (char *) malloc( sizeof(char) * (55)); 
  if(!query)
    {
    ERROR("Erro de Alocacao de memoria");
    exit(-1);
    }  

  //Requerindo se existe ou nao o laboratorio
  sprintf(query, "SELECT EXISTS( SELECT 1 FROM Laboratory WHERE id=%d);", laboratory_number);
  rslt = pgQuery(conn, query);
  free((query=NULL));

  //Verifica se o resultado da existencia do lab e verdadeiro ou falso
  if( strcmp( PQgetvalue(rslt, 0, 0), "t" ) == 0 )
    {
    return true;
    }

   printf("%s\n", PQgetvalue(rslt, 0, 0));

   return false;
}

/*Aloca um novo ponteiro para Host*/
_HOST *
initHost(_HOST *hosts)
{
  hosts = (_HOST *) malloc(sizeof(_HOST));
  if(!hosts)
    {
    ERROR("Erro de alocacao de memoria");
    exit(-1);
    }

  memset(hosts, 0, sizeof(_HOST));
  hosts->next = NULL;
}

/*Aloca um novo atributo de host*/
char *
initHostAttribute(char * attr, size_t len )
{
  char
   *new_attr = (char *) malloc(sizeof(char) * len);

  if(!new_attr)
    {
    ERROR("Erro de alocacao de memoria");
    exit(-1);
    }

   memset(new_attr, 0, len);
   strncpy(new_attr, attr, len);

   return new_attr;
}

short
insertHost(char *id, char *idlab, char *idos, char *patri, char *ip, char *comm, char *status, _HOST *hosts )
{
  _HOST *new;

  new = initHost(new);
  new->id_host   = initHostAttribute( id    , (strlen(id)     + 1));
  new->id_lab    = initHostAttribute( idlab , (strlen(idlab)  + 1));
  new->id_os     = initHostAttribute( idos  , (strlen(idos)   + 1));
  new->patrimony = initHostAttribute( patri , (strlen(patri)  + 1));
  new->ip_host   = initHostAttribute( ip    , (strlen(ip)     + 1));
  new->community = initHostAttribute( comm  , (strlen(comm)   + 1));
  new->status    = initHostAttribute( status, (strlen(status) + 1));

  if(hosts->next == NULL)
    {
    hosts->next = new;
    return 0;
    }
  else
    {
    _HOST *tmp = hosts;
    while(tmp->next != NULL)
      {
      tmp = tmp->next;
      } 

    tmp->next = new;

    return 1;
    }
  return -1;
}

bool
removeHost(_HOST *hosts)
{
  _HOST *gb;

  if(hosts->next == NULL)
    {
    printf("Empty queue");
    return false;
    }

   gb          = hosts->next;
   hosts->next = gb->next;
   gb          = NULL;
   free(gb);

   return true;
}

_HOST *
detachHost(_HOST *hosts)
{
  _HOST *detached;

  if(hosts->next == NULL)
    {
    printf("Empty queue");
    return NULL;
    }

   detached    = hosts->next;
   hosts->next = detached->next;

   return detached;
}

void
freeHost(_HOST *hosts)
{
  while(removeHost(hosts) == true );
}

/*Pega todos os computadores que estao cadastrado para um determindo laboratory*/
_HOST *
getHost(PGconn *conn, _HOST * hosts, int class_number)
{
  /*Variaveis*/
  PGresult
   *rslt;           //Resultado dos computadores da sala;

  int
    count,
    tuples_number,  //Numero de Tuplas
    fields_number;  //Numero de Campos

  char
   *query;


  query = (char *) malloc( sizeof(char) * (80)); 
  if(!query)
    {
    ERROR("Alocacao de memoria");
    exit(-1);
    }

  /*Requerindo do banco um SELEC dos computadores cadastrados*/
  sprintf(query, "SELECT * FROM Computer WHERE id_lab=%d ORDER BY id;", class_number);
  rslt = pgQuery(conn, query);
  
  /*Numero de rows = tuples e column = fields*/
  tuples_number = number_of_rows = pgNumTuples(rslt);
  fields_number = pgNumFields(rslt);

  hosts = initHost(hosts);

  /*Percorre cada tupla de acordo com o numero de usuarios*/
  for(count = 0; count < tuples_number; count ++ )
    {
    short
      check; //Verifica o retorno

    insertHost
      (
      PQgetvalue(rslt, count, 0), 
      PQgetvalue(rslt, count, 1), 
      PQgetvalue(rslt, count, 2), 
      PQgetvalue(rslt, count, 3), 
      PQgetvalue(rslt, count, 4),
      PQgetvalue(rslt, count, 5),
      PQgetvalue(rslt, count, 6),
      hosts
      );

      if(check == -1)
        {
        ERROR("Comportamento inesperado o ocorreu");
        exit(-1);
        }     
    }

  /*Encerramento da funcao*/
  free((query=NULL));
  pgClearResult(rslt);

  return hosts;
}

int 
saveResult(PGconn * conn, int status, struct snmp_session *sp, struct snmp_pdu *pdu, const char *auxoid, char  *auxid)
{
  char buf[1024];
  struct variable_list *variable_p;

  switch (status)
    {
    case STAT_SUCCESS://CASO OCORREU TUDO CORRETAMENTE -------------------------
      { 
      variable_p = pdu->variables;

      if(pdu->errstat == SNMP_ERR_NOERROR)
        {
        while(variable_p != NULL)
          {
          snprint_variable(buf, 1024, variable_p->name, variable_p->name_length, variable_p);
          printf("\n\nBUFF: %s - OID: %s\n\n", buf, auxoid);

          if(strcmp(auxoid, ".1.3.6.1.2.1.1.5.0")==0)
            {
            printf("PRIMEIRO OID\n");
            pgUpdateRegister(conn, "status","'Disponivel'",auxid);
            }
          if(strcmp(auxoid, ".1.3.6.1.2.1.1.3.0")==0)
            {
            printf("SEGUNDO OID\n");
            pgUpdateRegister(conn, "status","'Disponivel'",auxid);
            }
          if(strcmp(auxoid, ".1.3.6.1.2.1.1.1.0")==0)
            {
            printf("TERCEIRO OID\n");
            pgUpdateRegister(conn, "status","'Disponivel'",auxid);
            }
          variable_p = variable_p->next_variable;
          }
        }
      else
        {
        for ( ;variable_p != NULL ; variable_p = variable_p->next_variable)
          {
          if(variable_p != NULL)
            {
            snprint_objid(buf, 1024, variable_p->name, variable_p->name_length);
          }
          else
          {
            strcpy(buf, "(none)");
            pgUpdateRegister(conn, "status", snmp_errstring(pdu->errstat),auxid);
            fprintf(stdout, "%s: %s: %s\n", sp->peername, buf, snmp_errstring(pdu->errstat));
          } 
        }
      }
      return 0;
    }
    case STAT_TIMEOUT:
    {
      pgUpdateRegister(conn, "status","'Timeout'",auxid);
      return -1;
    }
    case STAT_ERROR:
    {
      pgUpdateRegister(conn, "status","'Desconhecido'",auxid);
      snmp_perror(sp->peername);
      return -1;
    }
  }
  return 0;
}

/* Realiza a contagem de todas a palavras divididas pelo simbolo*/
unsigned int
countWord( char *text, char simbol )
{
   char
     *buf = text;

   int
      i,
      value=0;

   for( i = 0 ; buf[i] != 0x00 ; i++ )
   {
      if(( (buf[i+1] == simbol) || ( buf[i+1] == 0x20 ) || ( buf[i+1] == 0x00 ) ))
      {
         value++;
      }
   }
   return value;
}

/* Esta funcao conta o tamanho da primeira palavra que ate um simbolo definido*/
unsigned int
firstWordInText( char *text, char simbol )
{
   char
     *buf = text;

   int 
      i,
      value=0;

   for( i = 0 ; ((buf[i] != simbol) || (buf[i] == 0x00)) ; i++ )
   {
      value++;
   }

   return value;
}
/*  Esta  funcao  prepara os  campos para  receber seus  respectivos valores, os  
  valores devem ser separados pelo mesmo elemento localizado em simbolo */
char *
prepareFields( char *fields, char *values, char simbol )
{
   /*Variaveis*/
  char 
   *buf_fields = fields,  //Buffer dos campos
   *buf_values = values,  //Buffer dos valores
   *word_field,           //Apenas uma palavra dos campos
   *word_value,           //Apenas uma palavra dos valores
   *query;

  size_t
    count = 0,
    fields_size,
    word_size_field,
    word_size_value;            //Tamanho da palavra paara alocar


  //Alocacao da query
  fields_size = countWord(fields, simbol);
 
  query = (char *) malloc( sizeof(char) * ( (strlen(fields)+ strlen(values) + (8 * fields_size) )));

  if(!query)
    {
    ERROR("Erro de Alocacao de memoria");
    exit(-1);
    }

  //Limpando a memoria
  memset(query, 0, strlen(query));

  do{ 
    //Variaveis temporarias
    int
      size_buf_field,
      size_buf_value;

    //Verifica os campos e separa da seu simbolo
    size_buf_field = word_size_field = firstWordInText( buf_fields, simbol );

    //Aloca memoria para a palavra do campo
    word_field = (char *) malloc( sizeof(char ) * (word_size_field + 1));
    if(!word_field)
      {
      ERROR("Erro de Alocacao de memoria");
      exit(-1);
      }

    //Limpando memoria 
    memset(word_field, 0, strlen(word_field));

    //copia um trecho da memoria referente ao tamanho da palavra
    strncpy(word_field, buf_fields, word_size_field + 1);

    //Adiciona \0 no final da string
    word_field[word_size_field] = 0x00;

    //Desloca o ponteiro paara a proxima palavra
    buf_fields = (buf_fields + (word_size_field + 1) ); 

    //Verifica os valores e separa da seu simbolo -- igual esquema acima
    size_buf_value = word_size_value = firstWordInText( buf_values, simbol );

    word_value = (char *) malloc( sizeof(char ) * (word_size_value + 1) );
    if(!word_value)
      {
      ERROR("Erro de Alocacao de memoria");
      exit(-1);
      }
 
    strncpy(word_value, buf_values, word_size_value + 1);
    word_value[word_size_value] = 0x00;
    buf_values = (buf_values + (word_size_value + 1) );  
 
    //Inicia a formacao da query
    strcat(query, word_field);
    strncat(query, " = ", 3); 

    strcat(query, word_value);      
    if(count + 1 < fields_size)
      {
      strcat(query, ", ");
      }

    word_field = word_value = NULL;
    free(word_field);
    free(word_value);
    count++;
  }while(count < (fields_size));

  query[strlen(query)] = 0x00;

  //Limpando e encerrando a memoria
  buf_fields = buf_values = NULL;
  free(buf_fields);
  free(buf_values);

  return(query);
}


/*Prepara para realizar update no banco*/
int
pgUpdateRegister(PGconn * conn, char * fields, char * values, char * id_host)
{  
   /*Variaveis*/
  char 
    init_query[] = "UPDATE Computer SET ",
    last_query[] = " WHERE id = ",
   *into_query,
   *query;
  unsigned
    count,
    query_len = 0;

  //Prepara os campos para UPDATE
  into_query = prepareFields(fields, values, ',');

  query_len = strlen(init_query) + strlen(last_query) + strlen(into_query) +10;
  
  query = (char *) malloc( sizeof(char ) * query_len);
  if(!query)
    {
    ERROR("Alocacao de memoria");
    exit(-1);
    }

  memset(query, 0, query_len);

  //Monta a query
  strcat(query, init_query);
  strcat(query, into_query);
  strcat(query, last_query);
  strcat(query, id_host);

  //Requista a query para o banco
  pgQuery(conn, query);

  free((into_query = NULL));
  free((query = NULL));

  return 0;
}

void
synchronous( PGconn *conn, _HOST *hosts)
{
  //Variaveis 
  _HOST
   *hp;  //Ponteiro para os hosts

  int
    i;

  //Percorre cada host para saber se os determinados OID estao operando
  for( hp = hosts->next, i = 0 ; (hp != NULL) && ( i < number_of_rows ); i++, hp = hp->next ) 
    {
    //Estruturas e variaveis internas
    struct snmp_session
       ss,              //Sessao
      *sp;              //Ponteiro da sessao

    struct oid
       *op;
    
    char 
       *aux_id;

    printf("Host: %s\n", hp->ip_host);

    /* initialize session */
    snmp_sess_init(&ss);  

    ss.version       = SNMP_VERSION_2c;
    ss.peername      = strdup(hp->ip_host);
    ss.community     = strdup(hp->community);
    ss.community_len = strlen(ss.community);

    if(!(sp = snmp_open(&ss))) 
      {
      snmp_perror("snmp_open");
      continue;
      }	

    for( op = oids; op->Name != NULL ; op = (op+1))
      {
      //Variaveis
      int
         status;  //Se ocorreram ocorretamente as operecoes do SNMP

      struct snmp_pdu
        *req,     //Requisicao enviada
        *resp;    //Resposta do host

      //Prepara o PDU da requisicao
      req = snmp_pdu_create(SNMP_MSG_GET);
      //Prepara a requisicao para um determinado OID
      snmp_add_null_var(req, op->Oid, op->OidLen);
      //Recebe a resposta do host
      status = snmp_synch_response(sp, req, &resp);

      if( saveResult(conn, status, sp, resp, op->Name, hp->id_host) == -1 )
        break;

      //Finaliza a resposta
      snmp_free_pdu(resp);
    }
    //Encerra sessao  
    snmp_close(sp);
  }
}



//MAIN--------------------------------------------------------------------------
int main (int argc, char **argv)
{
   /*Variaveis*/
   PGconn 
     *conn;

   int
      laboratory_number = 1;  

   _HOST 
     *hosts;              //Host do laboratorio


   /*Verificando numero de argumentos
   if(( argc > 2 ) || (argc <= 1) )
   {
      printf("ARG_CHECK:\n\tNumero de parametros esta incorreto\n");
      return -1;
   }*/
 
   /*Parametros de inicializacao do snmp*/
   netsnmp_ds_toggle_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_QUICK_PRINT);
   netsnmp_ds_toggle_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_PRINT_BARE_VALUE);

  while("happy")
    {
    /*Inicia o SNMP*/
    startSnmp();

    /*Abertura de conexao com PostgreSQL*/  
    conn = pgConn
             (
             "host=localhost "
             "port=5432 "
             "dbname=postgres "
             "user=root "
             "password=root"
             );

    /*Verifica se o labatorio existe*/
    if(checkLaboratory(conn, laboratory_number) == false )
      {
      printf("RESET SNMP HOST SEARCH LIST\n");
      pgClose(conn);
      laboratory_number = 1;
      continue;
      }

    initHost(hosts); 

    /*pega os computadores cadastrados*/
    hosts = getHost(conn, hosts, laboratory_number );

    synchronous(conn, hosts);

    freeHost(hosts);

    laboratory_number++;
    }

   return 0;
}
