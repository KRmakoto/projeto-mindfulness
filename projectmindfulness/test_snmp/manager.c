#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>

#include "pgsql.h"



//Teste para implementar com com a versão 3

#ifdef HAVE_WINSOCK_H
#include <winsock.h>
#endif


typedef struct host{
   char *id_host;
   char *ip_host;
   char *community;
} _HOST;


//Estrutura referente a tabela de computadores do banco
typedef struct computer{
   char *id;
   char *laboratory;
   char *mac;
   char *ip;
} COMPUTER;

struct oid{
   const char *Name;
   oid   Oid[MAX_OID_LEN];
   int OidLen;
} oids[] = {
  { ".1.3.6.1.2.1.1.5.0" },
  { ".1.3.6.1.2.1.1.3.0" },
  { ".1.3.6.1.2.1.1.1.0" },
  { NULL }
};


_HOST *hosts;

extern number_of_rows;


void startSnmp(void)
{
  struct oid *op = oids;
  SOCK_STARTUP;
  init_snmp("synchapp");
  while (op->Name) 
  {
    op->OidLen = sizeof(op->Oid)/sizeof(op->Oid[0]);
    if (!read_objid(op->Name, op->Oid, &op->OidLen)) {
      snmp_perror("read_objid");
      exit(1);
    }
    op++;
  }
}


void
searchHost(PG_CONN conn)
{
  //Variaveis
  PG_RSLT
     rslt;

  int
     num_tuples,
     num_fields,
     count;

  //Requerindo do banco um SELECT
  rslt = pg_query(conn, "SELECT * FROM computer")) 
  
  //Numero de rows = tuples e column = fields
  num_tuples = number_of_rows = pgNumTuples(rslt);
  num_fields = pgNumFields(rslt);
  

  hosts = malloc( sizeof(_HOST) * (num_tuples) );

  for(count = 0; count < num_tuples; count ++ )
  {
    hosts[count] = PQgetvalue(rslt, count, 0);
    hosts[count] = PQgetvalue(rslt, count, 1);
    hosts[count] = PQgetvalue(rslt, count, 2);
    hosts[count] = PQgetvalue(rslt, count, 3);
  }
  pgCleanResult(result);
  pgClose(con);
}


int 
saveResult(int status, struct snmp_session *sp, struct snmp_pdu *pdu, char *auxoid, char  *auxid)
{
  char buf[1024];
  struct variable_list *vp;
  int ix;

  switch (status)
  {
    case STAT_SUCCESS:
    {
      vp = pdu->variables;
      if (pdu->errstat == SNMP_ERR_NOERROR)
      {
        while(vp)
        {
          //Nao seria strlen(buf) ? -- debbugar
          snprint_variable(buf, sizeof(buf), vp->name, vp->name_length, vp);
          if(buf[0]=='"')
          {
            int it;
            for (it = 0; it<strlen(buf)-2; it++)
            {
              buf[it]=buf[it+1];
            }
            buf[it]='\0';
          }
          if(strcmp(auxoid,".1.3.6.1.2.1.1.5.0")==0){
            pgUpdateRegister("status","Disponivel",auxid);
            pgUpdateRegister("hostname",buf,auxid);
          }
          if(strcmp(auxoid,".1.3.6.1.2.1.1.3.0")==0){
            pgUpdateRegister("status","Disponivel",auxid);
            pgUpdateRegister("uptime",buf,auxid);
          }
          if(strcmp(auxoid,".1.3.6.1.2.1.1.1.0")==0){
            pgUpdateRegister("status","Disponivel",auxid);
            pgUpdateRegister("sysdesc",buf,auxid);
          }
          vp = vp->next_variable;
        }
      }
      else
      {
        for (ix = 1; vp && ix != pdu->errindex; vp = vp->next_variable, ix++)
        {
          if(vp)
          {
            snprint_objid(buf, sizeof(buf), vp->name, vp->name_length);
          }
          else
          {
            strcpy(buf, "(none)");
            pgUpdateRegister("status",snmp_errstring(pdu->errstat),auxid);
            fprintf(stdout, "%s: %s: %s\n",sp->peername, buf, snmp_errstring(pdu->errstat));
          } 
        }
      }
      return 1;
    }
    case STAT_TIMEOUT:
    {
      pgUpdateRegister("status","Timeout",auxid);
      fprintf(stdout, "%s: Timeout\n", sp->peername);
      return 0;
    }
    case STAT_ERROR:
    {
      pgUpdateRegister("status","Desconhecido",auxid);
      snmp_perror(sp->peername);
      return 0;
    }
  }
  return 0;
}

int
pgUpdateRegister(PG_CONN conn, char * campo, char * valor, char * id_host)
{
  char auxsql[1000]="UPDATE computer_status SET ";
  strcat(auxsql,campo);
  if(valor== NULL)
  {
    strcat(auxsql," = NULL, ");   
  }else{
    strcat(auxsql," = '");
    strcat(auxsql,valor);
    strcat(auxsql,"', ");
  }
  strcat(auxsql,"login= now() ");
  strcat(auxsql,"WHERE id='");
  strcat(auxsql,id_host);
  strcat(auxsql,"';");

  pgQuery(conn, auxsql);

  pgClose(conn);
  return 0	;
}

void
synchronous(void)
{
  //Variaveis 
  _HOST *hp;
  int i;

  hp = hosts;

  for (i=0;i < n_rows; i++) 
  {
    //Estruturas e variaveis internas
    struct snmp_session
        ss,              //Sessao
       *sp;              //Ponteiro da sessao
    struct oid
       *op;
    
    char 
       *aux_id;


    printf("Host: %s\n", hp[i].ip_host);

    /* initialize session */
    snmp_sess_init(&ss);
    
    ss.version = SNMP_VERSION_2c;

    /*
    // set the SNMPv3 user name 
    ss.securityName = strdup("MD5User");
    ss.securityNameLen = strlen(ss.securityName);

    // set the security level to authenticated, but not encrypted
    ss.securityLevel = SNMP_SEC_LEVEL_AUTHNOPRIV;

    // set the authentication method to MD5 
    ss.securityAuthProto = usmHMACMD5AuthProtocol;
    ss.securityAuthProtoLen = sizeof(usmHMACMD5AuthProtocol)/sizeof(oid);
    ss.securityAuthKeyLen = USM_AUTH_KU_LEN;

    // set the authentication key to a MD5 hashed version of our
    //   passphrase "The UCD Demo Password" (which must be at least 8
    //   characters long)
    if (generate_Ku(ss.securityAuthProto,
                    ss.securityAuthProtoLen,
                    (u_char *) our_v3_passphrase, strlen(our_v3_passphrase),
                    ss.securityAuthKey,
                    &ss.securityAuthKeyLen) != SNMPERR_SUCCESS) {
        snmp_log(LOG_ERR,
                 "Error generating Ku from authentication pass phrase. \n");
        exit(1);
    }      

    */


    ss.peername = strdup(hp[i].ip_host);

    ss.community = strdup(hp[i].community);

    ss.community_len = strlen(ss.community);
    if (!(sp = snmp_open(&ss))) 
    {
       snmp_perror("snmp_open");
       continue;
    }
 
    
    aux_id = malloc(sizeof(char)*strlen(hp[i].id_host));
    strcpy(aux_id,hp[i].id_host);

    for (op = oids; op->Name; op++)
    {
      char * aux_oid;
      aux_oid = malloc(sizeof(char)*strlen(op->Name));
      strcpy(aux_oid,op->Name);
      struct snmp_pdu *req, *resp;
      int status;
      req = snmp_pdu_create(SNMP_MSG_GET);
      snmp_add_null_var(req, op->Oid, op->OidLen);
      status = snmp_synch_response(sp, req, &resp);
      if (!saveResult(status, sp, resp, aux_oid, aux_id))
      {
        aux_id = NULL;
        aux_oid = NULL;
        free(aux_id);
        free(aux_oid);
        break;
      }
      aux_oid = NULL;
      free(aux_oid);
      snmp_free_pdu(resp);
    }
    aux_id = NULL;
    free(aux_id);   
    snmp_close(sp);
  }
}



//MAIN--------------------------------------------------------------------------
int main (int argc, char **argv)
{
  netsnmp_ds_toggle_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_QUICK_PRINT);
  netsnmp_ds_toggle_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_PRINT_BARE_VALUE);
  int segundos = 10;

  PG_CONN conn = pgConn("host=localhost port=5432 dbname=postgres user=root password=root");

  while(TRUE)
  {
    startSnmp();
    searchHost();
    synchronous();
    printf("Aguardando %d segundos.\n",segundos);
    sleep(segundos);
  }
  return 0;
}
