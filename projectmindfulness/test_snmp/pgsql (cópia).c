/*
 
    Arquivo contendo as alteração de nomenclaturas de algumas funções do da 
  libpq-fe.h, para que a mesma se apresente mais próxima a que se encontra 
  nas bibliotecas do php.

  
  Author: Talles H.

*/


//Bibliotecas

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <libpq-fe.h>

#include "pgsql.h"


//typedef PGconn   *PG_CONN;
//typedef PGresult *PG_RSLT;

#define PG_CONN PGconn*
#define PG_RSLT PGresult*

//Fechar conexao e o sistema
void pgForceClose(PG_CONN conn)
{   
   PQfinish(conn);
   //Exit nao portavel para WIN - tratar com ifdef
   exit(1);
}

//Fechar a conexao
void pgClose(PG_CONN conn)
{   
   PQfinish(conn);
}

//Limpar o result
void pgClearResult(PG_RSLT rslt)
{
   PQclear(rslt);
}

//Abrir conexao
PG_CONN pgConn(const char *query)
{
   PG_CONN conn = PQconnectdb(query);

   if(PQstatus(conn) == CONNECTION_BAD)
   {
      fprintf(stderr, "Falha na conexao com o banco de dados: ERRO := %s\n", PQerrorMessage(conn));
      pgForceClose(conn);
   }

   return conn;
}

//Forcar resultado com saida 
void
pgResultForceClose(PG_RSLT rslt, const char *query)
{
   fprintf
      (
      stderr,
      "Falha no comando enviada ao banco:\n"
      "\tCMD  :=>> %s <<\n"
      "\tERROR:=>> %s <<\n",
      query, PQresultErrorMessage(rslt)
      );
   PQclear(rslt);
}

//Executar comando no banco 
PG_RSLT pgQuery(PG_CONN conn, const char *query)
{
   PG_RSLT rslt;
   ExecStatusType status;

   rslt = PQexec(conn, query);
   status = PQresultStatus(rslt); 

 
   switch(status)
   {//Caso retorne query vazia
      case PGRES_EMPTY_QUERY :
      { 
         pgResultForceClose(rslt, query);
         pgForceClose(conn);
      }
      case PGRES_COMMAND_OK  :
      {
         fprintf
            (
            stdout,
            "Resultado do comando:\n"
            "\tCMD  :=>> %s <<\n"
            "\tRSTL:=>> PGRES_COMMAND_OK <<\n",
            query
            );
         return(rslt); 
      }//Caso retorne tuples > 1
      case PGRES_TUPLES_OK  :
      {
         fprintf
            (
            stdout,
            "Resultado do comando:\n"
            "\tCMD  :=>> %s <<\n"
            "\tRSTL:=>> PGRES_TUPLES_OK <<\n",
            query
            );
         return(rslt); 
      }//Caso retorne apenas um tuple
      case PGRES_SINGLE_TUPLE:
      {
         fprintf
            (
            stdout,
            "Resultado do comando:\n"
            "\tCMD  :=>> %s <<\n"
            "\tRSTL:=>> PGRES_SINGLE_TUPLE  <<\n",
            query
            );
         return(rslt); 
      }
      default:
         return(rslt);
   }
}

//Pega o numero de tuple  - ROWS
int pgNumTuples(const PG_RSLT rslt)
{
   if(rslt == NULL)
   {
      return 0;
   }

   return PQntuples(rslt);
}

//Pega o numero de Fields - COLUMN
int pgNumFields(const PG_RSLT rslt)
{
   if(rslt == NULL)
   {
      return 0;
   }

   return PQnfields(rslt);
}


/*int
main()
{
   //Variaveis
   PG_CONN conn;
   PG_RSLT rslt;
   int ntuples, i; 

   //Conexao
   conn = pgConn("host=localhost port=5432 dbname=postgres user=root password=root");
   
   printf("Conetado com sucesso\n");

   rslt = pgQuery(conn, "SELECT * FROM operating_system");

   ntuples = pgNumTuples(rslt);

   printf("Numero de Tuples: %d\n", ntuples);

   for(i = 0; i < ntuples ; i++ )
   {
      printf("Resultado: %s - %s ", PQgetvalue(rslt, i, 0), PQgetvalue(rslt, i, 1));
   }
   
   pgClearResult(rslt);
   pgClose(conn);

   return 0;
}*/
