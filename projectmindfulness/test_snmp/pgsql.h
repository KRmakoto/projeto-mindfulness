#ifndef _PGSQL_H   // guardas de cabeçalho, impedem inclusões cíclicas
#define _PGSQL_H

extern PG_CONN;
extern PG_RSLT;

//Fechar conexao e o sistema
void pgForceClose(PG_CONN conn);


//Fechar a conexao
void pgClose(PG_CONN conn);


//Limpar o result
void pgClearResult(PG_RSLT rslt);


//Abrir conexao
PG_CONN pgConn(const char *query);


//Forcar resultado com saida 
void
pgResultForceClose(PG_RSLT rslt, const char *query);

//Executar comando no banco 
PG_RSLT pgQuery(PG_CONN conn, const char *query);

//Pega o numero de tuple  - ROWS
int pgNumTuples(const PG_RSLT rslt);

//Pega o numero de Fields - COLUMN
int pgNumFields(const PG_RSLT rslt);

#endif

