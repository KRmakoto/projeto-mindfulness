#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <stdlib.h>
#include <stdbool.h>
#include <libpq-fe.h>

//Teste para implementar com com a versão 3

#ifdef HAVE_WINSOCK_H
#include <winsock.h>
#endif

#define ERROR(msg) fprintf(stderr, "FAILURE := %s : \t\t\t\t\t\tLINE %d\n\n", (msg), __LINE__)


//Fechar conexao e o sistema
void pgForceClose(PGconn * conn)
{   
   PQfinish(conn);
   //Exit nao portavel para WIN - tratar com ifdef
   exit(1);
}

//Fechar a conexao
void pgClose(PGconn * conn)
{   
   PQfinish(conn);
}

//Limpar o result
void pgClearResult(PGresult * rslt)
{
   PQclear(rslt);
}

//Abrir conexao
PGconn * pgConn(const char *query)
{
   PGconn * conn = PQconnectdb(query);

   if(PQstatus(conn) == CONNECTION_BAD)
   {
      fprintf(stderr, "Falha na conexao com o banco de dados: ERRO := %s\n", PQerrorMessage(conn));
      pgForceClose(conn);
   }

   return conn;
}

//Forcar resultado com saida 
void
pgResultForceClose(PGresult * rslt, const char *query)
{
   fprintf
      (
      stderr,
      "Falha no comando enviada ao banco:\n"
      "\tCMD  :=>> %s <<\n"
      "\tERROR:=>> %s <<\n",
      query, PQresultErrorMessage(rslt)
      );
   PQclear(rslt);
}

//Executar comando no banco 
PGresult * pgQuery(PGconn * conn, const char *query)
{
   PGresult * rslt;
   ExecStatusType status;

   rslt = PQexec(conn, query);
   status = PQresultStatus(rslt);  

   switch(status)
   {//Caso retorne query vazia
      case PGRES_EMPTY_QUERY :
      { 
         pgResultForceClose(rslt, query);
         pgForceClose(conn);
      }
      case PGRES_COMMAND_OK  :
      {
         fprintf
            (
            stdout,
            "Resultado do comando:\n"
            "\tCMD  :=>> %s <<\n"
            "\tRSTL:=>> PGRES_COMMAND_OK <<\n",
            query
            );
         return(rslt); 
      }//Caso retorne tuples > 1
      case PGRES_TUPLES_OK  :
      {
         fprintf
            (
            stdout,
            "Resultado do comando:\n"
            "\tCMD  :=>> %s <<\n"
            "\tRSTL:=>> PGRES_TUPLES_OK <<\n",
            query
            );
         return(rslt); 
      }//Caso retorne apenas um tuple
      case PGRES_SINGLE_TUPLE:
      {
         fprintf
            (
            stdout,
            "Resultado do comando:\n"
            "\tCMD  :=>> %s <<\n"
            "\tRSTL:=>> PGRES_SINGLE_TUPLE  <<\n",
            query
            );
         return(rslt); 
      }
      default:
         return(rslt);
   }
}

//Pega o numero de tuple  - ROWS
int pgNumTuples(const PGresult * rslt)
{
   if(rslt == NULL)
   {
      return 0;
   }

   return PQntuples(rslt);
}

//Pega o numero de Fields - COLUMN
int pgNumFields(const PGresult * rslt)
{
   if(rslt == NULL)
   {
      return 0;
   }

   return PQnfields(rslt);
}

/*GERENTE**********************************************************************/
typedef struct host{
   char *id_host;
   char *id_lab;
   char *id_os;
   char *patrimony;
   char *ip_host;
   char *community;
   char *status;
   struct *host next;
} _HOST;


struct oid{
   const char *Name;
   oid   Oid[MAX_OID_LEN];
   int   OidLen;
} oids[] = {
  { ".1.3.6.1.2.1.1.5.0" },   //
  { ".1.3.6.1.2.1.1.3.0" },   //
  { ".1.3.6.1.2.1.1.1.0" },   //
  { NULL }
};


/*VARIAVEIS GLOBAIS*/

int
   number_of_rows;     //Numero de linhas 
/*FIM-VARIAVEIS GLOBAIS*/


/*ESCOPO DAS FUNCOES*/
int pgUpdateRegister(PGconn *conn, char *campo, char *valor, char *id_host);
void startSnmp(void);
/*FIM-ESCOPO DAS FUNCOES*/




/*Funcao que inicia o socket SNMP*/
/*  Como  utiliza-se da  chamada SOCK_STARTUP, utilizar  esta funcao  apenas  no
  escopo da main(). 
  url: http://net-snmp.sourceforge.net/docs/README.thread.html*/
void
startSnmp(void)
{
   struct oid
     *oid_pointer = oids;   //Ponteiro para os OIDS GLOBAIS
     SOCK_STARTUP;          //Inicia o Socket para o SNMP
     init_snmp("synchapp");

   //Percorre todos OIDS registrados
   while (oid_pointer->Name) 
   {
      oid_pointer->OidLen = sizeof(oid_pointer->Oid)/sizeof(oid_pointer->Oid[0]);
      if(!read_objid(oid_pointer->Name, oid_pointer->Oid, &(oid_pointer->OidLen)))
      {
         snmp_perror("read_objid");
         exit(1);
      }
      oid_pointer++;
   }
}

/*Verifica se o laboratorio para este numero de laboratorio*/
bool
checkLaboratory(PGconn *conn, int laboratory_number)
{
   /*Variaveis*/
   PGresult
     *rslt;

   char
     *query;

   //Requerindo se existe ou nao o laboratorio
   query = (char *) malloc( sizeof(char) * (51)); 
   sprintf(query, "SELECT EXISTS( SELECT 1 FROM Laboratory WHERE id=%d);", laboratory_number);
   rslt = pgQuery(conn, query);
   free((query=NULL));

   if( PQgetvalue(rslt, 0, 0) == "t" )
   {
      return true;
   }

   return false;
}



typedef struct host{
   char *id_host;
   char *id_lab;
   char *id_os;
   char *patrimony;
   char *ip_host;
   char *community;
   char *status;
   struct *host next;
} _HOST;


void
insertHost(char *id, char *idlab, char *idos, char *patri, char *comm, char *status, _HOST *hosts )
{
  _HOST *new;

  new->id_host = (char *) malloc(sizeof(char) * strlen(id));
  new->id_lab  = (char *) malloc(sizeof(char) * strlen(idlab));
  new->id_os = (char *) malloc(sizeof(char) * strlen(idos));
  new->id_host = (char *) malloc(sizeof(char) * strlen(patri));
  new->id_host = (char *) malloc(sizeof(char) * strlen(comm));
  new->id_host = (char *) malloc(sizeof(char) * strlen(status));

}


/*Pega todos os computadores que estao cadastrado para um determindo laboratory*/
_HOST *
getHost(PGconn *conn, _HOST * hosts, int class_number)
{
   /*Variaveis*/
   PGresult
     *rslt;           //Resultado dos computadores da sala;

   int
      count,
      tuples_number,  //Numero de Tuplas
      fields_number;  //Numero de Campos

   char
     *query;


   query = (char *) malloc( sizeof(char) * (40)); 
   if(!query)
   {
      ERROR("Alocacao de memoria");
      exit(-1);
   }

   /*Requerindo do banco um SELEC dos computadores cadastrados*/
   sprintf(query, "SELECT * FROM Computer WHERE id_lab=%d;", class_number);
   printf("%s\n", query);
   rslt = pgQuery(conn, query);
  
   /*Numero de rows = tuples e column = fields*/
   tuples_number = number_of_rows = pgNumTuples(rslt);
   fields_number = pgNumFields(rslt);
  
   int i, j, len;
   for( len = i = 0 ; i < tuples_number ; i++ )
   {
      
      for( j = 0 ; j < fields_number ; j++ )
         len += PQgetlength(rslt, i, j);

      hosts = (_HOST *) malloc( sizeof(_HOST) * len);
      len = 0;
      	
   }


   printf("LEN %d\n", len);

   hosts = (_HOST*) malloc( len * (sizeof(_HOST)) );
   if(!hosts)
   {
      ERROR("Alocacao de memoria");
      exit(-1);
   }

    
   memset(hosts, 0, (len * (tuples_number * fields_number)));

   /*Percorre cada tupla de acordo com o numero de usuarios*/
   for(count = 0; count < tuples_number; count ++ )
   {
      hosts[count].id_host   = PQgetvalue(rslt, count, 0);
      hosts[count].id_lab    = PQgetvalue(rslt, count, 1);
      hosts[count].id_os     = PQgetvalue(rslt, count, 2);
      hosts[count].patrimony = PQgetvalue(rslt, count, 3);
      hosts[count].ip_host   = PQgetvalue(rslt, count, 4);
      hosts[count].community = PQgetvalue(rslt, count, 5);
      hosts[count].status    = PQgetvalue(rslt, count, 6);
      printf(
            "|  ID : %s  | ID_LAB : %s |  ID_OS:  %s | PATRIMONY : %s |\n"
            "|  IP : %s  | COMM   : %s |  STATUS: %s |\n\n\n", 
            hosts[count].id_host, hosts[count].id_lab, hosts[count].id_os, hosts[count].patrimony,
            hosts[count].ip_host, hosts[count].community, hosts[count].status
            );
    }

  /*Encerramento da funcao*/
  //free((query=NULL));
  pgClearResult(rslt);
  return hosts;
}

int 
saveResult(PGconn * conn, int status, struct snmp_session *sp, struct snmp_pdu *pdu, char *auxoid, char  *auxid)
{
  char buf[1024];
  struct variable_list *vp;
  int ix;

  switch (status)
  {
    case STAT_SUCCESS:
    {
      vp = pdu->variables;
      if (pdu->errstat == SNMP_ERR_NOERROR)
      {
        while(vp)
        {
          //Nao seria strlen(buf) ? -- debbugar
          snprint_variable(buf, strlen(buf), vp->name, vp->name_length, vp);
          if(buf[0]=='"')
          {
            int it;
            for (it = 0; it<strlen(buf)-2; it++)
            {
              buf[it]=buf[it+1];
            }
            buf[it]='\0';
          }
          if(strcmp(auxoid,".1.3.6.1.2.1.1.5.0")==0){
            pgUpdateRegister(conn, "status","Disponivel",auxid);
            pgUpdateRegister(conn, "hostname",buf,auxid);
          }
          if(strcmp(auxoid,".1.3.6.1.2.1.1.3.0")==0){
            pgUpdateRegister(conn, "status","Disponivel",auxid);
            pgUpdateRegister(conn, "uptime",buf,auxid);
          }
          if(strcmp(auxoid,".1.3.6.1.2.1.1.1.0")==0){
            pgUpdateRegister(conn, "status","Disponivel",auxid);
            pgUpdateRegister(conn, "sysdesc",buf,auxid);
          }
          vp = vp->next_variable;
        }
      }
      else
      {
        for (ix = 1; vp && ix != pdu->errindex; vp = vp->next_variable, ix++)
        {
          if(vp)
          {
            snprint_objid(buf, sizeof(buf), vp->name, vp->name_length);
          }
          else
          {
            strcpy(buf, "(none)");
            pgUpdateRegister(conn, "status",snmp_errstring(pdu->errstat),auxid);
            fprintf(stdout, "%s: %s: %s\n",sp->peername, buf, snmp_errstring(pdu->errstat));
          } 
        }
      }
      return 1;
    }
    case STAT_TIMEOUT:
    {
      pgUpdateRegister(conn, "status","Timeout",auxid);
      return 0;
    }
    case STAT_ERROR:
    {
      pgUpdateRegister(conn, "status","Desconhecido",auxid);
      snmp_perror(sp->peername);
      return 0;
    }
  }
  return 0;
}

/* Realiza a contagem de todas a palavras divididas pelo simbolo*/
unsigned int
countWord( char *text, char simbol )
{
   char
     *buf = text;

   int
      i,
      value=0;

   for( i = 0 ; buf[i] != 0x00 ; i++ )
   {
      if(( (buf[i+1] == simbol) || ( buf[i+1] == 0x20 ) || ( buf[i+1] == 0x00 ) ))
      {
         value++;
      }
   }
   return value;
}

/* Esta funcao conta o tamanho da primeira palavra que ate um simbolo definido*/
unsigned int
firstWordInText( char *text, char simbol )
{
   char
     *buf = text;

   int 
      i,
      value=0;

   for( i = 0 ; ((buf[i] != simbol) || (buf[i] == 0x00)) ; i++ )
   {
      value++;
   }

   return value;
}
/*  Esta  funcao  prepara os  campos para  receber seus  respectivos valores, os  
  valores devem ser separados pelo mesmo elemento localizado em simbolo */
char *
prepareFields( char *fields, char *values, char simbol )
{
    /*Variaveis*/
   char 
     *buf_fields = fields,  //Buffer dos campos
     *buf_values = values,  //Buffer dos valores
     *word_field,           //Apenas uma palavra dos campos
     *word_value,           //Apenas uma palavra dos valores
     *query;
   size_t
      count,
      fields_size,
      word_size_field,
      word_size_value;            //Tamanho da palavra paara alocar


   //Alocacao da query
   fields_size = countWord(fields, simbol);
   //Estimativa do tamanha e, LIKE + 2 espacos 
   query = (char *) malloc( sizeof(char) * ( (strlen(fields)+ strlen(values) + (8 * fields_size) )));
   if(!query)
   {
      ERROR("Erro de Alocacao de memoria");
      exit(-1);
   }



   //Limpando a memoria
  memset(query, 0, strlen(query));

  count = 0;

  do{ 
    //Variaveis temporarias
    int
      size_buf_field,
      size_buf_value;

    //Verifica os campos e separa da seu simbolo
    size_buf_field = word_size_field = firstWordInText( buf_fields, simbol );

    //Aloca memoria para a palavra do campo
    word_field = (char *) malloc( sizeof(char ) * (word_size_field + 1));
    if(!word_field)
      {
      ERROR("Erro de Alocacao de memoria");
      exit(-1);
      }

    //Limpando memoria 
    memset(word_field, 0, strlen(word_field));

    //copia um trecho da memoria referente ao tamanho da palavra
    strncpy(word_field, buf_fields, word_size_field + 1);

    //Adiciona \0 no final da string
    word_field[word_size_field] = 0x00;

    //Desloca o ponteiro paara a proxima palavra
    buf_fields = (buf_fields + (word_size_field + 1) ); 

    //Verifica os valores e separa da seu simbolo -- igual esquema acima
    size_buf_value = word_size_value = firstWordInText( buf_values, simbol );

    word_value = (char *) malloc( sizeof(char ) * (word_size_value + 1) );
    if(!word_value)
      {
      ERROR("Erro de Alocacao de memoria");
      exit(-1);
      }
 
    strncpy(word_value, buf_values, word_size_value + 1);
    word_value[word_size_value] = 0x00;
    buf_values = (buf_values + (word_size_value + 1) );  
 
    //Inicia a formacao da query
    strcat(query, word_field);
    if(word_value[0] == '\'')
      {
      strncat(query, " LIKE ", 6);
      }
    else
      {
      strncat(query, " = ", 3);
      } 

    strcat(query, word_value);      
    if(count + 1 < fields_size)
      {
      strcat(query, ", ");
      }

    word_field = word_value = NULL;
    free(word_field);
    free(word_value);
    count++;
  }while(count < (fields_size));

  query[strlen(query)] = 0x00;

  printf("MINHA QUERY: %s\n", query);

  buf_fields = buf_values = NULL;

  //free(buf_fields);
  //free(buf_values);


  return(query);
}

int
pgUpdateRegister(PGconn * conn, char * fields, char * values, char * id_host)
{  
   /*Variaveis*/
  char 
     *init_query = "UPDATE Computer SET ",
     *last_query = " WHERE id = ",
     *into_query,
     *query;
   unsigned
     count,
     query_len = 0;

  into_query = prepareFields(fields, values, ',');

  query = (char *) malloc( sizeof(char) * (strlen(init_query) + strlen(into_query) + strlen(last_query) + 4 ));
  if(!query)
    {
    ERROR("Erro de alocacao de memoria");
    exit(-1);
    }

  //Limpando memoria 
  memset(query, 0, strlen(query));

  sprintf(query, "%s%s%s%s\0", init_query, into_query, last_query, id_host);

  printf("MYQUERY: %s MEMLEN: %d\n", query, strlen(query));

  pgQuery(conn, query);

  query = init_query = into_query = last_query = NULL;

  free(last_query);
  free(into_query);
  free(init_query);
  free(query);
  
  return 0;
}

void
synchronous( PGconn *conn, const _HOST *hosts)
{
  //Variaveis 
  const _HOST *hp;
  int i;

  hp = hosts;

  for (i=0;i < number_of_rows; i++) 
  {
    //Estruturas e variaveis internas
    struct snmp_session
       ss,              //Sessao
      *sp;              //Ponteiro da sessao

    struct oid
       *op;
    
    char 
       *aux_id;


    printf("Host: %s\n", hp[i].ip_host);

    /* initialize session */
    snmp_sess_init(&ss);  

    ss.version       = SNMP_VERSION_2c;
    ss.peername      = strdup(hp[i].ip_host);
    ss.community     = strdup(hp[i].community);
    ss.community_len = strlen(ss.community);
    if (!(sp = snmp_open(&ss))) 
    {
       snmp_perror("snmp_open");
       continue;
    }	
 
    
    aux_id = (char *) malloc(sizeof(char)*strlen(hp[i].id_host) +1 );
    if(aux_id == NULL)
    {
       exit(-1);
    }

    memset(aux_id, 0, strlen(aux_id)); 

    strncpy(aux_id,hp[i].id_host, strlen(hp[i].id_host));

    for ( op = oids; op->Name ; op++)
    {
      //Variaveis
      char
        *aux_oid;
      int
         status;

      const oid auxOid = *(op->Oid);
      size_t auxOidLen = op->OidLen;

      aux_oid = (char *) malloc(sizeof(char) * strlen(op->Name)+1);
      if(aux_oid == NULL)
      {
         ERROR("Erro de alocacao de memoria em AUX_OID");
         exit(-1);
      }

      memset(aux_oid, 0, strlen(aux_oid));

      strcpy(aux_oid, op->Name);

      struct snmp_pdu
        *req,
        *resp;

      req = snmp_pdu_create(SNMP_MSG_GET);
      snmp_add_null_var(req, &auxOid , auxOidLen);
      status = snmp_synch_response(sp, req, &resp);
      if (!saveResult(conn, status, sp, resp, aux_oid, aux_id))
      {
        aux_id = NULL;
        aux_oid = NULL;
        free(aux_id);
        free(aux_oid);
        break;
      }
      aux_oid = NULL;
      free(aux_oid);
      snmp_free_pdu(resp);
    }
    aux_id = NULL;
    free(aux_id);   
    snmp_close(sp);
  }
}



//MAIN--------------------------------------------------------------------------
int main (int argc, char **argv)
{
   /*Variaveis*/
   PGconn 
     *conn;

   int
      laboratory_number;  

   _HOST 
     *hosts;              //Host do laboratorio


   /*Verificando numero de argumentos*/
   if(( argc > 2 ) || (argc <= 1) )
   {
      printf("ARG_CHECK:\n\tNumero de parametros esta incorreto\n");
      return -1;
   }
 
   /*Parametros de inicializacao do snmp*/
   netsnmp_ds_toggle_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_QUICK_PRINT);
   netsnmp_ds_toggle_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_PRINT_BARE_VALUE);

   /*Inicia o SNMP*/
   startSnmp();

   /*Abertura de conexao com PostgreSQL*/  
   conn = pgConn(
                 "host=localhost "
                 "port=5432 "
                 "dbname=postgres "
                 "user=root "
                 "password=root"
                );

   /*Verifica se o labatorio existe*/
   laboratory_number = atoi(argv[1]);
   if(checkLaboratory(conn, laboratory_number))
   {
      printf("ARG_CHECK:\n\tEste parametro nao existe, apenas numero inteiros positivos sao aceitos\n");
      return -1;  
   }

 
   /*pega os computadores cadastrados*/
   hosts = getHost(conn, hosts,laboratory_number);

   synchronous(conn, hosts);

   hosts = NULL;
   free(hosts);

   pgClose(conn);
   return 0;
}
