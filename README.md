# README do Projeto Mindfulness

## Sobre o projeto
  Projeto visa projetar um sistema de controle das redes dos laboratórios do IFMS, permitindo ao professor administrar através de comando o acesso a internet e detectar possíveis computadores conectados a rede.

## Versão
  Está é a versão _ALFA_ do projeto e contempla a etapa BÁSICA do projeto. 

## OBJETIVOS DO PROJETO
  Objetivo é melhorar a administração dos laboratórios do IFMS, permitindo aos professores executar ações de terminal, através de uma interface
web e ter o controle do laboratório em uso por ele no momento da aula.

  Com isso o professor poderá estar ciete quais computadores estão ligados, quais portas estão abertas e se deve fechá-las ou não, além de permitir
administrar a distância eventos dos computadores vinculados e ter o retorno vizualisado em tudo através de uma interface web.

### ETAPA BÁSICA
+ Pesquisa das tecnologia:
  * Pesquisar _Websocket_ com __Socket.io__
  * Pesquisar _dcomRPC_
  * Pesquisar acessoes de terminal com _PHP_ 
+ Banco de Dados
    + Projetar banco de dados em __postgresql__ contendo
        - Tabela __teacher__:
            1. __id__           : long int
            1. __teacher_name__ : varchar
            1. __log__          : boolean
        - Tabela __teacher_login__: REL >> 1[__teacher__] x N[__teacher_login__] 
            1. __id__
            1. __id_teacher__   : long int
            1. __login__        : timestamp
            1. __logout__       : timestamp
            1. __laboratory__   : timestamp
        - Tabela __student__:
            1. __id__           : long int
            2. __student_name__ : varchar
            3. __rga__          : varchar
            4. __log__          : boolean
        - Tabela __student_login__: REL >> 1[__student__] x N[__student_login__] 
            1. __id__
            2. __id_student__   : long int
            3. __login__        : timestamp
            4. __logout__       : timestamp
            5. __laboratory__   : timestamp
        - Tabela __teacher_cmd__  : REL >> 1[__student__] x N[__student_login__] 
            1. __id__           : long int
            2. __id_teacher__   : long int
            3. __cmd__          : varchar
            6. __timein__       : timestamp
            7. __timeout__      : timestamp
        - Tabela __group__        : REL >> N[__teacher__] x N[__computer__] 
            1. __id__           : long int
            2. __group_number__ : long int
            3. __id_teacher__   : long int
            3. __id_computer__  : long int
        - Tabela __computer__:
            1. __id__           : long int
            2. __mac__          : varchar
            3. __ip__           : varchar
            4. __status__       : boolean
            5. __laboratory__   : varchar
        - Tabela __computer_on__  : REL >> N[__teacher__] x N[__computer__]
            1. __id__           : long int 
            2. __id_teacher__   : long int
            3. __id_op_system__ : int : : FK >> [__op_system__ >> *id*]
            4. __id_computer__  : long int : FK >> [__computer__ >> *id*]
            5. __id_student__   : long int : FK >> [__student__ >> *id*]
            6. __timein__       : timestamp
            7. __timeout__      : timestamp
            7. __portHTTP__     : boolean
            8. __portSSH__      : boolean
        - Tabela __op_system__: REL >> 1[__computer_on__] x 1[__op_system__]
            1. __id__           : long int
            2. __os_name__      : varchar
    * Gerar uma __View__ do __group__
+ Engenharia de Software e Análise
    + Projetar documento de análise do projeto
        * Diagrama de Classes
        * Diagrama de Sequência
        * Diagrama de Estado
    * Aninhar esta etapa com a projeção do banco
+ Website
    + Projetar protótipo estático do site apenas em _HTML_
        * Integrar interface gráfica com _javascript para_ um _website_
    * Projetar site dinâmico com PHP e o CRUD dos objetos 
    * Projetar interface de comunicação entre o projeto __DWIT__ de __Fabiano__

### ETAPA ESSENCIAL
* Todo o escopo do básico completo
+ Website
    * Implementação de Websocket 
    * Melhora da interface gráfica
    * Login de acesso de usuário 

### ETAPA AVANÇADA
* Todo o escopo do básico essencial
+ Website
    * Controle de acesso duplicado ao mesmo usuário.
    * Controle de notificação caso portas que foram bloqueadas sejam desbloqueadas pelos computadores.
    * Script de auto-executável para linux e windows que retorna informações dos aluno usuário na máquina
    * Implementação de dcom-RPC para controle do sistema Windows
    * Website responsivo
    

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Kourei Makoto [koureimakoto_bitbucket@gmail.com]

